##### Available End Points:
	
1. Add new borrower application

   ```
   - POST 
   - Body (ApplicationRequest)
   - Response: (IApplication)
   ``` 
   
2. Get Application information by providing application number

   ```
   - Get("{applicationNumber}")
   - Response: (IApplication)
   ``` 
   
3. Add/Update the given bank information in given application number

   ```
   - Put [("{applicationNumber}/bankinformation")]
   - Body (BankInformation)
   - Response: (No content)			
   ```
4. Add/Update the given email information in given application number

   ```
   - Put [("{applicationNumber}/emailinforamtion")]
   - Body (EmailAddress)
   - Response: (No content)
   ```
5. Add/Update the given income information in given application number

   ```
   - Put [("{applicationNumber}/incomeinformation")]
   - Body (IncomeInformation)
   - Response: (No content)
   ```
6. Add/Update the given self declared expense in given application number

   ```
   - Put [("{applicationNumber}/declaredexpense")]
   - Body (SelfDeclareExpense)
   - Response: (No content)
   ```
7. Add/Update the given employment detail in given application number

   ```
   - Put [("{applicationNumber}/employmentinformation")]
   - Body (EmploymentDetail)
   - Response: (No content)			
   ```
8. Update application detail for given application number

   ```
   - Put [("{applicationNumber}")]
   - Body (ApplicationRequest)
   - Response: (No content)			
   ```

### Schema

```

// ApplicationRequest
{
	"RequestedAmount":"",
	"RequestedTermType":"",
	"RequestedTermValue":"",
	"PurposeOfLoan":"",
	"Source":{
				"trackingCode": "",
				"systemChannel": "",
				"sourceType": "",
				"sourceReferenceId": ""
			 },
	"ApplicationNumber":"",
	
	"EmploymentDetail":{
      "addresses": [
        {
          "addressLine1": "Employeertest1",
          "addressLine2": "Employeertest2",
          "addressLine3": "Employeertest3",
          "addressLine4": "Employeertest4",
          "landMark": "Ratna High Street",
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "work",
          "isDefault": true
        }
      ],
      "designation": "Manager",
	  "CinNumber" : "U40101TN2004PTC054931",
      "LengthOfEmploymentInMonths": 60,
      "name": "SIS",
      "workEmail": "TestSigma@gmail.com",
      "EmploymentStatus" : "Salaried",
      "workPhoneNumbers": [
      {
        "PhoneId":"",
		"phone": "9898989898",
        "countryCode": "91",
        "phoneType": "work",
        "isDefault": false
        }
      ],
	"IncomeInformation":
	[{
		"VerifiedIncome" : "1000000",
		"PaymentFrequency" : "8",
		"IncomeVerifiedDate" : "2016-10-10T15:24:28.2480918+05:30"
	}]
    },
	"ResidenceType":"",
	"SelfDeclareExpense":{
							"DebtPayments": "",
							"MonthlyRent": "",
							"MonthlyExpenses": "",
							"creditCardBalances": ""
						  },
	"ApplicationDate":"",
	"OtherPurposeDescription":"",
	"PrimaryApplicant":{}
	}
	
// Primary Applicant Request
ApplicantRequest
{
    "UserName":"",
    "Password":"",
	"UserId":"application",
    "Salutation":"",
    "FirstName":"",
    "MiddleName":"",
    "LastName":"",
   	"DateOfBirth":"",
	  "emailAddresses": [
    {
	  "id":"589c5f51fda561c9fab4cc85",
      "email": "test@gmail.com",
      "emailType": "Personal",
      "isDefault": true
    }
	],
	"phoneNumbers": [
    {
      "PhoneId":"",
      "phone": "9898989898",
      "countryCode": "91",
      "phoneType": "Mobile",
      "isDefault": false
    }
	],
	"addresses": [
    {
      "addressLine1": "test1",
      "addressLine2": "test3",
      "addressLine3": null,
      "addressLine4": null,
      "landMark": null,
      "city": "Ahmedabad",
      "state": "Gujarat",
      "pinCode": "380034",
      "country": "India",
      "addressType": "Residence",
      "isVerified": false,
      "isDefault": false
    }
	],
	"AadhaarNumber" : "",
	"PermanentAccountNumber" : "",
	"Gender":"",
	"MaritalStatus":"",
	"IncomeInformation":
	[{
		"VerifiedIncome" : "1000000",
		"PaymentFrequency" : "8",
		"IncomeVerifiedDate" : "2016-10-10T15:24:28.2480918+05:30"
	}]
	,
	"bankInformation": 
    [{
    "BankName" : "HDFC",
   	"AccountNumber" : "10004567",
   	"AccountHolderName":"SigmaInfo",
   	"IfscCode" : "Ifsccode100",
   	"AccountType":"Savings",
   	"IsVerified":"false",
	BankAddresses : {
          "addressLine1": "BankAddress1",
          "addressLine2": "BankAddress2",
          "addressLine3": null,
          "addressLine4": null,
          "landMark": null,
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "Bank",
          "isVerified": false
        }
   }],
   "employmentDetails": [
    {
      "addresses": [
        {
          "addressLine1": "Employeertest1",
          "addressLine2": "Employeertest2",
          "addressLine3": "Employeertest3",
          "addressLine4": "Employeertest4",
          "landMark": "Ratna High Street",
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "work",
          "isDefault": true
        }
      ],
      "designation": "Manager",
	  "CinNumber" : "U40101TN2004PTC054931",
      "LengthOfEmploymentInMonths": 60,
      "name": "SIS",
      "workEmail": "TestSigma@gmail.com",
      "EmploymentStatus" : "Salaried",
      "workPhoneNumbers": [
      {
        "PhoneId":"",
		"phone": "9898989898",
        "countryCode": "91",
        "phoneType": "work",
        "isDefault": false
        }
      ],
	"IncomeInformation":
	[{
		"VerifiedIncome" : "1000000",
		"PaymentFrequency" : "8",
		"IncomeVerifiedDate" : "2016-10-10T15:24:28.2480918+05:30"
	}]
    }
  ]
}
						
// ApplicationResponse
{
	"RequestedAmount":"",
	"RequestedTermType":"",
	"RequestedTermValue":"",
	"PurposeOfLoan":"",
	"Source":{
				"trackingCode": "",
				"systemChannel": "",
				"sourceType": "",
				"sourceReferenceId": ""
			 },
	"ApplicantId":"",
	"ApplicationNumber":"",
	"EmploymentDetail":
    {
      "addresses": [
        {
          "addressLine1": "Employeertest1",
          "addressLine2": "Employeertest2",
          "addressLine3": "Employeertest3",
          "addressLine4": "Employeertest4",
          "landMark": "Ratna High Street",
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "work",
          "isDefault": true
        }
      ],
      "designation": "Manager",
	  "CinNumber" : "U40101TN2004PTC054931",
      "LengthOfEmploymentInMonths": 60,
      "name": "SIS",
      "workEmail": "TestSigma@gmail.com",
      "EmploymentStatus" : "Salaried",
      "workPhoneNumbers": [
      {
        "PhoneId":"",
		"phone": "9898989898",
        "countryCode": "91",
        "phoneType": "work",
        "isDefault": false
        }
      ],
	"IncomeInformation":
	[{
		"VerifiedIncome" : "1000000",
		"PaymentFrequency" : "8",
		"IncomeVerifiedDate" : "2016-10-10T15:24:28.2480918+05:30"
	}]
    },
	"ResidenceType":"",
	"SelfDeclareExpense": {
							"DebtPayments": "",
							"MonthlyRent": "",
							"MonthlyExpenses": "",
							"creditCardBalances": ""
						  },
	"ApplicationDate":"",
	"ExpiryDate":""
}

// EmailAddress
{
	"Id":"",
	"Email":"",
	"EmailType":"",
	"IsDefault":
}

// PhoneNumber
{
	"PhoneId":"",
	"Phone":"",
	"CountryCode":"",
	"PhoneType":"",
	"IsDefault":
}

// Address
{
	"AddressId":"",
	"AddressLine1":"",
	"AddressLine2":"",
	"AddressLine3":"",
	"AddressLine4":"",
	"City":"",
	"State":"",
	"PinCode":"",
	"Country":"",
	"AddressType":"",
	"IsDefault":"",
	"LandMark":"",
	"Location":""
}

// BankInformation
{
	"BankId":"",
	"BankName":"",
	"AccountNumber":"",
	"AccountHolderName":"",
	"IfscCode":"",
	"AccountType":"Bank",
	"BankAddresses":{
	"AddressId":"",
	"AddressLine1":"",
	"AddressLine2":"",
	"AddressLine3":"",
	"AddressLine4":"",
	"City":"",
	"State":"",
	"PinCode":"",
	"Country":"",
	"AddressType":"",
	"IsDefault":"",
	"LandMark":"",
	"Location":""
	}
}

// EmploymentDetail
{
	"EmploymentId":"",
	"Name":"",
	"Addresses":[{}],
	"Designation":"",
	"WorkPhoneNumbers":[{}],
	"WorkEmailId":"",
	"LengthOfEmployment":"",
	"IncomeInformation":{},
	"EmploymentStatus":"",
	"AsOfDate":"",
	"CinNumber":""
}

// IncomeInformation
{
	"Income":"",
	"PaymentFrequency":""
}

//HighestEducationInformation
{
	"LevelOfEducation":"",
	"EducationalInstitution":""
}

// Source
{
	"TrackingCode":"",
	"SystemChannel":"",
	"SourceType":"",
	"SourceReferenceId":""
}
```
#**Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/number-generator
```

	{
     "configurations": 
      {     
        "application": {
        "prefix": "CE-",
		"suffix": "",
		"type": "Incremental",
		"separator": "",
		"startsFrom": 1,
		"leftPadding": 7
		}
      }
	}

```

2. Configurations for {{default_host}}:{{configuration_port}}/security-identity
```
{
  "roles": {
    "VP Of Sales": {
      "permissions": [
        "{*any}",
        "back-office"
      ],
      "roles": {
        "Loan Coordinator Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Loan Coordinator": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "Sr. Relationship Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Relationship Manager": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        }
      }
    },
    "VP Of Operations": {
      "permissions": [
        "{*any}",
        "back-office"
      ],
      "roles": {
        "Sr. Loan Processor": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Loan Processor": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "Credit Specialist Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Credit Specialist": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "Merchant Underwriter Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Merchant Underwriter": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "Customer Service Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Customer Service Representative": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "QA Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "QA": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        }
      }
    },
    "Director Of Merchant Support": {
      "permissions": [
        "{*any}",
        "back-office"
      ],
      "roles": {
        "Merchant Support Specialist": {
          "permissions": [
            "{*any}",
            "back-office"
          ]
        }
      }
    },
    "Marketing": {
      "permissions": [
        "{*any}",
        "back-office"
      ],
      "roles": {
        "Social Media Coordinator": {
          "permissions": [
            "{*any}",
            "back-office"
          ]
        }
      }
    },
    "Borrower": {
      "permissions": [
        "{*any}",
        "borrower"
      ]
    },
    "Merchant": {
      "permissions": [
        "{*any}",
        "merchant"
      ]
    }
  },
  "sessionTimeout": "10",
  "tokenTimeout": "60"
}
```

3. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
  "isActive": "True",
  "email": "cetenantX@creditexchange.com",
  "name": "ce",
  "timezone": "Asia/Calcutta",
  "phone": "anything",
  "paymentTimeLimit": "19",
  "website": "www.creditexchange.com"
}
```

4. Configurations for {{default_host}}:{{configuration_port}}/applicant
```
{
  "defaultRoles": [
    "borrower"
  ]
}
```

5. Configurations for {{default_host}}:{{configuration_port}}/application
```
{
  "expiryDays": "60",
  "initialLeadStatus": "200.01",
  "initialStatus": "Created/Lead"
}
```
