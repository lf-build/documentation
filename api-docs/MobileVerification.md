##### Availalbe End Points:
	
1. **Initiate Mobile Verification**
   **Description** : Initiate Mobile verification by sending across OTP using a concrete provider implementation
   **HTTP Verb**: POST
   **Route** : "{entityType}/{entityId}"
   ```
   - Request: POST("{entityType}/{entityId}")    
   - Request Schema:  {
							"Name" : "Applicant Name",
							"Phone" : "9003884343",
							"Mobile" : "8118333833",
							"Data" : {}
				    }    
   - Response:-// VerificationInitiateResponse object
   - Response Schema:  { "ReferenceNumber" : "9ee33a8e-9b4a-11e6-a40f-00163ef91450" } 
   
2. **Verify Mobile OTP**
   **Description** : Verifies OTP input by user against the reference number generated while initiating mobile verification using a concrete provider implementation
   **HTTP Verb**: POST
   **Route** : "verify/{referencenumber}/{verificationCode}"
   ```
   - Request: POST("{verify/{referencenumber}/{verificationCode}")
  
   - Response :- // VerificationResponse			
   - Response Schema:  { "VerificationSuccess" : "true"	}
   
   
### Schema 
// No separate schema to mention

```
### **Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/mobile-verification
```
{
    "MobileVerificationProvider": "Two2Factor",
    "MaxAttemptsAllowed": "3",
    "MobileVerificationSettings": {
      "BaseUrl": "http://2factor.in/API/V1",
      "ApiKey": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
      "OtpEndPoint": "SMS/{phone_number}/AUTOGEN/{template_name}",
      "OtpCheckBalanceEndPoint": "BAL/SMS",
      "VerifyOtpEndPoint": "SMS/VERIFY/{session_id}/{otp_input}",
      "TrSmsEndPoint": "ADDON_SERVICES/SEND/TSMS",
      "TrSmsCheckBalanceEndPoint": "ADDON_SERVICES/BAL/TRANSACTIONAL_SMS",
      "TrSmsDeliveryReportEndPoint": "ADDON_SERVICES/RPT/TSMS/{session_id}",
      "OtpTemplateName": "LatestMobileVerification",
      "SenderName": "QBERAA"
    }
}
```
