##### Availalbe End Points:

1. **Start Cobrand Session**  
   **Description:** Get Cobrand Log in Session .
   **HTTP Verb:** Post  
   **Route:**  "{entityType}/{entityId}/cobrand-login"  
 
       
  ```
  - Request: POST("{entityType}/{entityId}/cobrand-login")
  

  - Response:-// Add Document

 	{
  "cobrandId": 3010018936,
  "applicationId": "3349D4EC6E0D4705FFEC26F5C9EA5E00",
  "session": {
    "cobSession": "07152016_2:a9a3ecf0cf58e44914c2b379d8b00eac693c04128868775e100f2bc9e1289f54d3d1ddeec9de79e307dce40423e6d1ba38b84e0fd737fa8ff0f76f92b99be10d"
  },
  "locale": "en_US"
    }

   ```   
    
2. **Registed User**  
   **Description:** Registered New User.
   **HTTP Verb:** Post  
   **Route:**  "{entityType}/{entityId}/register"  
 
       
  ```
  - Request: POST("{entityType}/{entityId}/register")
  - Request Schema:

  {
	"userRegistrationRequest": {
		"user": {
			"loginName": "creditexchangeysl1",
			"password": "Sigma@123",
			"email": "test@gmail.com"
		}
	}
  } 

  - Response:-// Registed User Response Document

 	{
  "user": {
    "id": 32209388,
    "loginName": "00005",
    "session": {
      "userSession": "07152016_1:324faeb0a51397daee2bf715095e03ff750447811317a7544ee99a9f15509a994c07e09ef88251b7437fda7cba39cc41c481de0258457ff03217b9caaffd83b7"
    },
    "name": null,
    "preferences": {
      "currency": "USD",
      "timeZone": "PST",
      "dateFormat": "MM/dd/yyyy",
      "locale": "en_US"
    }
  }
}

   ```   
3. **Get User Session**  
   **Description:** Get User Session.
   **HTTP Verb:** Post  
   **Route:**  "{entityType}/{entityId}/user-login"  
 
       
  ```
  - Request: POST("{entityType}/{entityId}/user-login")
  
  - Response:-// Get User Session Response

 	{
  "user": {
    "id": 32209388,
    "loginName": "00005",
    "session": {
      "userSession": "07152016_1:324faeb0a51397daee2bf715095e03ff750447811317a7544ee99a9f15509a994c07e09ef88251b7437fda7cba39cc41c481de0258457ff03217b9caaffd83b7"
    },
    "name": null,
    "preferences": {
      "currency": "USD",
      "timeZone": "PST",
      "dateFormat": "MM/dd/yyyy",
      "locale": "en_US"
    }
  }
}

   ```   
4. **Get FastLink Access Toker**  
   **Description:** Generate fast link access token for account linking.
   **HTTP Verb:** GET  
   **Route:**  "{entityType}/{entityid}/fastlink-access-token"  
 
       
  ```
  - Request: GET("{entityType}/{entityid}/fastlink-access-token")
  
  - Response:-// Get Fast link access token Response

 	{
  "accessTokenData": {
    "accessTokens": [
      {
        "appId": "10003600",
        "value": "07cb259eba5c45b24a636c528de33a1ceed84ecac328b91d4a859fd6414bab64"
      }
    ]
  }
}

   ```   
5. **Generate Fast link UI form**  
   **Description:** Generate Fast link Ui form for UI integration.
   **HTTP Verb:** POST  
   **Route:**  "{entityType}/{entityid}/fastlink-url"  
 
       
  ```
  - Request: POST("{entityType}/{entityid}/fastlink-access-token")
  - Request Schema :
    {
  
  "FastLinkFlow": 0,
  "BankSearchKeyWord": "america",
  "SiteId": 0,
  "CallBackUrl":"https://www.google.com"
    }

  - Response:-// Get Fast link UI

    {
  "htmlForm": "<form id='fastlinkForm' style='display:none' action='https://yieapnode.stage.yodleeinteractive.com/authenticate/private-CreditExchange/?channelAppName=yieap' method='POST'><input type='text' name='app' value='10003600' /><input type='text' name='rsession' value='07152016_0:951dbc1d89459c35fb456805fb920ccf66002865f48f1859e0b5e7f31d0964396efc21fc968d346bed84856b703e123d2c4883d045d89318a1002b7029aafb8f' /><input type='text' name='token'  value='07cb259eba5c45b24a636c528de33a1ceed84ecac328b91d4a859fd6414bab64' /><input type='text' name='redirectReq' value='True'/><input type='text' name='extraParams' value='keyword=america&callback=https://www.google.com' /></form><script>document.getElementById('fastlinkForm').submit();</script>"
  }
``` 
6. **Get All linked Bank Accounts**  
   **Description:** List all linked bank account.  
   **HTTP Verb:** GET  
   **Route:**  "{entityType}/{entityid}/user-bank-accounts"  
 
       
  ```
  - Request: GET("{entityType}/{entityid}/user-bank-accounts")
  
  - Response:-// List All linked bank accounts
 	{
  "accounts": [
    {
      "container": "bank",
      "providerAccountId": 15089492,
      "accountName": "TESTDATA1",
      "id": 29845672,
      "accountNumber": "xxxx3xxx",
      "availableBalance": {
        "amount": 65454.78,
        "currency": "USD"
      },
      "accountType": "SAVINGS",
      "createdDate": "2017-03-28T04:57:14Z",
      "isAsset": true,
      "balance": {
        "amount": 9044.78,
        "currency": "USD"
      },
      "providerId": 16441,
      "providerName": "Dag Site",
      "refreshInfo": {
        "statusCode": 0,
        "statusMessage": "OK",
        "lastRefreshed": "2017-03-28T04:57:14Z",
        "lastRefreshAttempt": "2017-03-28T04:57:14Z",
        "nextRefreshScheduled": "2017-03-28T18:19:37Z"
      },
      "accountStatus": "ACTIVE"
    },
    {
      "container": "bank",
      "providerAccountId": 15089492,
      "accountName": "TESTDATA",
      "id": 29845668,
      "accountNumber": "xxxx3xxx",
      "availableBalance": {
        "amount": 54.78,
        "currency": "USD"
      },
      "accountType": "CHECKING",
      "createdDate": "2017-03-28T04:57:14Z",
      "isAsset": true,
      "balance": {
        "amount": 44.78,
        "currency": "USD"
      },
      "providerId": 16441,
      "providerName": "Dag Site",
      "refreshInfo": {
        "statusCode": 0,
        "statusMessage": "OK",
        "lastRefreshed": "2017-03-28T04:57:14Z",
        "lastRefreshAttempt": "2017-03-28T04:57:14Z",
        "nextRefreshScheduled": "2017-03-28T18:19:37Z"
      },
      "accountStatus": "ACTIVE"
    }
  ]
}
``` 
7. **Get ItemSummary for each site Account**  
   **Description:** Get ItemSummary for each site Account. 
   **HTTP Verb:** GET  
   **Route:**  "{entityType}/{entityId}/itemsummary"  
 
       
  ```
  - Request: GET("{entityType}/{entityId}/itemsummary")
  
  - Response:-// Get ItemSummary Response

 	{
  "result": [
    {
      "contentServiceId": 20559,
      "contentServiceInfo": {
        "contentServiceId": 20559
      },
      "isCustom": false,
      "isDisabled": false,
      "isHeld": 0,
      "isPrepop": false,
      "isSharedItem": false,
      "itemData": {
        "accounts": [
          {
            "isSeidMod": 0,
            "acctTypeId": 3,
            "acctType": "checking",
            "localizedAcctType": "fixedDeposit",
            "billPreferenceId": 0,
            "billingAccountId": 0,
            "customName": null,
            "isDeleted": 0,
            "lastUpdated": 1490677034,
            "hasDetails": 0,
            "accountNumber": "xxxx3xxx",
            "dueDate": null,
            "lastPaymentDate": null,
            "derivedAutopayEnrollmentStatusId": 0,
            "derivedAutopayEnrollmentStatus": null,
            "localizedDerivedAutopayEnrollmentStatus": null,
            "accountName": "TESTDATA",
            "userAutopayEnrollmentStatusId": 0,
            "userAutopayEnrollmentStatus": null,
            "localizedUserAutopayEnrollmentStatus": null,
            "billListToDate": null,
            "billListFromDate": null,
            "autopayEnrollmentStatusId": 0,
            "autopayEnrollmentStatus": null,
            "localizedAutopayEnrollmentStatus": null,
            "asofDate": {
              "date": null
            },
            "created": 1490677034,
            "nickName": null,
            "itemDataTableId": 10,
            "itemAccountStatusId": 1,
            "includeInNetworth": 1,
            "isAsset": 0,
            "isLinkedItemAccount": false,
            "itemAccountExtensions": null,
            "isUpdatePastTransaction": false,
            "isUpdateTxCategory": true,
            "createOpeningTxn": true,
            "cacheItemId": 23241244,
            "isItemAccountDeleted": 0,
            "accountId": 23859580,
            "level": 0,
            "accountDisplayName": {
              "defaultNormalAccountName": "Dag Site - Bank - TESTDATA"
            },
            "itemAccountId": 29845668,
            "baseTagDataId": "BANK_ACCOUNT:23241244:0:23859580",
            "refreshTime": 1490677034
          },
          {
            "isSeidMod": 0,
            "acctTypeId": 4,
            "acctType": "savings",
            "localizedAcctType": "savings",
            "billPreferenceId": 0,
            "billingAccountId": 0,
            "customName": null,
            "isDeleted": 0,
            "lastUpdated": 1490677034,
            "hasDetails": 0,
            "accountNumber": "xxxx3xxx",
            "dueDate": null,
            "lastPaymentDate": null,
            "derivedAutopayEnrollmentStatusId": 0,
            "derivedAutopayEnrollmentStatus": null,
            "localizedDerivedAutopayEnrollmentStatus": null,
            "accountName": "TESTDATA1",
            "userAutopayEnrollmentStatusId": 0,
            "userAutopayEnrollmentStatus": null,
            "localizedUserAutopayEnrollmentStatus": null,
            "billListToDate": null,
            "billListFromDate": null,
            "autopayEnrollmentStatusId": 0,
            "autopayEnrollmentStatus": null,
            "localizedAutopayEnrollmentStatus": null,
            "asofDate": {
              "date": null
            },
            "created": 1490677034,
            "nickName": null,
            "itemDataTableId": 10,
            "itemAccountStatusId": 1,
            "includeInNetworth": 1,
            "isAsset": 0,
            "isLinkedItemAccount": false,
            "itemAccountExtensions": null,
            "isUpdatePastTransaction": false,
            "isUpdateTxCategory": true,
            "createOpeningTxn": true,
            "cacheItemId": 23241244,
            "isItemAccountDeleted": 0,
            "accountId": 23859584,
            "level": 0,
            "accountDisplayName": {
              "defaultNormalAccountName": "Dag Site - Bank - TESTDATA1"
            },
            "itemAccountId": 29845672,
            "baseTagDataId": "BANK_ACCOUNT:23241244:0:23859584",
            "refreshTime": 1490677034
          }
        ]
      },
      "itemDisplayName": "Dag Site - Bank",
      "itemId": 23232244,
      "itemStatus": 0,
      "memSiteAccId": 15089492,
      "refreshInfo": {
        "itemId": 23232244,
        "statusCode": 0,
        "refreshType": 2,
        "refreshRequestTime": 0,
        "lastUpdatedTime": 1490677034,
        "lastUpdateAttemptTime": 1490677034,
        "itemAccessStatus": {
          "name": "ACCESS_VERIFIED"
        },
        "userActionRequiredType": {
          "name": "NONE"
        },
        "userActionRequiredSince": null,
        "itemCreateDate": "2017-03-27T21:57:13-0700",
        "nextUpdateTime": 1490725177,
        "responseCodeType": {
          "responseCodeTypeId": 1
        },
        "retryCount": 0,
        "refreshMode": "NORMAL"
      }
    }
  ],
  "id": 2,
  "exception": null,
  "status": "RanToCompletion",
  "isCanceled": false,
  "isCompleted": true,
  "creationOptions": "None",
  "asyncState": null,
  "isFaulted": false
}

   ```   
8. **Get Transaction Report**  
   **Description:** Get Transaction Report. 
   **HTTP Verb:** GET  
   **Route:**  "{entityType}/{entityId}/finalreport"  
 
       
  ```
  - Request: GET("{entityType}/{entityId}/finalreport")
  
  - Response:-// Get Transaction Report

 	{
  "metaData": {
    "currency": "USD",
    "duration": {
      "fromDate": "09/01/2016",
      "toDate": "03/27/2017"
    },
    "dateFormatString": "MM/dd/yyyy",
    "isManual": false
  },
  "ambDetails": [
    {
      "monthNumber": "9",
      "monthName": "September",
      "avgAmount": "0.00",
      "minBalance": "0.00",
      "maxBalance": "0.00",
      "eomBalance": "0.00"
    },
    {
      "monthNumber": "10",
      "monthName": "October",
      "avgAmount": "0.00",
      "minBalance": "0.00",
      "maxBalance": "0.00",
      "eomBalance": "0.00"
    },
    {
      "monthNumber": "11",
      "monthName": "November",
      "avgAmount": "0.00",
      "minBalance": "0.00",
      "maxBalance": "0.00",
      "eomBalance": "0.00"
    },
    {
      "monthNumber": "12",
      "monthName": "December",
      "avgAmount": "0.00",
      "minBalance": "0.00",
      "maxBalance": "0.00",
      "eomBalance": "0.00"
    },
    {
      "monthNumber": "1",
      "monthName": "January",
      "avgAmount": "1,948.46",
      "minBalance": "-6,809.22",
      "maxBalance": "9,044.78",
      "eomBalance": "9,044.78"
    },
    {
      "monthNumber": "2",
      "monthName": "February",
      "avgAmount": "9,044.78",
      "minBalance": "9,044.78",
      "maxBalance": "9,044.78",
      "eomBalance": "9,044.78"
    },
    {
      "monthNumber": "3",
      "monthName": "March",
      "avgAmount": "N/A",
      "minBalance": "9,044.78",
      "maxBalance": "9,044.78",
      "eomBalance": "N/A"
    }
  ],
  "adbDetails": {
    "avgDailyBalance": "7,102.31"
  },
  "categoryDetails": {
    "income": {
      "avgIncome": "0.0",
      "annualIncome": null
    },
    "expense": {
      "avgExpense": "0.0",
      "isTransfersIncluded": true
    },
    "transfer": {
      "totalCredit": "0.0",
      "totalDebit": "0.0"
    },
    "avgIncomeExpenseRatio": null,
    "salary": {
      "totalSalary": "0.0",
      "avgSalary": "0.0",
      "count": "0"
    }
  },
  "transactionDataObjects": [
    {
      "txnListType": "Credits",
      "count": "3",
      "totalAmt": "18,957.00",
      "transactionDataObjects": [
        {
          "dateToString": "01/17/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 1,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 9846,
          "txnType": "credit",
          "txnCategoryTypeId": 0
        },
        {
          "dateToString": "01/16/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 1,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 3465,
          "txnType": "credit",
          "txnCategoryTypeId": 0
        },
        {
          "dateToString": "01/10/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 1,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 5646,
          "txnType": "credit",
          "txnCategoryTypeId": 0
        }
      ]
    },
    {
      "txnListType": "Debits",
      "count": "1",
      "totalAmt": "-3,103.00",
      "transactionDataObjects": [
        {
          "dateToString": "01/14/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 2,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 3103,
          "txnType": "debit",
          "txnCategoryTypeId": 0
        }
      ]
    },
    {
      "txnListType": "Credit Card Payments",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Checks",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Check Inward",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Check Outward",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Inward Return",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "ATM Withdrawals",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Service Charges Fees",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "EMI",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "POS",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Reversal",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Credit Transfers",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Debit Transfers",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Debits excluding inward return",
      "count": "1",
      "totalAmt": "-3,103.00",
      "transactionDataObjects": [
        {
          "dateToString": "01/14/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 2,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 3103,
          "txnType": "debit",
          "txnCategoryTypeId": 0
        }
      ]
    },
    {
      "txnListType": "Top 5 Credits",
      "count": "3",
      "totalAmt": "",
      "transactionDataObjects": [
        {
          "dateToString": "01/17/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 1,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 9846,
          "txnType": "credit",
          "txnCategoryTypeId": 0
        },
        {
          "dateToString": "01/10/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 1,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 5646,
          "txnType": "credit",
          "txnCategoryTypeId": 0
        },
        {
          "dateToString": "01/16/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 1,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 3465,
          "txnType": "credit",
          "txnCategoryTypeId": 0
        }
      ]
    },
    {
      "txnListType": "Top 5 Debits",
      "count": "1",
      "totalAmt": "",
      "transactionDataObjects": [
        {
          "dateToString": "01/14/2017",
          "originalDescription": "DESC",
          "categoryId": 1,
          "categoryTypeId": 1,
          "transactionBaseTypeId": 2,
          "category": "Uncategorized",
          "parentCategory": "Uncategorized",
          "currency": "USD",
          "amount": 3103,
          "txnType": "debit",
          "txnCategoryTypeId": 0
        }
      ]
    },
    {
      "txnListType": "Payments made from same account",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    },
    {
      "txnListType": "Payments recieved to same account",
      "count": "0",
      "totalAmt": "0.00",
      "transactionDataObjects": null
    }
  ],
  "monthlyTotal": {
    "creditTotalList": [
      {
        "key": "1-2017",
        "total": "18,957.00"
      }
    ],
    "incomeTotalList": null
  },
  "referenceNumber": null,
  "bankAccountName": "TESTDATA1",
  "bankAccountNo": "xxxx3xxx"
}
``` 