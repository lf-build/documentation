##### Availalbe End Points:

1. Based on Pincode it will give Zone,City & GeoRiskRanking

   ```
   - [HttpGet("/pincode/verify/{pincode}")]
   - Body (pincode)
			- Response: (GeoPincodeResponse)
   ```

### Schema

```
//GeoPincodeResponse
{
	"Pincode":"123456",
	"Zone":"Zone 1",
	"City":"Ahmedabad",
	"GeoRiskRanking":1
}
```

	