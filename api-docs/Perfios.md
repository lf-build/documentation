##### Availalbe End Points:

1. **Start Transaction**  
   **Description:**Start Transaction for given institutionId by providing entitytype, entityid and Request object   
   **HTTP Verb:** POST   
   **Route:**  "{entitytype}/{entityid}/start-transaction"  
   ```
   - Request: POST("{entitytype}/{entityid}/start-transaction")    
      {
        "institutionId":"28",
        "yearMonthFrom":"2015-02",
        "yearMonthTo":"2016-02"

      }
   - Response:-// Start Transaction Response Object
     {
       "perfiosTransactionId": "ae44da2f2f1b463b94789f1fda1384f4",
       "transactionId": "ae44da2f2f1b463b94789f1fda1384f4",
       "referencenumber": "63623520bcb74764bf788a26da073c4a"
     }
   ``` 
2. **Upload Statement**  
   **Description:** After starting the transaction, upload files one by one using this endpoint by providing entitytype, entityid and Send multi-part POST requests with the one file per  
     request.  
     
   **HTTP Verb:** POST  
   **Route:**  ""{entitytype}/{entityid}/upload-statement""  
   ```
   - Request: POST("{entitytype}/{entityid}/upload-statement")    
  
   - Response:-// Upload Statement Response Object
    {
      "statementId": "1234",
      "referenceNumber": "f176c2a4d3ed4106bd256c4d50bef998"
    }
   ``` 
3. **Complete Transaction**  
   **Description:** endpoint complete transaction by providing entitytype , entityId.
   **HTTP Verb:** POST  
   **Route:**  "{entitytype}/{entityid}/complete-transaction"  
   ```
   - Request: POST("{entitytype}/{entityid}/complete-transaction")    

   - Response:-//Complete Transaction Response Object
   {
       "expiration": null,
       "data": null,
       "name": "perfios",
       "entityId": "1",
       "entityType": "application",
       "syndicationData": null,
       "syndicationName": "perfios",
       "request": {
         "perfiosTransactionId": "f176c2a4d3ed4106bd256c4d50bef998"
       },
      "response": "Transaction completed successfully"
           
    }
   ``` 
4. **Transaction Status**  
   **Description:** endpoint find out what went wrong if the Complete Transaction API returns an error response by providing entitytype,entityid.  
   **HTTP Verb:** GET  
   **Route:**  "{entitytype}/{entityid}/status"  
   ```
   - Request: GET("{entitytype}/{entityid}/status")    

   - Response:-// Transaction Status Response Object
    {
      "status": "success",
      "errorCode":"E_NO_ERROR",
      "reason":"",
      "parts":"1",
      "processing":"completed",
      "files":"available",
      "referenceNumber":"f176c2a4d3ed4106bd256c4d50bef998"
           
    }
   ```
5. **Delete data**  
   **Description:** endpoint delete data regarding transaction by providing entitytype,entityid.  
   **HTTP Verb:** DELETE  
   **Route:**  "{entitytype}/{entityid}/delete-data"  
   ```
   - Request: DELETE("{entitytype}/{entityid}/delete-data")    

   - Response:-// delete data Response Object
    {
      "status": "success",
      "referenceNumber":"f176c2a4d3ed4106bd256c4d50bef998"
           
    }
   ```  
6. **Request Review of A Perfios Transaction**  
   **Description:** get the status of perfios transaction that submited for review  by providing entitytype,entityid.  
   **HTTP Verb:** POST  
   **Route:**  "{entitytype}/{entityid}/review-transaction"  
   ```
   - Request: POST("{entitytype}/{entityid}/review-transaction")    

   - Response:-// Review of A Perfios Transaction Response Object
    {
      "status": "success",
      "referenceNumber":"f176c2a4d3ed4106bd256c4d50bef998"
           
    }
   ```  
7. **List of supported Institutions**  
   **Description:** get list of supported Institutions  by providing entitytype,entityid.  
   **HTTP Verb:** GET  
   **Route:**  "supported-instition"  
   ```
   - Request: POST("supported-instition")    

   - Response:-// List supported-instition Response Object
    {
     "instiutions": [
     {
      "id": "3",
      "name": "ADCB, India",
      "institutionType": "bank"
     },
     {  
      "id": "998",
      "name": "Acme Bank Ltd., India",
      "institutionType": "bank"
     },
     {
      "id": "4",
      "name": "Allahabad Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "5",
      "name": "American Express Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "6",
      "name": "Andhra Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "2",
      "name": "Axis Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "7",
      "name": "BNP Paribas, India",
      "institutionType": "bank"
     },
     {
      "id": "156",
      "name": "Bandhan Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "8",
      "name": "Bank of Baroda, India",
      "institutionType": "bank"
     },
     {
      "id": "9",
      "name": "Bank of India, India",
      "institutionType": "bank"
     },
     {
      "id": "10",
      "name": "Bank of Maharashtra, India",
      "institutionType": "bank"
     },
     {
      "id": "73",
      "name": "Barclays Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "11",
      "name": "Canara Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "111",
      "name": "Catholic Syrian Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "12",
      "name": "Central Bank of India, India",
      "institutionType": "bank"
    },
    {
      "id": "14",
      "name": "Citibank, India",
      "institutionType": "bank"
    },
    {
      "id": "57",
      "name": "City Union Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "15",
      "name": "Corporation Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "83",
      "name": "Cosmos Co-op. Bank Ltd., India",
      "institutionType": "bank"
    },
    {
      "id": "16",
      "name": "DBS Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "17",
      "name": "Dena Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "49",
      "name": "Deutsche Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "18",
      "name": "Development Credit Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "71",
      "name": "Dhanalakshmi Bank Ltd., India",
      "institutionType": "bank"
    },
    {
      "id": "19",
      "name": "Federal Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "152",
      "name": "Fin Growth Co-Op bank Ltd., India, India",
      "institutionType": "bank"
    },
    {
      "id": "20",
      "name": "HDFC Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "21",
      "name": "HSBC, India",
      "institutionType": "bank"
    },
    {
      "id": "74",
      "name": "ICICI Bank B2 (Branch Free Banking), India",
      "institutionType": "bank"
    },
    {
      "id": "22",
      "name": "ICICI Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "23",
      "name": "IDBI, India",
      "institutionType": "bank"
    },
    {
      "id": "140",
      "name": "IDFC Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "24",
      "name": "Indian Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "25",
      "name": "Indian Overseas Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "26",
      "name": "IndusInd Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "27",
      "name": "J&K Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "51",
      "name": "Karnataka Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "50",
      "name": "Karur Vysya Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "28",
      "name": "Kotak Mahindra / Ing Vysya Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "77",
      "name": "Lakshmi Vilas Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "29",
      "name": "Oriental Bank of Commerce, India",
      "institutionType": "bank"
    },
    {
      "id": "30",
      "name": "Punjab National Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "31",
      "name": "Punjab and Sind Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "81",
      "name": "RBL (Ratnakar) Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "48",
      "name": "RBS (ABN AMRO), India",
      "institutionType": "bank"
    },
    {
      "id": "72",
      "name": "Saraswat Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "52",
      "name": "South Indian Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "32",
      "name": "Standard Chartered Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "33",
      "name": "State Bank of Bikaner and Jaipur, India",
      "institutionType": "bank"
    },
    {
      "id": "34",
      "name": "State Bank of Hyderabad, India",
      "institutionType": "bank"
    },
    {
      "id": "35",
      "name": "State Bank of India, India",
      "institutionType": "bank"
    },
    {
      "id": "36",
      "name": "State Bank of Indore, India",
      "institutionType": "bank"
    },
    {
      "id": "37",
      "name": "State Bank of Mysore, India",
      "institutionType": "bank"
    },
    {
      "id": "38",
      "name": "State Bank of Patiala, India",
      "institutionType": "bank"
    },
    {
      "id": "39",
      "name": "State Bank of Saurashtra, India",
      "institutionType": "bank"
    },
    {
      "id": "40",
      "name": "State Bank of Travancore, India",
      "institutionType": "bank"
    },
    {
      "id": "41",
      "name": "Syndicate Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "90",
      "name": "TJSB Sahakari Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "75",
      "name": "Tamilnad Mercantile Bank Ltd., India",
      "institutionType": "bank"
    },
    {
      "id": "42",
      "name": "UCO Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "43",
      "name": "Union Bank of India, India",
      "institutionType": "bank"
    },
    {
      "id": "44",
      "name": "United Bank of India, India",
      "institutionType": "bank"
    },
    {
      "id": "45",
      "name": "Vijaya Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "46",
      "name": "Yes Bank, India",
      "institutionType": "bank"
    }
   ],
    "referenceNumber": "6eb2500860ec49fe86743a981e1e7767"
           
    }
   ```  
8. **Retrieve PDF Reports**  
   **Description:** get pdf report  by providing entitytype,entityid.  
   **HTTP Verb:** GET  
   **Route:**  "{entitytype}/{entityid}/pdf"  
   ```
   - Request: GET("{entitytype}/{entityid}/pdf")    

   - Response:-// PDF report response Object
    {
      "report": "f176c2a4d3ed4106bd256c4d50bef998f176c2a4d3ed4106bd256c4d50bef998f176c2a4d3ed4106bd256c4d50bef998f176c2a4d3ed4106bd256c4d50bef998",
      "referenceNumber":"f176c2a4d3ed4106bd256c4d50bef998"
    }
   ```  
9. **Retrieve XML Reports**  
   **Description:** get XML report  by providing entitytype,entityid.  
   **HTTP Verb:** GET  
   **Route:**  "{entitytype}/{entityid}/xml"  
   ```
   - Request: GET("{entitytype}/{entityid}/xml")    

   - Response:-// XML report response Object
    {
       "startDate": "01/01/2015 00:00:00",
       "endDate": "08/31/2016 00:00:00",
       "durationInMonths": "20",
       "bank": "HDFC Bank, India",
       "accountNo": "15731140009913",
       "accountType": "",
       "customerInfo":
        {
            "name": "GEETA RAHUL SHARMA",
            "address": "HYDERABAD INDIA",
            "landline": "",
            "mobile": "022-61606161",
            "email": "geetars1981@gmail.com",
            "pan": ""
        },
      "statementSummary":
        {
         "avgBalanceOf6Dates": "8954.71",
         "avgDailyBalance": "8302.96",
         "avgInvestmentExpensePerMonth": "0.00",
         "avgInvestmentIncomePerMonth": "4.10",
         "avgMonthlyExpense": "1000.55",
    "avgMonthlyIncome": "34562.45",
    "avgMonthlySurplus": "596.10",
    "closingBalance": "6621.86",
    "expenseToIncomeRatio": "0.45",
    "highestExpenseInAMonth": null,
    "highestIncomeInAMonth": "49000.00",
    "highestInvestmentExpenseInAMonth": "0.00",
    "highestInvestmentIncomeInAMonth": "82.00",
    "highestSavingsInAMonth": "12175.24",
    "inwardChqBounces": "0",
    "inwardEcsBounces": "0",
    "inwardOtherBounces": "0",
    "lastMonthExpense": "0.00",
    "lastMonthIncome": "0.00",
    "lastMonthInvestmentExpense": "0.00",
    "lastMonthInvestmentIncome": "0.00",
    "lastMonthSavings": "0.00",
    "lowestExpenseInAMonth": "2.81",
    "lowestIncomeInAMonth": "60.00",
    "lowestInvestmentExpenseInAMonth": "0.00",
    "lowestInvestmentIncomeInAMonth": "82.00",
    "lowestSavingsInAMonth": "-21292.27",
    "maxBalance": "70018.47",
    "maxCredit": "70000.00",
    "maxDebit": "70000.00",
    "minBalance": "0.00",
    "minCredit": "18.00",
    "minDebit": "2.81",
    "numberOfTransactions": "287",
    "openingBalance": "18432.62",
    "outwardChqBounces": "0",
    "outwardEcsBounces": "0",
    "outwardOtherBounces": "0",
    "salaryCredits": "4"
    },
    "transactions": [
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3828843347/IDEA (Ref#601100304)",
      "amount": "-140.00",
      "category": "Others",
      "balance": "18292.62"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "",
      "narration": "REF-PAYUBOOKMY-345103748-",
      "amount": "111.24",
      "category": "Reversal",
      "balance": "18403.86"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "1469",
      "narration": "NWD-5242540100259417-S1CW6195-MUBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "8403.86"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "1469",
      "narration": "NWD-5242540100259417-S1CW6195-MUBAI",
      "amount": "10000.00",
      "category": "Reversal",
      "balance": "18403.86"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "7722",
      "narration": "NWD-5242540100259417-S1CW4684-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "8403.86"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "7723",
      "narration": "NWD-5242540100259417-S1CW4684-MUMBAI",
      "amount": "-5000.00",
      "category": "Cash Withdrawal",
      "balance": "3403.86"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CHGS INCL ST & CESS (Ref#300515)",
      "amount": "-2.81",
      "category": "Bank Charges",
      "balance": "3401.05"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.47",
      "category": "Bank Charges",
      "balance": "3378.58"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM NON CASH(1TXN)",
      "amount": "-9.55",
      "category": "Others",
      "balance": "3369.03"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "1119.03"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.47",
      "category": "Bank Charges",
      "balance": "1096.56"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3831667658/IDEA (Ref#602210144)",
      "amount": "-200.00",
      "category": "Others",
      "balance": "896.56"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "820",
      "narration": "ATW-5242540100259417-S1ACMM76-MUMBAI",
      "amount": "-400.00",
      "category": "Cash Withdrawal",
      "balance": "496.56"
    },
    {
      "date": "06/04/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR151554200964 (Ref#AXIR151554200964)",
      "amount": "2700.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "3196.56"
    },
    {
      "date": "06/05/2015 00:00:00",
      "chqNo": "",
      "narration": "PMJBY339656",
      "amount": "-330.00",
      "category": "Others",
      "balance": "2866.56"
    },
    {
      "date": "06/05/2015 00:00:00",
      "chqNo": "1384",
      "narration": "ATW-5242540100259417-S1ACMM76-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "1866.56"
    },
    {
      "date": "06/06/2015 00:00:00",
      "chqNo": "969557",
      "narration": "POS 5242540100259417 D MART POS DEBIT",
      "amount": "-596.00",
      "category": "Household",
      "balance": "1270.56"
    },
    {
      "date": "06/12/2015 00:00:00",
      "chqNo": "",
      "narration": "352281774/PAYUBOOKMYSHOW (Ref#612123621)",
      "amount": "-89.12",
      "category": "Entertainment",
      "balance": "1181.44"
    },
    {
      "date": "06/12/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3847457708/IDEA (Ref#612152157)",
      "amount": "-50.00",
      "category": "Others",
      "balance": "1131.44"
    },
    {
      "date": "06/12/2015 00:00:00",
      "chqNo": "3442",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-200.00",
      "category": "Cash Withdrawal",
      "balance": "931.44"
    },
    {
      "date": "06/15/2015 00:00:00",
      "chqNo": "",
      "narration": "161927130/TECHTATASKY (Ref#615154246)",
      "amount": "-200.00",
      "category": "Utilities",
      "balance": "731.44"
    },
    {
      "date": "06/19/2015 00:00:00",
      "chqNo": "1073",
      "narration": "NWD-5242540100259417-SPCND467-MUMBAI",
      "amount": "-700.00",
      "category": "Cash Withdrawal",
      "balance": "31.44"
    },
    {
      "date": "06/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "8.64"
    },
    {
      "date": "06/30/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-HSBC0400002-SALARY PROCESSING - EPAY ACC-CHETAN GORE-HSBCN15181981785 (Ref#HSBCN15181981785)",
      "amount": "52546.00",
      "category": "Salary",
      "balance": "32554.64"
    },
    {
      "date": "06/30/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3879170439/BILLDKVODAFONEINDIAL (Ref#630193324)",
      "amount": "-506.61",
      "category": "Utilities",
      "balance": "32048.03"
    },
    {
      "date": "06/30/2015 00:00:00",
      "chqNo": "6485",
      "narration": "ATW-5242540100259417-S1ANMI64-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "22048.03"
    },
    {
      "date": "06/30/2015 00:00:00",
      "chqNo": "6486",
      "narration": "ATW-5242540100259417-S1ANMI64-MUMBAI",
      "amount": "-2500.00",
      "category": "Cash Withdrawal",
      "balance": "19548.03"
    },
    {
      "date": "07/01/2015 00:00:00",
      "chqNo": "2771",
      "narration": "POS 5242540100259417 BIG BAZAAR. POS DEB",
      "amount": "-613.00",
      "category": "Household",
      "balance": "18935.03"
    },
    {
      "date": "07/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT DR-UTIB0000373-CHETAN GORE-NETBANK, MUM-N182150078915389 (Ref#N182150078915389)",
      "amount": "-500.00",
      "category": "Transfer to CHETAN GORE",
      "balance": "18435.03"
    },
    {
      "date": "07/01/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3880588179/IDEA (Ref#701141700)",
      "amount": "-140.00",
      "category": "Others",
      "balance": "18295.03"
    },
    {
      "date": "07/02/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT DR-UTIB0000373-CHETAN GORE-NETBANK, MUM-N183150079168962 (Ref#N183150079168962)",
      "amount": "-500.00",
      "category": "Transfer to CHETAN GORE",
      "balance": "17795.03"
    },
    {
      "date": "07/02/2015 00:00:00",
      "chqNo": "3034",
      "narration": "POS 5242540100259417 BIG BAZAAR. POS DEB IT",
      "amount": "-453.00",
      "category": "Household",
      "balance": "17342.03"
    },
    {
      "date": "07/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "15092.03"
    },
    {
      "date": "07/03/2015 00:00:00",
      "chqNo": "6539",
      "narration": "NWD-5242540100259417-SN003312-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "14592.03"
    },
    {
      "date": "07/03/2015 00:00:00",
      "chqNo": "6540",
      "narration": "NWD-5242540100259417-SN003312-MUMBAI",
      "amount": "-2500.00",
      "category": "Cash Withdrawal",
      "balance": "12092.03"
    },
    {
      "date": "07/04/2015 00:00:00",
      "chqNo": "2538",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-6000.00",
      "category": "Cash Withdrawal",
      "balance": "6092.03"
    },
    {
      "date": "07/08/2015 00:00:00",
      "chqNo": "2815",
      "narration": "POS 5242540100259417 BIG BAZAAR. POS DEB IT",
      "amount": "-197.00",
      "category": "Household",
      "balance": "5895.03"
    },
    {
      "date": "07/08/2015 00:00:00",
      "chqNo": "1908",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-1800.00",
      "category": "Cash Withdrawal",
      "balance": "4095.03"
    },
    {
      "date": "07/10/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CHGS INCL ST & CESS (Ref#10715)",
      "amount": "-2.85",
      "category": "Bank Charges",
      "balance": "4092.18"
    },
    {
      "date": "07/10/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CHGS INCL ST & CESS (Ref#20715)",
      "amount": "-2.85",
      "category": "Bank Charges",
      "balance": "4089.33"
    },
    {
      "date": "07/10/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "4066.53"
    },
    {
      "date": "07/15/2015 00:00:00",
      "chqNo": "8253",
      "narration": "NWD-5242540100259417-SN003312-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "3066.53"
    },
    {
      "date": "07/15/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3905243994/IDEA (Ref#715143607)",
      "amount": "-50.00",
      "category": "Others",
      "balance": "3016.53"
    },
    {
      "date": "07/17/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "2993.73"
    },
    {
      "date": "07/17/2015 00:00:00",
      "chqNo": "",
      "narration": "374419962/PAYUBOOKMYSHOW (Ref#717143640)",
      "amount": "-106.94",
      "category": "Entertainment",
      "balance": "2886.79"
    },
    {
      "date": "07/19/2015 00:00:00",
      "chqNo": "6930",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "2386.79"
    },
    {
      "date": "07/19/2015 00:00:00",
      "chqNo": "1477",
      "narration": "POS 5242540100259417 HOTEL NITYANAND POS DEBIT",
      "amount": "-505.00",
      "category": "Others",
      "balance": "1881.79"
    },
    {
      "date": "07/20/2015 00:00:00",
      "chqNo": "3134",
      "narration": "ATW-5242540100259417-S1ANMI71-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "881.79"
    },
    {
      "date": "07/21/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "858.99"
    },
    {
      "date": "07/23/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR152040438382 (Ref#AXIR152040438382)",
      "amount": "1900.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "2758.99"
    },
    {
      "date": "07/24/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3922347277/IDEA (Ref#724111657)",
      "amount": "-50.00",
      "category": "Others",
      "balance": "2708.99"
    },
    {
      "date": "07/25/2015 00:00:00",
      "chqNo": "1668",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "2208.99"
    },
    {
      "date": "07/25/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3923959791/BOOKMYSHOW COM HDFC BANK LIMITED (Ref#725083015)",
      "amount": "-434.46",
      "category": "Entertainment",
      "balance": "1774.53"
    },
    {
      "date": "07/25/2015 00:00:00",
      "chqNo": "6198",
      "narration": "NWD-5242540100259417-SPCND467-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "774.53"
    },
    {
      "date": "07/26/2015 00:00:00",
      "chqNo": "",
      "narration": "381915803/PAYUBOOKMYSHOW (Ref#726090334)",
      "amount": "-115.86",
      "category": "Entertainment",
      "balance": "658.67"
    },
    {
      "date": "07/26/2015 00:00:00",
      "chqNo": "7816",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-400.00",
      "category": "Cash Withdrawal",
      "balance": "258.67"
    },
    {
      "date": "07/27/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN)",
      "amount": "-45.60",
      "category": "Bank Charges",
      "balance": "213.07"
    },
    {
      "date": "07/28/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "190.27"
    },
    {
      "date": "07/29/2015 00:00:00",
      "chqNo": "2372",
      "narration": "ATW-5242540100259417-P3AWBL15-MUMBAI",
      "amount": "-100.00",
      "category": "Cash Withdrawal",
      "balance": "90.27"
    },
    {
      "date": "07/30/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3934068894/IDEA (Ref#730114018)",
      "amount": "-50.00",
      "category": "Others",
      "balance": "40.27"
    },
    {
      "date": "07/30/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3934073816/IDEA (Ref#730114231)",
      "amount": "-30.00",
      "category": "Others",
      "balance": "10.27"
    },
    {
      "date": "07/31/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-HSBC0400002-SALARY PROCESSING - EPAY ACC-CHETAN GORE-HSBCN15212130937 (Ref#HSBCN15212130937)",
      "amount": "52713.00",
      "category": "Salary",
      "balance": "31723.27"
    },
    {
      "date": "08/01/2015 00:00:00",
      "chqNo": "8801",
      "narration": "NWD-5242540100259417-SN003308-MUMBAI",
      "amount": "-2500.00",
      "category": "Cash Withdrawal",
      "balance": "29223.27"
    },
    {
      "date": "08/01/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3938767447/IDEA (Ref#801211637)",
      "amount": "-200.00",
      "category": "Others",
      "balance": "29023.27"
    },
    {
      "date": "08/01/2015 00:00:00",
      "chqNo": "8472",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "19023.27"
    },
    {
      "date": "08/01/2015 00:00:00",
      "chqNo": "8473",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-7000.00",
      "category": "Cash Withdrawal",
      "balance": "12023.27"
    },
    {
      "date": "08/02/2015 00:00:00",
      "chqNo": "4101",
      "narration": "EAW-5242540100259417-CWAC0290-MUMBAI",
      "amount": "-1500.00",
      "category": "Cash Withdrawal",
      "balance": "10523.27"
    },
    {
      "date": "08/02/2015 00:00:00",
      "chqNo": "1307",
      "narration": "ATW-5242540100259417-S1ACMU25-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "9523.27"
    },
    {
      "date": "08/02/2015 00:00:00",
      "chqNo": "715",
      "narration": "POS 5242540100259417 BABA SWEET MART POS DEBIT",
      "amount": "-439.00",
      "category": "Others",
      "balance": "9084.27"
    },
    {
      "date": "08/03/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "6834.27"
    },
    {
      "date": "08/03/2015 00:00:00",
      "chqNo": "1744",
      "narration": "POS 5242540100259417 BIG BAZAAR. POS DEB IT",
      "amount": "-240.00",
      "category": "Household",
      "balance": "6594.27"
    },
    {
      "date": "08/04/2015 00:00:00",
      "chqNo": "1613",
      "narration": "ATW-5242540100259417-P3AWMT46-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "6094.27"
    },
    {
      "date": "08/04/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3943044886/BILLDKVODAFONEINDIAL (Ref#804115625)",
      "amount": "-572.28",
      "category": "Utilities",
      "balance": "5521.99"
    },
    {
      "date": "08/05/2015 00:00:00",
      "chqNo": "6582",
      "narration": "ATW-5242540100259417-S1ANMB09-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "5021.99"
    },
    {
      "date": "08/05/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "4999.19"
    },
    {
      "date": "08/07/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR152193001689 (Ref#AXIR152193001689)",
      "amount": "1950.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "6949.19"
    },
    {
      "date": "08/07/2015 00:00:00",
      "chqNo": "5445",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-900.00",
      "category": "Cash Withdrawal",
      "balance": "6049.19"
    },
    {
      "date": "08/09/2015 00:00:00",
      "chqNo": "",
      "narration": "392558594/PAYUBOOKMYSHOW (Ref#809074727)",
      "amount": "-94.69",
      "category": "Entertainment",
      "balance": "5954.50"
    },
    {
      "date": "08/09/2015 00:00:00",
      "chqNo": "6053",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-900.00",
      "category": "Cash Withdrawal",
      "balance": "5054.50"
    },
    {
      "date": "08/10/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "5031.70"
    },
    {
      "date": "08/11/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) HDFC BANK LIMITED",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "5008.90"
    },
    {
      "date": "08/12/2015 00:00:00",
      "chqNo": "81977",
      "narration": "CHQ PAID-MICR CTS-MU-RELIANCE INFRASTRAT",
      "amount": "-4000.00",
      "category": "Transfer to RELIANCE INFRASTRAT",
      "balance": "1008.90"
    },
    {
      "date": "08/12/2015 00:00:00",
      "chqNo": "5996",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "8.90"
    },
    {
      "date": "08/13/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-8.90",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "08/17/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXMB152294484860 (Ref#AXMB152294484860)",
      "amount": "3000.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "3000.00"
    },
    {
      "date": "08/17/2015 00:00:00",
      "chqNo": "",
      "narration": "398392885/PAYUBOOKMYSHOW (Ref#817110507)",
      "amount": "-94.03",
      "category": "Entertainment",
      "balance": "2905.97"
    },
    {
      "date": "08/17/2015 00:00:00",
      "chqNo": "81978",
      "narration": "CHQ PAID - GOREGOAN EAS",
      "amount": "-800.00",
      "category": "Others",
      "balance": "2105.97"
    },
    {
      "date": "08/19/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM NON CASH(1TXN)",
      "amount": "-9.69",
      "category": "Others",
      "balance": "2096.28"
    },
    {
      "date": "08/20/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3972800655/IDEA (Ref#820100919)",
      "amount": "-120.00",
      "category": "Others",
      "balance": "1976.28"
    },
    {
      "date": "08/20/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3972805551/IDEA (Ref#820101204)",
      "amount": "-98.00",
      "category": "Others",
      "balance": "1878.28"
    },
    {
      "date": "08/20/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3972808542/IDEA (Ref#820101349)",
      "amount": "-75.00",
      "category": "Others",
      "balance": "1803.28"
    },
    {
      "date": "08/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) (Ref#130815)",
      "amount": "-13.90",
      "category": "Bank Charges",
      "balance": "1789.38"
    },
    {
      "date": "08/21/2015 00:00:00",
      "chqNo": "6993",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-700.00",
      "category": "Cash Withdrawal",
      "balance": "1089.38"
    },
    {
      "date": "08/23/2015 00:00:00",
      "chqNo": "9959",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "589.38"
    },
    {
      "date": "08/24/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "566.58"
    },
    {
      "date": "08/24/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "543.78"
    },
    {
      "date": "08/25/2015 00:00:00",
      "chqNo": "7241",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "43.78"
    },
    {
      "date": "08/25/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXMB152375761856 (Ref#AXMB152375761856)",
      "amount": "60.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "103.78"
    },
    {
      "date": "08/26/2015 00:00:00",
      "chqNo": "1334",
      "narration": "ATW-5242540100259417-S1ANMB09-MUMBAI",
      "amount": "-100.00",
      "category": "Cash Withdrawal",
      "balance": "3.78"
    },
    {
      "date": "08/26/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-3.78",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-HSBC0400002-SALARY PROCESSING - EPAY ACC-CHETAN GORE-HSBCN15243032650 (Ref#HSBCN15243032650)",
      "amount": "52713.00",
      "category": "Salary",
      "balance": "31713.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "545",
      "narration": "POS 5242540100259417 BIG BAZAAR POS DEBI T",
      "amount": "-1282.00",
      "category": "Household",
      "balance": "30431.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "3797",
      "narration": "ATW-5242540100259417-S1ANMI71-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "20431.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "3798",
      "narration": "ATW-5242540100259417-S1ANMI71-MUMBAI",
      "amount": "-9000.00",
      "category": "Cash Withdrawal",
      "balance": "11431.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "7295",
      "narration": "ATW-5242540100259417-S1ACMU25-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "10431.00"
    },
    {
      "date": "09/01/2015 00:00:00",
      "chqNo": "",
      "narration": "407992034/PAYUBOOKMYSHOW (Ref#901094153)",
      "amount": "-80.21",
      "category": "Entertainment",
      "balance": "10350.79"
    },
    {
      "date": "09/01/2015 00:00:00",
      "chqNo": "311914",
      "narration": "POS 5242540100259417 VODAFONE INDIA L PO S DEBIT",
      "amount": "-728.00",
      "category": "Utilities",
      "balance": "9622.79"
    },
    {
      "date": "09/01/2015 00:00:00",
      "chqNo": "5273",
      "narration": "ATW-5242540100259417-S1ANMI64-MUMBAI HDFC BANK LIMITED",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "8622.79"
    },
    {
      "date": "09/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "6372.79"
    },
    {
      "date": "09/02/2015 00:00:00",
      "chqNo": "2052",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "5372.79"
    },
    {
      "date": "09/03/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) (Ref#280815)",
      "amount": "-19.02",
      "category": "Bank Charges",
      "balance": "5353.77"
    },
    {
      "date": "09/04/2015 00:00:00",
      "chqNo": "2670",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "4353.77"
    },
    {
      "date": "09/04/2015 00:00:00",
      "chqNo": "",
      "narration": "410139517/PAYUBOOKMYSHOW (Ref#904210837)",
      "amount": "-140.36",
      "category": "Entertainment",
      "balance": "4213.41"
    },
    {
      "date": "09/06/2015 00:00:00",
      "chqNo": "3812",
      "narration": "NWD-524254XXXXXX9417-SPCND467-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "3213.41"
    },
    {
      "date": "09/08/2015 00:00:00",
      "chqNo": "8684",
      "narration": "NWD-524254XXXXXX9417-SN003315-MUMBAI",
      "amount": "-1500.00",
      "category": "Cash Withdrawal",
      "balance": "1713.41"
    },
    {
      "date": "09/08/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF4007924308/IDEA (Ref#908154515)",
      "amount": "-150.00",
      "category": "Others",
      "balance": "1563.41"
    },
    {
      "date": "09/09/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "1540.61"
    },
    {
      "date": "09/11/2015 00:00:00",
      "chqNo": "4760",
      "narration": "ATW-524254XXXXXX9417-S1ANMB09-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "1040.61"
    },
    {
      "date": "09/12/2015 00:00:00",
      "chqNo": "6607",
      "narration": "NWD-524254XXXXXX9417-SPCND467-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "540.61"
    },
    {
      "date": "09/14/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXMB152579104307 (Ref#AXMB152579104307)",
      "amount": "1500.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "2040.61"
    },
    {
      "date": "09/15/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "2017.81"
    },
    {
      "date": "09/18/2015 00:00:00",
      "chqNo": "6599",
      "narration": "NWD-524254XXXXXX9417-SACWF642-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "1517.81"
    },
    {
      "date": "09/19/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#526208989357)",
      "amount": "-1400.00",
      "category": "Cash Withdrawal",
      "balance": "117.81"
    },
    {
      "date": "09/19/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#526208989358)",
      "amount": "-100.00",
      "category": "Cash Withdrawal",
      "balance": "17.81"
    },
    {
      "date": "09/19/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-17.81",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "09/28/2015 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP MAROL - MUMB",
      "amount": "2000.00",
      "category": "Cash Deposit",
      "balance": "2000.00"
    },
    {
      "date": "09/28/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF4037729215/IDEA (Ref#928130051)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "1900.00"
    },
    {
      "date": "09/29/2015 00:00:00",
      "chqNo": "6842",
      "narration": "EAW-524254XXXXXX9417-AECN2190-AMUMBAI",
      "amount": "-800.00",
      "category": "Cash Withdrawal",
      "balance": "1100.00"
    },
    {
      "date": "09/29/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) (Ref#210915)",
      "amount": "-4.99",
      "category": "Bank Charges",
      "balance": "1095.01"
    },
    {
      "date": "09/29/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN) (Ref#220915)",
      "amount": "-45.60",
      "category": "Bank Charges",
      "balance": "1049.41"
    },
    {
      "date": "09/30/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "1026.61"
    },
    {
      "date": "10/01/2015 00:00:00",
      "chqNo": "",
      "narration": "CREDIT INTEREST CAPITALISED",
      "amount": "82.00",
      "category": "Interest",
      "balance": "1108.61"
    },
    {
      "date": "10/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#527408989396)",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "608.61"
    },
    {
      "date": "10/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#527408989397)",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "108.61"
    },
    {
      "date": "10/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#527416989566)",
      "amount": "-100.00",
      "category": "Cash Withdrawal",
      "balance": "8.61"
    },
    {
      "date": "10/03/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-2241.39"
    },
    {
      "date": "10/03/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "8.61"
    },
    {
      "date": "10/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-2241.39"
    },
    {
      "date": "10/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253698879 HDFC BANK LIMITED",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "8.61"
    },
    {
      "date": "10/09/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-HSBC0400002-LINTAS I P L SALARY ACCOUNT-CHETAN GORE-HSBCN15282837860 (Ref#HSBCN15282837860)",
      "amount": "52585.00",
      "category": "Salary",
      "balance": "10593.61"
    },
    {
      "date": "10/09/2015 00:00:00",
      "chqNo": "595397",
      "narration": "POS 524254XXXXXX9417 VODAFONE INDIA L PO S DEBIT",
      "amount": "-1393.00",
      "category": "Utilities",
      "balance": "9200.61"
    },
    {
      "date": "10/09/2015 00:00:00",
      "chqNo": "8961",
      "narration": "EAW-524254XXXXXX9417-DWDW1062-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "7200.61"
    },
    {
      "date": "10/09/2015 00:00:00",
      "chqNo": "40870",
      "narration": "POS 524254XXXXXX9417 MORRIS POS DEBIT",
      "amount": "-600.00",
      "category": "Others",
      "balance": "6600.61"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4056476401/IDEA (Ref#1010071819)",
      "amount": "-154.00",
      "category": "Others",
      "balance": "6446.61"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4056479972/IDEA (Ref#1010072653)",
      "amount": "-350.00",
      "category": "Others",
      "balance": "6096.61"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "435706119/PAYUBOOKMYSHOW (Ref#1010080614)",
      "amount": "-380.99",
      "category": "Entertainment",
      "balance": "5715.62"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4056519363/IDEA (Ref#1010082604)",
      "amount": "-209.00",
      "category": "Others",
      "balance": "5506.62"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "690090",
      "narration": "POS 524254XXXXXX9417 HARDCASTLE RESTA PO S DEBIT",
      "amount": "-141.13",
      "category": "Others",
      "balance": "5365.49"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "6641",
      "narration": "NWD-524254XXXXXX9417-SN003308-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "3365.49"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "9288",
      "narration": "EAW-524254XXXXXX9417-DWDW1062-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "1365.49"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "436511024/PAYUBOOKMYSHOW (Ref#1010222609)",
      "amount": "-127.00",
      "category": "Entertainment",
      "balance": "1238.49"
    },
    {
      "date": "10/11/2015 00:00:00",
      "chqNo": "7826",
      "narration": "EAW-524254XXXXXX9417-DWDC1062-MUMBAI",
      "amount": "-1200.00",
      "category": "Cash Withdrawal",
      "balance": "38.49"
    },
    {
      "date": "10/12/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN)",
      "amount": "-38.49",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGAON WES",
      "amount": "40000.00",
      "category": "Cash Deposit",
      "balance": "40000.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4063567190/BILLDESKBAJAJFINANCE (Ref#1014150436)",
      "amount": "-2600.00",
      "category": "Loan",
      "balance": "37400.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "8266",
      "narration": "EAW-524254XXXXXX9417-CWAC3730-HMUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "27400.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "3869",
      "narration": "EAW-524254XXXXXX9417-CWAN3730-MUMBAI",
      "amount": "-6000.00",
      "category": "Cash Withdrawal",
      "balance": "21400.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "6019",
      "narration": "ATW-524254XXXXXX9417-S1ACMU25-MUMBAI",
      "amount": "-15000.00",
      "category": "Cash Withdrawal",
      "balance": "6400.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "4701",
      "narration": "EAW-524254XXXXXX9417-TTCC1348-EMUMBAI",
      "amount": "-400.00",
      "category": "Cash Withdrawal",
      "balance": "6000.00"
    },
    {
      "date": "10/15/2015 00:00:00",
      "chqNo": "",
      "narration": "REF-IDEA-EHDF4056479972-",
      "amount": "350.00",
      "category": "Reversal",
      "balance": "6350.00"
    },
    {
      "date": "10/16/2015 00:00:00",
      "chqNo": "",
      "narration": "104735129210870/CITRUSSHOPCLUES (Ref#1016101816)",
      "amount": "-218.00",
      "category": "Online Shopping",
      "balance": "6132.00"
    },
    {
      "date": "10/16/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(3TXN)",
      "amount": "-68.40",
      "category": "Bank Charges",
      "balance": "6063.60"
    },
    {
      "date": "10/16/2015 00:00:00",
      "chqNo": "140336",
      "narration": "POS 524254XXXXXX9417 VIKAS AUTO ACCES PO S DEBIT",
      "amount": "-1080.00",
      "category": "Others",
      "balance": "4983.60"
    },
    {
      "date": "10/17/2015 00:00:00",
      "chqNo": "2526",
      "narration": "EAW-524254XXXXXX9417-AECN2190-AMUMBAI",
      "amount": "-1900.00",
      "category": "Cash Withdrawal",
      "balance": "3083.60"
    },
    {
      "date": "10/17/2015 00:00:00",
      "chqNo": "5327",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "2083.60"
    },
    {
      "date": "10/19/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "2060.80"
    },
    {
      "date": "10/20/2015 00:00:00",
      "chqNo": "",
      "narration": "EAW DECCHG CARDEND 9417 HDFC BANK LIMITED (Ref#121015)",
      "amount": "-28.50",
      "category": "Bank Charges",
      "balance": "2032.30"
    },
    {
      "date": "10/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN) (Ref#151015)",
      "amount": "-7.11",
      "category": "Bank Charges",
      "balance": "2025.19"
    },
    {
      "date": "10/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN) (Ref#151015)",
      "amount": "-45.60",
      "category": "Bank Charges",
      "balance": "1979.59"
    },
    {
      "date": "10/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM NON CASH(1TXN) (Ref#151015)",
      "amount": "-9.69",
      "category": "Others",
      "balance": "1969.90"
    },
    {
      "date": "10/21/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXMB152945466474 (Ref#AXMB152945466474)",
      "amount": "1700.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "3669.90"
    },
    {
      "date": "10/22/2015 00:00:00",
      "chqNo": "5975",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-3600.00",
      "category": "Cash Withdrawal",
      "balance": "69.90"
    },
    {
      "date": "10/29/2015 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGAON WES",
      "amount": "20000.00",
      "category": "Cash Deposit",
      "balance": "20069.90"
    },
    {
      "date": "10/29/2015 00:00:00",
      "chqNo": "8352",
      "narration": "ATW-524254XXXXXX9417-S1ACMB55-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "18069.90"
    },
    {
      "date": "10/29/2015 00:00:00",
      "chqNo": "7802",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "8069.90"
    },
    {
      "date": "10/30/2015 00:00:00",
      "chqNo": "1583",
      "narration": "NWD-524254XXXXXX9417-SACWF642-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "7069.90"
    },
    {
      "date": "10/31/2015 00:00:00",
      "chqNo": "",
      "narration": "454793673/PAYUBOOKMYSHOW (Ref#1031123633)",
      "amount": "-105.83",
      "category": "Entertainment",
      "balance": "6964.07"
    },
    {
      "date": "10/31/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "6941.27"
    },
    {
      "date": "10/31/2015 00:00:00",
      "chqNo": "5778",
      "narration": "ATW-524254XXXXXX9417-S1ACMM10-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "4941.27"
    },
    {
      "date": "11/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-08075011-MUMBAI (Ref#530513017294)",
      "amount": "-2900.00",
      "category": "Cash Withdrawal",
      "balance": "2041.27"
    },
    {
      "date": "11/01/2015 00:00:00",
      "chqNo": "1884",
      "narration": "NWD-524254XXXXXX9417-SACWF642-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "1541.27"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "8861",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "1041.27"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-1208.73"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "1041.27"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "1018.47"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-ATM0036-MUMBAI (Ref#530619995514)",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "518.47"
    },
    {
      "date": "11/03/2015 00:00:00",
      "chqNo": "5088",
      "narration": "NWD-524254XXXXXX9417-02054830-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "18.47"
    },
    {
      "date": "11/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-2231.53"
    },
    {
      "date": "11/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "18.47"
    },
    {
      "date": "11/18/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-2231.53"
    },
    {
      "date": "11/18/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "18.47"
    },
    {
      "date": "12/01/2015 00:00:00",
      "chqNo": "72344",
      "narration": "CHQ DEP - MICR 8 CLEARING - MUMBAI CLEAR",
      "amount": "70000.00",
      "category": "Transfer in",
      "balance": "70018.47"
    },
    {
      "date": "12/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "67768.47"
    },
    {
      "date": "12/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "70018.47"
    },
    {
      "date": "12/02/2015 00:00:00",
      "chqNo": "",
      "narration": "CHQ DEP RET- INSUFFICIENT FUNDS",
      "amount": "-70000.00",
      "category": "Bounced O/W Cheque",
      "balance": "18.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGAON WES",
      "amount": "49000.00",
      "category": "Cash Deposit",
      "balance": "49018.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4152729729/RELIANCE ENERGY LTD (Ref#1204171323)",
      "amount": "-2750.00",
      "category": "Utilities",
      "balance": "46268.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4153066719/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#1204201241)",
      "amount": "-297.00",
      "category": "Utilities",
      "balance": "45971.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "6223",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-20000.00",
      "category": "Cash Withdrawal",
      "balance": "25971.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "4946",
      "narration": "ATW-524254XXXXXX9417-S1ACMU25-MUMBAI",
      "amount": "-3000.00",
      "category": "Cash Withdrawal",
      "balance": "22971.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "4947",
      "narration": "ATW-524254XXXXXX9417-S1ACMU25-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "21971.47"
    },
    {
      "date": "12/05/2015 00:00:00",
      "chqNo": "",
      "narration": "477820748/PAYUBOOKMYSHOW (Ref#1205102525)",
      "amount": "-144.89",
      "category": "Entertainment",
      "balance": "21826.58"
    },
    {
      "date": "12/05/2015 00:00:00",
      "chqNo": "5259",
      "narration": "EAW-524254XXXXXX9417-DWDC1062-MUMBAI",
      "amount": "-5000.00",
      "category": "Cash Withdrawal",
      "balance": "16826.58"
    },
    {
      "date": "12/05/2015 00:00:00",
      "chqNo": "662",
      "narration": "POS 524254XXXXXX9417 FOOD BAZAAR POS DEB IT",
      "amount": "-1005.00",
      "category": "Groceries",
      "balance": "15821.58"
    },
    {
      "date": "12/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "13571.58"
    },
    {
      "date": "12/06/2015 00:00:00",
      "chqNo": "271776",
      "narration": "POS 524254XXXXXX9417 JYOTI COLLECTION PO S DEBIT",
      "amount": "-1050.00",
      "category": "Others",
      "balance": "12521.58"
    },
    {
      "date": "12/06/2015 00:00:00",
      "chqNo": "1347",
      "narration": "POS 524254XXXXXX9417 VINOD JEWELLERS, PO S DEBIT",
      "amount": "-3700.00",
      "category": "Jewelry",
      "balance": "8821.58"
    },
    {
      "date": "12/08/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#534215989511)",
      "amount": "-1800.00",
      "category": "Cash Withdrawal",
      "balance": "7021.58"
    },
    {
      "date": "12/11/2015 00:00:00",
      "chqNo": "4930",
      "narration": "ATW-524254XXXXXX9417-P3AWMT46-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "5021.58"
    },
    {
      "date": "12/11/2015 00:00:00",
      "chqNo": "",
      "narration": "CHQ DEP RETURN CHARGES-0 021215",
      "amount": "-114.50",
      "category": "Bank Charges",
      "balance": "4907.08"
    },
    {
      "date": "12/11/2015 00:00:00",
      "chqNo": "240146",
      "narration": "POS 524254XXXXXX9417 NATIONAL TELECOM PO S DEBIT",
      "amount": "-1400.00",
      "category": "Others",
      "balance": "3507.08"
    },
    {
      "date": "12/12/2015 00:00:00",
      "chqNo": "",
      "narration": "482573378/PAYUBOOKMYSHOW (Ref#1212074633)",
      "amount": "-334.36",
      "category": "Entertainment",
      "balance": "3172.72"
    },
    {
      "date": "12/13/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4168923141/VODAFONE BILLDESK (Ref#1213073458)",
      "amount": "-100.00",
      "category": "Utilities",
      "balance": "3072.72"
    },
    {
      "date": "12/13/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4168925365/VODAFONE BILLDESK (Ref#1213074052)",
      "amount": "-100.00",
      "category": "Utilities",
      "balance": "2972.72"
    },
    {
      "date": "12/13/2015 00:00:00",
      "chqNo": "531326",
      "narration": "POS 524254XXXXXX9417 MAXUS CINEMA BOR PO S DEBIT",
      "amount": "-465.00",
      "category": "Entertainment",
      "balance": "2507.72"
    },
    {
      "date": "12/14/2015 00:00:00",
      "chqNo": "5393",
      "narration": "EAW-524254XXXXXX9417-APCN0631-GR MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "1507.72"
    },
    {
      "date": "12/15/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "1484.82"
    },
    {
      "date": "12/17/2015 00:00:00",
      "chqNo": "",
      "narration": "REF-HUTCH-EHDF4168923141-",
      "amount": "100.00",
      "category": "Reversal",
      "balance": "1584.82"
    },
    {
      "date": "12/17/2015 00:00:00",
      "chqNo": "9935",
      "narration": "ATW-524254XXXXXX9417-P1ENPY02-MUMBAI",
      "amount": "-400.00",
      "category": "Cash Withdrawal",
      "balance": "1184.82"
    },
    {
      "date": "12/18/2015 00:00:00",
      "chqNo": "8067",
      "narration": "ATW-524254XXXXXX9417-P3AWMT46-MUMBAI",
      "amount": "-1100.00",
      "category": "Cash Withdrawal",
      "balance": "84.82"
    },
    {
      "date": "12/19/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "61.92"
    },
    {
      "date": "12/21/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "39.02"
    },
    {
      "date": "01/04/2016 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGAON EAS",
      "amount": "7500.00",
      "category": "Cash Deposit",
      "balance": "7539.02"
    },
    {
      "date": "01/14/2016 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGOAN EAS",
      "amount": "20000.00",
      "category": "Cash Deposit",
      "balance": "27539.02"
    },
    {
      "date": "01/14/2016 00:00:00",
      "chqNo": "409217",
      "narration": "POS 524254XXXXXX9417 VODAFONE INDIA L PO HDFC BANK LIMITED",
      "amount": "-1292.00",
      "category": "Utilities",
      "balance": "26247.02"
    },
    {
      "date": "01/14/2016 00:00:00",
      "chqNo": "",
      "narration": "EHDF4235049117/IDEA (Ref#160145594717)",
      "amount": "-30.00",
      "category": "Others",
      "balance": "26217.02"
    },
    {
      "date": "01/15/2016 00:00:00",
      "chqNo": "",
      "narration": "EHDF4235377162/IDEA (Ref#160155688407)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "26117.02"
    },
    {
      "date": "01/16/2016 00:00:00",
      "chqNo": "81982",
      "narration": "CHQ PAID-MICR CTS-MU-SATPURA NAGARI NIWA",
      "amount": "-7100.00",
      "category": "Transfer to SATPURA NAGARI NIWA",
      "balance": "19017.02"
    },
    {
      "date": "01/16/2016 00:00:00",
      "chqNo": "",
      "narration": "199579116/TECHBIG TREE ENTERTA (Ref#160166228395)",
      "amount": "-334.36",
      "category": "Others",
      "balance": "18682.66"
    },
    {
      "date": "01/16/2016 00:00:00",
      "chqNo": "744767",
      "narration": "POS 524254XXXXXX9417 HARDCASTLE RESTA PO S DEBIT",
      "amount": "-208.91",
      "category": "Others",
      "balance": "18473.75"
    },
    {
      "date": "01/16/2016 00:00:00",
      "chqNo": "2160",
      "narration": "ATW-524254XXXXXX9417-S1ACMU25-MUMBAI",
      "amount": "-2400.00",
      "category": "Cash Withdrawal",
      "balance": "16073.75"
    },
    {
      "date": "01/20/2016 00:00:00",
      "chqNo": "",
      "narration": "EHDF4246640233/VODAFONE BILLDESK (Ref#160208867188)",
      "amount": "-297.00",
      "category": "Utilities",
      "balance": "15776.75"
    },
    {
      "date": "01/21/2016 00:00:00",
      "chqNo": "86",
      "narration": "ATW-524254XXXXXX9417-S1ANMI64-MUMBAI",
      "amount": "-2500.00",
      "category": "Cash Withdrawal",
      "balance": "13276.75"
    },
    {
      "date": "01/22/2016 00:00:00",
      "chqNo": "",
      "narration": "511708479/PAYUIBIBOWEBPVTLTD (Ref#160229524553)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "13176.75"
    },
    {
      "date": "01/22/2016 00:00:00",
      "chqNo": "831",
      "narration": "POS 524254XXXXXX9417 MC DONALDS POS DEBI T",
      "amount": "-291.22",
      "category": "Food",
      "balance": "12885.53"
    },
    {
      "date": "01/22/2016 00:00:00",
      "chqNo": "3083",
      "narration": "ATW-524254XXXXXX9417-P3AWBL15-MUMBAI",
      "amount": "-800.00",
      "category": "Cash Withdrawal",
      "balance": "12085.53"
    },
    {
      "date": "01/23/2016 00:00:00",
      "chqNo": "2916",
      "narration": "NWD-524254XXXXXX9417-S1CW4684-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "11085.53"
    },
    {
      "date": "01/23/2016 00:00:00",
      "chqNo": "2904",
      "narration": "POS 524254XXXXXX9417 BABA SWEET MART POS DEBIT",
      "amount": "-375.00",
      "category": "Others",
      "balance": "10710.53"
    },
    {
      "date": "01/26/2016 00:00:00",
      "chqNo": "237418",
      "narration": "POS 524254XXXXXX9417 POONAM TEA CENTE PO S DEBIT",
      "amount": "-424.00",
      "category": "Others",
      "balance": "10286.53"
    },
    {
      "date": "01/28/2016 00:00:00",
      "chqNo": "7453",
      "narration": "POS 524254XXXXXX9417 AMAR JEET CHEMIS PO S DEBIT",
      "amount": "-170.00",
      "category": "Others",
      "balance": "10116.53"
    },
    {
      "date": "01/30/2016 00:00:00",
      "chqNo": "5384",
      "narration": "ATW-524254XXXXXX9417-S1ANMM43-MUMBAI",
      "amount": "-3000.00",
      "category": "Cash Withdrawal",
      "balance": "7116.53"
    },
    {
      "date": "01/31/2016 00:00:00",
      "chqNo": "",
      "narration": "202840029/TECHBIG TREE ENTERTA (Ref#160314628126)",
      "amount": "-111.45",
      "category": "Others",
      "balance": "7005.08"
    },
    {
      "date": "01/31/2016 00:00:00",
      "chqNo": "",
      "narration": "519398065/PAYUIBIBOWEBPVTLTD (Ref#160314872396)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "6905.08"
    },
    {
      "date": "01/31/2016 00:00:00",
      "chqNo": "1365",
      "narration": "EAW-524254XXXXXX9417-AECN2191-MUMBAI",
      "amount": "-800.00",
      "category": "Cash Withdrawal",
      "balance": "6105.08"
    },
    {
      "date": "02/02/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160332787565 (Ref#AXIR160332787565)",
      "amount": "100.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "6205.08"
    },
    {
      "date": "02/02/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160332856470 (Ref#AXIR160332856470)",
      "amount": "9900.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "16105.08"
    },
    {
      "date": "02/06/2016 00:00:00",
      "chqNo": "2917",
      "narration": "EAW-524254XXXXXX9417-AECN2191-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "15105.08"
    },
    {
      "date": "02/07/2016 00:00:00",
      "chqNo": "3089",
      "narration": "EAW-524254XXXXXX9417-AECN2191-MUMBAI",
      "amount": "-5000.00",
      "category": "Cash Withdrawal",
      "balance": "10105.08"
    },
    {
      "date": "02/07/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4278195546/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#160388981459)",
      "amount": "-18.00",
      "category": "Utilities",
      "balance": "10087.08"
    },
    {
      "date": "02/07/2016 00:00:00",
      "chqNo": "591830",
      "narration": "POS 524254XXXXXX9417 TINY BABY POS DEBIT",
      "amount": "-1840.00",
      "category": "Others",
      "balance": "8247.08"
    },
    {
      "date": "02/08/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT DR-ORBC0105142-MRS ANITA VIJAY POL- NETBANK, MUM-N039160127876371 (Ref#N039160127876371)",
      "amount": "-100.00",
      "category": "Transfer to MRS ANITA VIJAY POL",
      "balance": "8147.08"
    },
    {
      "date": "02/08/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160394322218 (Ref#AXIR160394322218)",
      "amount": "10000.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "18147.08"
    },
    {
      "date": "02/08/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT DR-ORBC0105142-MRS ANITA VIJAY POL- NETBANK, MUM-N039160127922308 (Ref#N039160127922308)",
      "amount": "-10400.00",
      "category": "Transfer to MRS ANITA VIJAY POL",
      "balance": "7747.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "524830834/PAYUINDIA (Ref#160400224031)",
      "amount": "-564.00",
      "category": "Others",
      "balance": "7183.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160404627716 (Ref#AXIR160404627716)",
      "amount": "1300.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "8483.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4282844242/IDEA (Ref#160400437070)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "8383.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4282859690/VODAFONE BILLDESK (Ref#160400442033)",
      "amount": "-100.00",
      "category": "Utilities",
      "balance": "8283.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "REF-HUTCH-FHDF4278195546-",
      "amount": "18.00",
      "category": "Reversal",
      "balance": "8301.08"
    },
    {
      "date": "02/10/2016 00:00:00",
      "chqNo": "5477",
      "narration": "EAW-524254XXXXXX9417-AECN2190-AMUMBAI",
      "amount": "-7000.00",
      "category": "Cash Withdrawal",
      "balance": "1301.08"
    },
    {
      "date": "02/12/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "1278.18"
    },
    {
      "date": "02/12/2016 00:00:00",
      "chqNo": "8331",
      "narration": "NWD-524254XXXXXX9417-SN004814-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "278.18"
    },
    {
      "date": "02/12/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4289973926/VODAFONE BILLDESK (Ref#160432596736)",
      "amount": "-78.00",
      "category": "Utilities",
      "balance": "200.18"
    },
    {
      "date": "02/13/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4290333689/VODAFONE BILLDESK (Ref#160442689724)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "98.18"
    },
    {
      "date": "02/15/2016 00:00:00",
      "chqNo": "",
      "narration": "529290373/PAYUIBIBOWEBPVTLTD (Ref#160463622716)",
      "amount": "-80.00",
      "category": "Others",
      "balance": "18.18"
    },
    {
      "date": "02/15/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CHGS INCL ST & CESS (Ref#80216)",
      "amount": "-8.59",
      "category": "Bank Charges",
      "balance": "9.59"
    },
    {
      "date": "02/15/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-9.59",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "02/15/2016 00:00:00",
      "chqNo": "",
      "narration": "REF-HUTCH-FHDF4289973926-",
      "amount": "78.00",
      "category": "Reversal",
      "balance": "78.00"
    },
    {
      "date": "02/18/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160496299751 (Ref#AXIR160496299751)",
      "amount": "4500.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "4578.00"
    },
    {
      "date": "02/18/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4300873416/BILLDKVODAFONEINDIAL (Ref#160495690609)",
      "amount": "-2000.00",
      "category": "Utilities",
      "balance": "2578.00"
    },
    {
      "date": "02/19/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4302724051/VODAFONE BILLDESK (Ref#160506164102)",
      "amount": "-18.00",
      "category": "Utilities",
      "balance": "2560.00"
    },
    {
      "date": "02/19/2016 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-08075037-MUMBAI (Ref#605013001911)",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "2060.00"
    },
    {
      "date": "02/20/2016 00:00:00",
      "chqNo": "",
      "narration": "207463027/TECHBIG TREE ENTERTA (Ref#160516615141)",
      "amount": "-291.78",
      "category": "Others",
      "balance": "1768.22"
    },
    {
      "date": "02/20/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) (Ref#160216)",
      "amount": "-13.31",
      "category": "Bank Charges",
      "balance": "1754.91"
    },
    {
      "date": "02/20/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "1732.01"
    },
    {
      "date": "02/20/2016 00:00:00",
      "chqNo": "2130",
      "narration": "NWD-524254XXXXXX9417-SPCND467-MUMBAI",
      "amount": "-700.00",
      "category": "Cash Withdrawal",
      "balance": "1032.01"
    },
    {
      "date": "02/21/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4306316368/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#160527057406)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "930.01"
    },
    {
      "date": "02/21/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4306318751/VODAFONE BILLDESK (Ref#160527056345)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "828.01"
    },
    {
      "date": "02/21/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4306601349/VODAFONE BILLDESK (Ref#160527142657)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "726.01"
    },
    {
      "date": "02/21/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4307668959/VODAFONE BILLDESK (Ref#160527374897)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "624.01"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160536749366 (Ref#AXIR160536749366)",
      "amount": "7500.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "8124.01"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "234",
      "narration": "NWD-524254XXXXXX9417-SN004814-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "7124.01"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "100000442263472/IRCTC_NEW (Ref#160537722666)",
      "amount": "-362.90",
      "category": "Travel",
      "balance": "6761.11"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "100000442263472/IRCTC_NEW (Ref#160537722666)",
      "amount": "-11.45",
      "category": "Travel",
      "balance": "6749.66"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "REF-HUTCH-FHDF4302724051-",
      "amount": "18.00",
      "category": "Reversal",
      "balance": "6767.66"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "6744.76"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "534324027/PAYUIBIBOWEBPVTLTD (Ref#160537996214)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "6644.76"
    },
    {
      "date": "02/23/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "6621.86"
    }
    ],
     "recurringExpenses": [
      {
      "category": "Loan",
      "monthwiseBreakups": [
        {
          "month": "2015-06",
          "amount": "2250.00"
        },
        {
          "month": "2015-07",
          "amount": "2250.00"
        },
        {
          "month": "2015-08",
          "amount": "2250.00"
        },
        {
          "month": "2015-09",
          "amount": "2250.00"
        },
        {
          "month": "2015-10",
          "amount": "2600.00"
        },
        {
          "month": "2015-12",
          "amount": "2250.00"
        }
      ],
      "tranasactions": [
        {
          "date": "06/02/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        },
        {
          "date": "07/02/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        },
        {
          "date": "08/03/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        },
        {
          "date": "09/02/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        },
        {
          "date": "10/14/2015 00:00:00",
          "chqNo": "",
          "narration": "EHDF4063567190/BILLDESKBAJAJFINANCE (Ref#1014150436)",
          "amount": "2600.00",
          "category": null,
          "balance": null
        },
        {
          "date": "12/05/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        }
      ]
    },
    {
      "category": "Utilities",
      "monthwiseBreakups": [
        {
          "month": "2015-06",
          "amount": "706.61"
        },
        {
          "month": "2015-08",
          "amount": "572.28"
        },
        {
          "month": "2015-09",
          "amount": "728.00"
        },
        {
          "month": "2015-10",
          "amount": "1393.00"
        },
        {
          "month": "2015-12",
          "amount": "3147.00"
        },
        {
          "month": "2016-01",
          "amount": "1589.00"
        },
        {
          "month": "2016-02",
          "amount": "2610.00"
        }
      ],
      "tranasactions": [
        {
          "date": "06/15/2015 00:00:00",
          "chqNo": "",
          "narration": "161927130/TECHTATASKY (Ref#615154246)",
          "amount": "200.00",
          "category": null,
          "balance": null
        },
        {
          "date": "06/30/2015 00:00:00",
          "chqNo": "",
          "narration": "DHDF3879170439/BILLDKVODAFONEINDIAL (Ref#630193324)",
          "amount": "506.61",
          "category": null,
          "balance": null
        },
        {
          "date": "08/04/2015 00:00:00",
          "chqNo": "",
          "narration": "DHDF3943044886/BILLDKVODAFONEINDIAL (Ref#804115625)",
          "amount": "572.28",
          "category": null,
          "balance": null
        },
        {
          "date": "09/01/2015 00:00:00",
          "chqNo": "311914",
          "narration": "POS 5242540100259417 VODAFONE INDIA L PO S DEBIT",
          "amount": "728.00",
          "category": null,
          "balance": null
        },
        {
          "date": "10/09/2015 00:00:00",
          "chqNo": "595397",
          "narration": "POS 524254XXXXXX9417 VODAFONE INDIA L PO S DEBIT",
          "amount": "1393.00",
          "category": null,
          "balance": null
        },
        {
          "date": "12/04/2015 00:00:00",
          "chqNo": "",
          "narration": "EHDF4152729729/RELIANCE ENERGY LTD (Ref#1204171323)",
          "amount": "2750.00",
          "category": null,
          "balance": null
        },
        {
          "date": "12/04/2015 00:00:00",
          "chqNo": "",
          "narration": "EHDF4153066719/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#1204201241)",
          "amount": "297.00",
          "category": null,
          "balance": null
        },
        {
          "date": "12/13/2015 00:00:00",
          "chqNo": "",
          "narration": "EHDF4168923141/VODAFONE BILLDESK (Ref#1213073458)",
          "amount": "100.00",
          "category": null,
          "balance": null
        },
        {
          "date": "01/14/2016 00:00:00",
          "chqNo": "409217",
          "narration": "POS 524254XXXXXX9417 VODAFONE INDIA L PO HDFC BANK LIMITED",
          "amount": "1292.00",
          "category": null,
          "balance": null
        },
        {
          "date": "01/20/2016 00:00:00",
          "chqNo": "",
          "narration": "EHDF4246640233/VODAFONE BILLDESK (Ref#160208867188)",
          "amount": "297.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/09/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4282859690/VODAFONE BILLDESK (Ref#160400442033)",
          "amount": "100.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/13/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4290333689/VODAFONE BILLDESK (Ref#160442689724)",
          "amount": "102.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/18/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4300873416/BILLDKVODAFONEINDIAL (Ref#160495690609)",
          "amount": "2000.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/21/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4306316368/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#160527057406)",
          "amount": "102.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/21/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4306318751/VODAFONE BILLDESK (Ref#160527056345)",
          "amount": "102.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/21/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4306601349/VODAFONE BILLDESK (Ref#160527142657)",
          "amount": "102.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/21/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4307668959/VODAFONE BILLDESK (Ref#160527374897)",
          "amount": "102.00",
          "category": null,
          "balance": null
        }
      ]
    }
   ],
   "referenceNumber": "a750bf80d4f64c25a1dbcbe76e0fe4ef"
    }
   ```  
##### Schema
   // Start Transaction Response Object
   ```
    {
       "perfiosTransactionId": "ae44da2f2f1b463b94789f1fda1384f4",
       "transactionId": "ae44da2f2f1b463b94789f1fda1384f4",
       "referencenumber": "63623520bcb74764bf788a26da073c4a"
     }

   ```  
// Upload Statement Response Object
   ```
    {
       "statementId": "1234",
      "referenceNumber": "f176c2a4d3ed4106bd256c4d50bef998"
           
    }
   ``` 
   //Complete Transaction Response Object
   ```
    {
       "expiration": null,
       "data": null,
       "name": "perfios",
       "entityId": "1",
       "entityType": "application",
       "syndicationData": null,
       "syndicationName": "perfios",
       "request": {
         "perfiosTransactionId": "f176c2a4d3ed4106bd256c4d50bef998"
       },
      "response": "Transaction completed successfully"
           
    }
   ```
   // Transaction Status Response Object
   ```
    {
       "status": "success",
      "errorCode":"E_NO_ERROR",
      "reason":"",
      "parts":"1",
      "processing":"completed",
      "files":"available",
      "referenceNumber":"f176c2a4d3ed4106bd256c4d50bef998"
           
    }
   ```
  // delete data Response Object
   ```
    {
      "status": "success",
      "referenceNumber":"f176c2a4d3ed4106bd256c4d50bef998"
           
    }
   ```
   // Review of A Perfios Transaction Response Object
   ```
    {
      "status": "success",
      "referenceNumber":"f176c2a4d3ed4106bd256c4d50bef998"
           
    }
   ```
    // List supported-instition Response Object
   ```
    {
     "instiutions": [
     {
      "id": "3",
      "name": "ADCB, India",
      "institutionType": "bank"
     },
     {
      "id": "998",
      "name": "Acme Bank Ltd., India",
      "institutionType": "bank"
     },
     {
      "id": "4",
      "name": "Allahabad Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "5",
      "name": "American Express Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "6",
      "name": "Andhra Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "2",
      "name": "Axis Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "7",
      "name": "BNP Paribas, India",
      "institutionType": "bank"
     },
     {
      "id": "156",
      "name": "Bandhan Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "8",
      "name": "Bank of Baroda, India",
      "institutionType": "bank"
     },
     {
      "id": "9",
      "name": "Bank of India, India",
      "institutionType": "bank"
     },
     {
      "id": "10",
      "name": "Bank of Maharashtra, India",
      "institutionType": "bank"
     },
     {
      "id": "73",
      "name": "Barclays Bank, India",
      "institutionType": "bank"
     },
     {
      "id": "11",
      "name": "Canara Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "111",
      "name": "Catholic Syrian Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "12",
      "name": "Central Bank of India, India",
      "institutionType": "bank"
    },
    {
      "id": "14",
      "name": "Citibank, India",
      "institutionType": "bank"
    },
    {
      "id": "57",
      "name": "City Union Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "15",
      "name": "Corporation Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "83",
      "name": "Cosmos Co-op. Bank Ltd., India",
      "institutionType": "bank"
    },
    {
      "id": "16",
      "name": "DBS Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "17",
      "name": "Dena Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "49",
      "name": "Deutsche Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "18",
      "name": "Development Credit Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "71",
      "name": "Dhanalakshmi Bank Ltd., India",
      "institutionType": "bank"
    },
    {
      "id": "19",
      "name": "Federal Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "152",
      "name": "Fin Growth Co-Op bank Ltd., India, India",
      "institutionType": "bank"
    },
    {
      "id": "20",
      "name": "HDFC Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "21",
      "name": "HSBC, India",
      "institutionType": "bank"
    },
    {
      "id": "74",
      "name": "ICICI Bank B2 (Branch Free Banking), India",
      "institutionType": "bank"
    },
    {
      "id": "22",
      "name": "ICICI Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "23",
      "name": "IDBI, India",
      "institutionType": "bank"
    },
    {
      "id": "140",
      "name": "IDFC Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "24",
      "name": "Indian Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "25",
      "name": "Indian Overseas Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "26",
      "name": "IndusInd Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "27",
      "name": "J&K Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "51",
      "name": "Karnataka Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "50",
      "name": "Karur Vysya Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "28",
      "name": "Kotak Mahindra / Ing Vysya Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "77",
      "name": "Lakshmi Vilas Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "29",
      "name": "Oriental Bank of Commerce, India",
      "institutionType": "bank"
    },
    {
      "id": "30",
      "name": "Punjab National Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "31",
      "name": "Punjab and Sind Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "81",
      "name": "RBL (Ratnakar) Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "48",
      "name": "RBS (ABN AMRO), India",
      "institutionType": "bank"
    },
    {
      "id": "72",
      "name": "Saraswat Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "52",
      "name": "South Indian Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "32",
      "name": "Standard Chartered Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "33",
      "name": "State Bank of Bikaner and Jaipur, India",
      "institutionType": "bank"
    },
    {
      "id": "34",
      "name": "State Bank of Hyderabad, India",
      "institutionType": "bank"
    },
    {
      "id": "35",
      "name": "State Bank of India, India",
      "institutionType": "bank"
    },
    {
      "id": "36",
      "name": "State Bank of Indore, India",
      "institutionType": "bank"
    },
    {
      "id": "37",
      "name": "State Bank of Mysore, India",
      "institutionType": "bank"
    },
    {
      "id": "38",
      "name": "State Bank of Patiala, India",
      "institutionType": "bank"
    },
    {
      "id": "39",
      "name": "State Bank of Saurashtra, India",
      "institutionType": "bank"
    },
    {
      "id": "40",
      "name": "State Bank of Travancore, India",
      "institutionType": "bank"
    },
    {
      "id": "41",
      "name": "Syndicate Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "90",
      "name": "TJSB Sahakari Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "75",
      "name": "Tamilnad Mercantile Bank Ltd., India",
      "institutionType": "bank"
    },
    {
      "id": "42",
      "name": "UCO Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "43",
      "name": "Union Bank of India, India",
      "institutionType": "bank"
    },
    {
      "id": "44",
      "name": "United Bank of India, India",
      "institutionType": "bank"
    },
    {
      "id": "45",
      "name": "Vijaya Bank, India",
      "institutionType": "bank"
    },
    {
      "id": "46",
      "name": "Yes Bank, India",
      "institutionType": "bank"
    }
   ],
    "referenceNumber": "6eb2500860ec49fe86743a981e1e7767"
           
    }
   ```
   // PDF report response Object
   ```
   {
     "report": "f176c2a4d3ed4106bd256c4d50bef998f176c2a4d3ed4106bd256c4d50bef998f176c2a4d3ed4106bd256c4d50bef998f176c2a4d3ed4106bd256c4d50bef998",
    "referenceNumber":"f176c2a4d3ed4106bd256c4d50bef998"
   }
   ```
   // xml report response Object
   ```
   {
      "startDate": "01/01/2015 00:00:00",
       "endDate": "08/31/2016 00:00:00",
       "durationInMonths": "20",
       "bank": "HDFC Bank, India",
       "accountNo": "15731140009913",
       "accountType": "",
       "customerInfo":
        {
            "name": "GEETA RAHUL SHARMA",
            "address": "HYDERABAD INDIA",
            "landline": "",
            "mobile": "022-61606161",
            "email": "geetars1981@gmail.com",
            "pan": ""
        },
      "statementSummary":
        {
         "avgBalanceOf6Dates": "8954.71",
         "avgDailyBalance": "8302.96",
         "avgInvestmentExpensePerMonth": "0.00",
         "avgInvestmentIncomePerMonth": "4.10",
         "avgMonthlyExpense": "1000.55",
    "avgMonthlyIncome": "34562.45",
    "avgMonthlySurplus": "596.10",
    "closingBalance": "6621.86",
    "expenseToIncomeRatio": "0.45",
    "highestExpenseInAMonth": null,
    "highestIncomeInAMonth": "49000.00",
    "highestInvestmentExpenseInAMonth": "0.00",
    "highestInvestmentIncomeInAMonth": "82.00",
    "highestSavingsInAMonth": "12175.24",
    "inwardChqBounces": "0",
    "inwardEcsBounces": "0",
    "inwardOtherBounces": "0",
    "lastMonthExpense": "0.00",
    "lastMonthIncome": "0.00",
    "lastMonthInvestmentExpense": "0.00",
    "lastMonthInvestmentIncome": "0.00",
    "lastMonthSavings": "0.00",
    "lowestExpenseInAMonth": "2.81",
    "lowestIncomeInAMonth": "60.00",
    "lowestInvestmentExpenseInAMonth": "0.00",
    "lowestInvestmentIncomeInAMonth": "82.00",
    "lowestSavingsInAMonth": "-21292.27",
    "maxBalance": "70018.47",
    "maxCredit": "70000.00",
    "maxDebit": "70000.00",
    "minBalance": "0.00",
    "minCredit": "18.00",
    "minDebit": "2.81",
    "numberOfTransactions": "287",
    "openingBalance": "18432.62",
    "outwardChqBounces": "0",
    "outwardEcsBounces": "0",
    "outwardOtherBounces": "0",
    "salaryCredits": "4"
    },
    "transactions": [
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3828843347/IDEA (Ref#601100304)",
      "amount": "-140.00",
      "category": "Others",
      "balance": "18292.62"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "",
      "narration": "REF-PAYUBOOKMY-345103748-",
      "amount": "111.24",
      "category": "Reversal",
      "balance": "18403.86"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "1469",
      "narration": "NWD-5242540100259417-S1CW6195-MUBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "8403.86"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "1469",
      "narration": "NWD-5242540100259417-S1CW6195-MUBAI",
      "amount": "10000.00",
      "category": "Reversal",
      "balance": "18403.86"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "7722",
      "narration": "NWD-5242540100259417-S1CW4684-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "8403.86"
    },
    {
      "date": "06/01/2015 00:00:00",
      "chqNo": "7723",
      "narration": "NWD-5242540100259417-S1CW4684-MUMBAI",
      "amount": "-5000.00",
      "category": "Cash Withdrawal",
      "balance": "3403.86"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CHGS INCL ST & CESS (Ref#300515)",
      "amount": "-2.81",
      "category": "Bank Charges",
      "balance": "3401.05"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.47",
      "category": "Bank Charges",
      "balance": "3378.58"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM NON CASH(1TXN)",
      "amount": "-9.55",
      "category": "Others",
      "balance": "3369.03"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "1119.03"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.47",
      "category": "Bank Charges",
      "balance": "1096.56"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3831667658/IDEA (Ref#602210144)",
      "amount": "-200.00",
      "category": "Others",
      "balance": "896.56"
    },
    {
      "date": "06/02/2015 00:00:00",
      "chqNo": "820",
      "narration": "ATW-5242540100259417-S1ACMM76-MUMBAI",
      "amount": "-400.00",
      "category": "Cash Withdrawal",
      "balance": "496.56"
    },
    {
      "date": "06/04/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR151554200964 (Ref#AXIR151554200964)",
      "amount": "2700.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "3196.56"
    },
    {
      "date": "06/05/2015 00:00:00",
      "chqNo": "",
      "narration": "PMJBY339656",
      "amount": "-330.00",
      "category": "Others",
      "balance": "2866.56"
    },
    {
      "date": "06/05/2015 00:00:00",
      "chqNo": "1384",
      "narration": "ATW-5242540100259417-S1ACMM76-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "1866.56"
    },
    {
      "date": "06/06/2015 00:00:00",
      "chqNo": "969557",
      "narration": "POS 5242540100259417 D MART POS DEBIT",
      "amount": "-596.00",
      "category": "Household",
      "balance": "1270.56"
    },
    {
      "date": "06/12/2015 00:00:00",
      "chqNo": "",
      "narration": "352281774/PAYUBOOKMYSHOW (Ref#612123621)",
      "amount": "-89.12",
      "category": "Entertainment",
      "balance": "1181.44"
    },
    {
      "date": "06/12/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3847457708/IDEA (Ref#612152157)",
      "amount": "-50.00",
      "category": "Others",
      "balance": "1131.44"
    },
    {
      "date": "06/12/2015 00:00:00",
      "chqNo": "3442",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-200.00",
      "category": "Cash Withdrawal",
      "balance": "931.44"
    },
    {
      "date": "06/15/2015 00:00:00",
      "chqNo": "",
      "narration": "161927130/TECHTATASKY (Ref#615154246)",
      "amount": "-200.00",
      "category": "Utilities",
      "balance": "731.44"
    },
    {
      "date": "06/19/2015 00:00:00",
      "chqNo": "1073",
      "narration": "NWD-5242540100259417-SPCND467-MUMBAI",
      "amount": "-700.00",
      "category": "Cash Withdrawal",
      "balance": "31.44"
    },
    {
      "date": "06/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "8.64"
    },
    {
      "date": "06/30/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-HSBC0400002-SALARY PROCESSING - EPAY ACC-CHETAN GORE-HSBCN15181981785 (Ref#HSBCN15181981785)",
      "amount": "52546.00",
      "category": "Salary",
      "balance": "32554.64"
    },
    {
      "date": "06/30/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3879170439/BILLDKVODAFONEINDIAL (Ref#630193324)",
      "amount": "-506.61",
      "category": "Utilities",
      "balance": "32048.03"
    },
    {
      "date": "06/30/2015 00:00:00",
      "chqNo": "6485",
      "narration": "ATW-5242540100259417-S1ANMI64-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "22048.03"
    },
    {
      "date": "06/30/2015 00:00:00",
      "chqNo": "6486",
      "narration": "ATW-5242540100259417-S1ANMI64-MUMBAI",
      "amount": "-2500.00",
      "category": "Cash Withdrawal",
      "balance": "19548.03"
    },
    {
      "date": "07/01/2015 00:00:00",
      "chqNo": "2771",
      "narration": "POS 5242540100259417 BIG BAZAAR. POS DEB",
      "amount": "-613.00",
      "category": "Household",
      "balance": "18935.03"
    },
    {
      "date": "07/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT DR-UTIB0000373-CHETAN GORE-NETBANK, MUM-N182150078915389 (Ref#N182150078915389)",
      "amount": "-500.00",
      "category": "Transfer to CHETAN GORE",
      "balance": "18435.03"
    },
    {
      "date": "07/01/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3880588179/IDEA (Ref#701141700)",
      "amount": "-140.00",
      "category": "Others",
      "balance": "18295.03"
    },
    {
      "date": "07/02/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT DR-UTIB0000373-CHETAN GORE-NETBANK, MUM-N183150079168962 (Ref#N183150079168962)",
      "amount": "-500.00",
      "category": "Transfer to CHETAN GORE",
      "balance": "17795.03"
    },
    {
      "date": "07/02/2015 00:00:00",
      "chqNo": "3034",
      "narration": "POS 5242540100259417 BIG BAZAAR. POS DEB IT",
      "amount": "-453.00",
      "category": "Household",
      "balance": "17342.03"
    },
    {
      "date": "07/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "15092.03"
    },
    {
      "date": "07/03/2015 00:00:00",
      "chqNo": "6539",
      "narration": "NWD-5242540100259417-SN003312-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "14592.03"
    },
    {
      "date": "07/03/2015 00:00:00",
      "chqNo": "6540",
      "narration": "NWD-5242540100259417-SN003312-MUMBAI",
      "amount": "-2500.00",
      "category": "Cash Withdrawal",
      "balance": "12092.03"
    },
    {
      "date": "07/04/2015 00:00:00",
      "chqNo": "2538",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-6000.00",
      "category": "Cash Withdrawal",
      "balance": "6092.03"
    },
    {
      "date": "07/08/2015 00:00:00",
      "chqNo": "2815",
      "narration": "POS 5242540100259417 BIG BAZAAR. POS DEB IT",
      "amount": "-197.00",
      "category": "Household",
      "balance": "5895.03"
    },
    {
      "date": "07/08/2015 00:00:00",
      "chqNo": "1908",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-1800.00",
      "category": "Cash Withdrawal",
      "balance": "4095.03"
    },
    {
      "date": "07/10/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CHGS INCL ST & CESS (Ref#10715)",
      "amount": "-2.85",
      "category": "Bank Charges",
      "balance": "4092.18"
    },
    {
      "date": "07/10/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CHGS INCL ST & CESS (Ref#20715)",
      "amount": "-2.85",
      "category": "Bank Charges",
      "balance": "4089.33"
    },
    {
      "date": "07/10/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "4066.53"
    },
    {
      "date": "07/15/2015 00:00:00",
      "chqNo": "8253",
      "narration": "NWD-5242540100259417-SN003312-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "3066.53"
    },
    {
      "date": "07/15/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3905243994/IDEA (Ref#715143607)",
      "amount": "-50.00",
      "category": "Others",
      "balance": "3016.53"
    },
    {
      "date": "07/17/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "2993.73"
    },
    {
      "date": "07/17/2015 00:00:00",
      "chqNo": "",
      "narration": "374419962/PAYUBOOKMYSHOW (Ref#717143640)",
      "amount": "-106.94",
      "category": "Entertainment",
      "balance": "2886.79"
    },
    {
      "date": "07/19/2015 00:00:00",
      "chqNo": "6930",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "2386.79"
    },
    {
      "date": "07/19/2015 00:00:00",
      "chqNo": "1477",
      "narration": "POS 5242540100259417 HOTEL NITYANAND POS DEBIT",
      "amount": "-505.00",
      "category": "Others",
      "balance": "1881.79"
    },
    {
      "date": "07/20/2015 00:00:00",
      "chqNo": "3134",
      "narration": "ATW-5242540100259417-S1ANMI71-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "881.79"
    },
    {
      "date": "07/21/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "858.99"
    },
    {
      "date": "07/23/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR152040438382 (Ref#AXIR152040438382)",
      "amount": "1900.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "2758.99"
    },
    {
      "date": "07/24/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3922347277/IDEA (Ref#724111657)",
      "amount": "-50.00",
      "category": "Others",
      "balance": "2708.99"
    },
    {
      "date": "07/25/2015 00:00:00",
      "chqNo": "1668",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "2208.99"
    },
    {
      "date": "07/25/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3923959791/BOOKMYSHOW COM HDFC BANK LIMITED (Ref#725083015)",
      "amount": "-434.46",
      "category": "Entertainment",
      "balance": "1774.53"
    },
    {
      "date": "07/25/2015 00:00:00",
      "chqNo": "6198",
      "narration": "NWD-5242540100259417-SPCND467-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "774.53"
    },
    {
      "date": "07/26/2015 00:00:00",
      "chqNo": "",
      "narration": "381915803/PAYUBOOKMYSHOW (Ref#726090334)",
      "amount": "-115.86",
      "category": "Entertainment",
      "balance": "658.67"
    },
    {
      "date": "07/26/2015 00:00:00",
      "chqNo": "7816",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-400.00",
      "category": "Cash Withdrawal",
      "balance": "258.67"
    },
    {
      "date": "07/27/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN)",
      "amount": "-45.60",
      "category": "Bank Charges",
      "balance": "213.07"
    },
    {
      "date": "07/28/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "190.27"
    },
    {
      "date": "07/29/2015 00:00:00",
      "chqNo": "2372",
      "narration": "ATW-5242540100259417-P3AWBL15-MUMBAI",
      "amount": "-100.00",
      "category": "Cash Withdrawal",
      "balance": "90.27"
    },
    {
      "date": "07/30/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3934068894/IDEA (Ref#730114018)",
      "amount": "-50.00",
      "category": "Others",
      "balance": "40.27"
    },
    {
      "date": "07/30/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3934073816/IDEA (Ref#730114231)",
      "amount": "-30.00",
      "category": "Others",
      "balance": "10.27"
    },
    {
      "date": "07/31/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-HSBC0400002-SALARY PROCESSING - EPAY ACC-CHETAN GORE-HSBCN15212130937 (Ref#HSBCN15212130937)",
      "amount": "52713.00",
      "category": "Salary",
      "balance": "31723.27"
    },
    {
      "date": "08/01/2015 00:00:00",
      "chqNo": "8801",
      "narration": "NWD-5242540100259417-SN003308-MUMBAI",
      "amount": "-2500.00",
      "category": "Cash Withdrawal",
      "balance": "29223.27"
    },
    {
      "date": "08/01/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3938767447/IDEA (Ref#801211637)",
      "amount": "-200.00",
      "category": "Others",
      "balance": "29023.27"
    },
    {
      "date": "08/01/2015 00:00:00",
      "chqNo": "8472",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "19023.27"
    },
    {
      "date": "08/01/2015 00:00:00",
      "chqNo": "8473",
      "narration": "NWD-5242540100259417-SPCNF278-MUMBAI",
      "amount": "-7000.00",
      "category": "Cash Withdrawal",
      "balance": "12023.27"
    },
    {
      "date": "08/02/2015 00:00:00",
      "chqNo": "4101",
      "narration": "EAW-5242540100259417-CWAC0290-MUMBAI",
      "amount": "-1500.00",
      "category": "Cash Withdrawal",
      "balance": "10523.27"
    },
    {
      "date": "08/02/2015 00:00:00",
      "chqNo": "1307",
      "narration": "ATW-5242540100259417-S1ACMU25-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "9523.27"
    },
    {
      "date": "08/02/2015 00:00:00",
      "chqNo": "715",
      "narration": "POS 5242540100259417 BABA SWEET MART POS DEBIT",
      "amount": "-439.00",
      "category": "Others",
      "balance": "9084.27"
    },
    {
      "date": "08/03/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "6834.27"
    },
    {
      "date": "08/03/2015 00:00:00",
      "chqNo": "1744",
      "narration": "POS 5242540100259417 BIG BAZAAR. POS DEB IT",
      "amount": "-240.00",
      "category": "Household",
      "balance": "6594.27"
    },
    {
      "date": "08/04/2015 00:00:00",
      "chqNo": "1613",
      "narration": "ATW-5242540100259417-P3AWMT46-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "6094.27"
    },
    {
      "date": "08/04/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3943044886/BILLDKVODAFONEINDIAL (Ref#804115625)",
      "amount": "-572.28",
      "category": "Utilities",
      "balance": "5521.99"
    },
    {
      "date": "08/05/2015 00:00:00",
      "chqNo": "6582",
      "narration": "ATW-5242540100259417-S1ANMB09-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "5021.99"
    },
    {
      "date": "08/05/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "4999.19"
    },
    {
      "date": "08/07/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR152193001689 (Ref#AXIR152193001689)",
      "amount": "1950.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "6949.19"
    },
    {
      "date": "08/07/2015 00:00:00",
      "chqNo": "5445",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-900.00",
      "category": "Cash Withdrawal",
      "balance": "6049.19"
    },
    {
      "date": "08/09/2015 00:00:00",
      "chqNo": "",
      "narration": "392558594/PAYUBOOKMYSHOW (Ref#809074727)",
      "amount": "-94.69",
      "category": "Entertainment",
      "balance": "5954.50"
    },
    {
      "date": "08/09/2015 00:00:00",
      "chqNo": "6053",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-900.00",
      "category": "Cash Withdrawal",
      "balance": "5054.50"
    },
    {
      "date": "08/10/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "5031.70"
    },
    {
      "date": "08/11/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) HDFC BANK LIMITED",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "5008.90"
    },
    {
      "date": "08/12/2015 00:00:00",
      "chqNo": "81977",
      "narration": "CHQ PAID-MICR CTS-MU-RELIANCE INFRASTRAT",
      "amount": "-4000.00",
      "category": "Transfer to RELIANCE INFRASTRAT",
      "balance": "1008.90"
    },
    {
      "date": "08/12/2015 00:00:00",
      "chqNo": "5996",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "8.90"
    },
    {
      "date": "08/13/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-8.90",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "08/17/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXMB152294484860 (Ref#AXMB152294484860)",
      "amount": "3000.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "3000.00"
    },
    {
      "date": "08/17/2015 00:00:00",
      "chqNo": "",
      "narration": "398392885/PAYUBOOKMYSHOW (Ref#817110507)",
      "amount": "-94.03",
      "category": "Entertainment",
      "balance": "2905.97"
    },
    {
      "date": "08/17/2015 00:00:00",
      "chqNo": "81978",
      "narration": "CHQ PAID - GOREGOAN EAS",
      "amount": "-800.00",
      "category": "Others",
      "balance": "2105.97"
    },
    {
      "date": "08/19/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM NON CASH(1TXN)",
      "amount": "-9.69",
      "category": "Others",
      "balance": "2096.28"
    },
    {
      "date": "08/20/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3972800655/IDEA (Ref#820100919)",
      "amount": "-120.00",
      "category": "Others",
      "balance": "1976.28"
    },
    {
      "date": "08/20/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3972805551/IDEA (Ref#820101204)",
      "amount": "-98.00",
      "category": "Others",
      "balance": "1878.28"
    },
    {
      "date": "08/20/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF3972808542/IDEA (Ref#820101349)",
      "amount": "-75.00",
      "category": "Others",
      "balance": "1803.28"
    },
    {
      "date": "08/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) (Ref#130815)",
      "amount": "-13.90",
      "category": "Bank Charges",
      "balance": "1789.38"
    },
    {
      "date": "08/21/2015 00:00:00",
      "chqNo": "6993",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-700.00",
      "category": "Cash Withdrawal",
      "balance": "1089.38"
    },
    {
      "date": "08/23/2015 00:00:00",
      "chqNo": "9959",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "589.38"
    },
    {
      "date": "08/24/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "566.58"
    },
    {
      "date": "08/24/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "543.78"
    },
    {
      "date": "08/25/2015 00:00:00",
      "chqNo": "7241",
      "narration": "NWD-5242540100259417-SN003315-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "43.78"
    },
    {
      "date": "08/25/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXMB152375761856 (Ref#AXMB152375761856)",
      "amount": "60.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "103.78"
    },
    {
      "date": "08/26/2015 00:00:00",
      "chqNo": "1334",
      "narration": "ATW-5242540100259417-S1ANMB09-MUMBAI",
      "amount": "-100.00",
      "category": "Cash Withdrawal",
      "balance": "3.78"
    },
    {
      "date": "08/26/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-3.78",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-HSBC0400002-SALARY PROCESSING - EPAY ACC-CHETAN GORE-HSBCN15243032650 (Ref#HSBCN15243032650)",
      "amount": "52713.00",
      "category": "Salary",
      "balance": "31713.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "545",
      "narration": "POS 5242540100259417 BIG BAZAAR POS DEBI T",
      "amount": "-1282.00",
      "category": "Household",
      "balance": "30431.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "3797",
      "narration": "ATW-5242540100259417-S1ANMI71-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "20431.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "3798",
      "narration": "ATW-5242540100259417-S1ANMI71-MUMBAI",
      "amount": "-9000.00",
      "category": "Cash Withdrawal",
      "balance": "11431.00"
    },
    {
      "date": "08/31/2015 00:00:00",
      "chqNo": "7295",
      "narration": "ATW-5242540100259417-S1ACMU25-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "10431.00"
    },
    {
      "date": "09/01/2015 00:00:00",
      "chqNo": "",
      "narration": "407992034/PAYUBOOKMYSHOW (Ref#901094153)",
      "amount": "-80.21",
      "category": "Entertainment",
      "balance": "10350.79"
    },
    {
      "date": "09/01/2015 00:00:00",
      "chqNo": "311914",
      "narration": "POS 5242540100259417 VODAFONE INDIA L PO S DEBIT",
      "amount": "-728.00",
      "category": "Utilities",
      "balance": "9622.79"
    },
    {
      "date": "09/01/2015 00:00:00",
      "chqNo": "5273",
      "narration": "ATW-5242540100259417-S1ANMI64-MUMBAI HDFC BANK LIMITED",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "8622.79"
    },
    {
      "date": "09/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "6372.79"
    },
    {
      "date": "09/02/2015 00:00:00",
      "chqNo": "2052",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "5372.79"
    },
    {
      "date": "09/03/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) (Ref#280815)",
      "amount": "-19.02",
      "category": "Bank Charges",
      "balance": "5353.77"
    },
    {
      "date": "09/04/2015 00:00:00",
      "chqNo": "2670",
      "narration": "NWD-5242540100259417-SACWF642-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "4353.77"
    },
    {
      "date": "09/04/2015 00:00:00",
      "chqNo": "",
      "narration": "410139517/PAYUBOOKMYSHOW (Ref#904210837)",
      "amount": "-140.36",
      "category": "Entertainment",
      "balance": "4213.41"
    },
    {
      "date": "09/06/2015 00:00:00",
      "chqNo": "3812",
      "narration": "NWD-524254XXXXXX9417-SPCND467-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "3213.41"
    },
    {
      "date": "09/08/2015 00:00:00",
      "chqNo": "8684",
      "narration": "NWD-524254XXXXXX9417-SN003315-MUMBAI",
      "amount": "-1500.00",
      "category": "Cash Withdrawal",
      "balance": "1713.41"
    },
    {
      "date": "09/08/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF4007924308/IDEA (Ref#908154515)",
      "amount": "-150.00",
      "category": "Others",
      "balance": "1563.41"
    },
    {
      "date": "09/09/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "1540.61"
    },
    {
      "date": "09/11/2015 00:00:00",
      "chqNo": "4760",
      "narration": "ATW-524254XXXXXX9417-S1ANMB09-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "1040.61"
    },
    {
      "date": "09/12/2015 00:00:00",
      "chqNo": "6607",
      "narration": "NWD-524254XXXXXX9417-SPCND467-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "540.61"
    },
    {
      "date": "09/14/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXMB152579104307 (Ref#AXMB152579104307)",
      "amount": "1500.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "2040.61"
    },
    {
      "date": "09/15/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "2017.81"
    },
    {
      "date": "09/18/2015 00:00:00",
      "chqNo": "6599",
      "narration": "NWD-524254XXXXXX9417-SACWF642-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "1517.81"
    },
    {
      "date": "09/19/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#526208989357)",
      "amount": "-1400.00",
      "category": "Cash Withdrawal",
      "balance": "117.81"
    },
    {
      "date": "09/19/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#526208989358)",
      "amount": "-100.00",
      "category": "Cash Withdrawal",
      "balance": "17.81"
    },
    {
      "date": "09/19/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-17.81",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "09/28/2015 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP MAROL - MUMB",
      "amount": "2000.00",
      "category": "Cash Deposit",
      "balance": "2000.00"
    },
    {
      "date": "09/28/2015 00:00:00",
      "chqNo": "",
      "narration": "DHDF4037729215/IDEA (Ref#928130051)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "1900.00"
    },
    {
      "date": "09/29/2015 00:00:00",
      "chqNo": "6842",
      "narration": "EAW-524254XXXXXX9417-AECN2190-AMUMBAI",
      "amount": "-800.00",
      "category": "Cash Withdrawal",
      "balance": "1100.00"
    },
    {
      "date": "09/29/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) (Ref#210915)",
      "amount": "-4.99",
      "category": "Bank Charges",
      "balance": "1095.01"
    },
    {
      "date": "09/29/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN) (Ref#220915)",
      "amount": "-45.60",
      "category": "Bank Charges",
      "balance": "1049.41"
    },
    {
      "date": "09/30/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "1026.61"
    },
    {
      "date": "10/01/2015 00:00:00",
      "chqNo": "",
      "narration": "CREDIT INTEREST CAPITALISED",
      "amount": "82.00",
      "category": "Interest",
      "balance": "1108.61"
    },
    {
      "date": "10/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#527408989396)",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "608.61"
    },
    {
      "date": "10/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#527408989397)",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "108.61"
    },
    {
      "date": "10/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#527416989566)",
      "amount": "-100.00",
      "category": "Cash Withdrawal",
      "balance": "8.61"
    },
    {
      "date": "10/03/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-2241.39"
    },
    {
      "date": "10/03/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "8.61"
    },
    {
      "date": "10/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-2241.39"
    },
    {
      "date": "10/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253698879 HDFC BANK LIMITED",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "8.61"
    },
    {
      "date": "10/09/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-HSBC0400002-LINTAS I P L SALARY ACCOUNT-CHETAN GORE-HSBCN15282837860 (Ref#HSBCN15282837860)",
      "amount": "52585.00",
      "category": "Salary",
      "balance": "10593.61"
    },
    {
      "date": "10/09/2015 00:00:00",
      "chqNo": "595397",
      "narration": "POS 524254XXXXXX9417 VODAFONE INDIA L PO S DEBIT",
      "amount": "-1393.00",
      "category": "Utilities",
      "balance": "9200.61"
    },
    {
      "date": "10/09/2015 00:00:00",
      "chqNo": "8961",
      "narration": "EAW-524254XXXXXX9417-DWDW1062-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "7200.61"
    },
    {
      "date": "10/09/2015 00:00:00",
      "chqNo": "40870",
      "narration": "POS 524254XXXXXX9417 MORRIS POS DEBIT",
      "amount": "-600.00",
      "category": "Others",
      "balance": "6600.61"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4056476401/IDEA (Ref#1010071819)",
      "amount": "-154.00",
      "category": "Others",
      "balance": "6446.61"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4056479972/IDEA (Ref#1010072653)",
      "amount": "-350.00",
      "category": "Others",
      "balance": "6096.61"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "435706119/PAYUBOOKMYSHOW (Ref#1010080614)",
      "amount": "-380.99",
      "category": "Entertainment",
      "balance": "5715.62"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4056519363/IDEA (Ref#1010082604)",
      "amount": "-209.00",
      "category": "Others",
      "balance": "5506.62"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "690090",
      "narration": "POS 524254XXXXXX9417 HARDCASTLE RESTA PO S DEBIT",
      "amount": "-141.13",
      "category": "Others",
      "balance": "5365.49"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "6641",
      "narration": "NWD-524254XXXXXX9417-SN003308-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "3365.49"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "9288",
      "narration": "EAW-524254XXXXXX9417-DWDW1062-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "1365.49"
    },
    {
      "date": "10/10/2015 00:00:00",
      "chqNo": "",
      "narration": "436511024/PAYUBOOKMYSHOW (Ref#1010222609)",
      "amount": "-127.00",
      "category": "Entertainment",
      "balance": "1238.49"
    },
    {
      "date": "10/11/2015 00:00:00",
      "chqNo": "7826",
      "narration": "EAW-524254XXXXXX9417-DWDC1062-MUMBAI",
      "amount": "-1200.00",
      "category": "Cash Withdrawal",
      "balance": "38.49"
    },
    {
      "date": "10/12/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN)",
      "amount": "-38.49",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGAON WES",
      "amount": "40000.00",
      "category": "Cash Deposit",
      "balance": "40000.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4063567190/BILLDESKBAJAJFINANCE (Ref#1014150436)",
      "amount": "-2600.00",
      "category": "Loan",
      "balance": "37400.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "8266",
      "narration": "EAW-524254XXXXXX9417-CWAC3730-HMUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "27400.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "3869",
      "narration": "EAW-524254XXXXXX9417-CWAN3730-MUMBAI",
      "amount": "-6000.00",
      "category": "Cash Withdrawal",
      "balance": "21400.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "6019",
      "narration": "ATW-524254XXXXXX9417-S1ACMU25-MUMBAI",
      "amount": "-15000.00",
      "category": "Cash Withdrawal",
      "balance": "6400.00"
    },
    {
      "date": "10/14/2015 00:00:00",
      "chqNo": "4701",
      "narration": "EAW-524254XXXXXX9417-TTCC1348-EMUMBAI",
      "amount": "-400.00",
      "category": "Cash Withdrawal",
      "balance": "6000.00"
    },
    {
      "date": "10/15/2015 00:00:00",
      "chqNo": "",
      "narration": "REF-IDEA-EHDF4056479972-",
      "amount": "350.00",
      "category": "Reversal",
      "balance": "6350.00"
    },
    {
      "date": "10/16/2015 00:00:00",
      "chqNo": "",
      "narration": "104735129210870/CITRUSSHOPCLUES (Ref#1016101816)",
      "amount": "-218.00",
      "category": "Online Shopping",
      "balance": "6132.00"
    },
    {
      "date": "10/16/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(3TXN)",
      "amount": "-68.40",
      "category": "Bank Charges",
      "balance": "6063.60"
    },
    {
      "date": "10/16/2015 00:00:00",
      "chqNo": "140336",
      "narration": "POS 524254XXXXXX9417 VIKAS AUTO ACCES PO S DEBIT",
      "amount": "-1080.00",
      "category": "Others",
      "balance": "4983.60"
    },
    {
      "date": "10/17/2015 00:00:00",
      "chqNo": "2526",
      "narration": "EAW-524254XXXXXX9417-AECN2190-AMUMBAI",
      "amount": "-1900.00",
      "category": "Cash Withdrawal",
      "balance": "3083.60"
    },
    {
      "date": "10/17/2015 00:00:00",
      "chqNo": "5327",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "2083.60"
    },
    {
      "date": "10/19/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "2060.80"
    },
    {
      "date": "10/20/2015 00:00:00",
      "chqNo": "",
      "narration": "EAW DECCHG CARDEND 9417 HDFC BANK LIMITED (Ref#121015)",
      "amount": "-28.50",
      "category": "Bank Charges",
      "balance": "2032.30"
    },
    {
      "date": "10/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN) (Ref#151015)",
      "amount": "-7.11",
      "category": "Bank Charges",
      "balance": "2025.19"
    },
    {
      "date": "10/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(2TXN) (Ref#151015)",
      "amount": "-45.60",
      "category": "Bank Charges",
      "balance": "1979.59"
    },
    {
      "date": "10/20/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM NON CASH(1TXN) (Ref#151015)",
      "amount": "-9.69",
      "category": "Others",
      "balance": "1969.90"
    },
    {
      "date": "10/21/2015 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXMB152945466474 (Ref#AXMB152945466474)",
      "amount": "1700.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "3669.90"
    },
    {
      "date": "10/22/2015 00:00:00",
      "chqNo": "5975",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-3600.00",
      "category": "Cash Withdrawal",
      "balance": "69.90"
    },
    {
      "date": "10/29/2015 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGAON WES",
      "amount": "20000.00",
      "category": "Cash Deposit",
      "balance": "20069.90"
    },
    {
      "date": "10/29/2015 00:00:00",
      "chqNo": "8352",
      "narration": "ATW-524254XXXXXX9417-S1ACMB55-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "18069.90"
    },
    {
      "date": "10/29/2015 00:00:00",
      "chqNo": "7802",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-10000.00",
      "category": "Cash Withdrawal",
      "balance": "8069.90"
    },
    {
      "date": "10/30/2015 00:00:00",
      "chqNo": "1583",
      "narration": "NWD-524254XXXXXX9417-SACWF642-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "7069.90"
    },
    {
      "date": "10/31/2015 00:00:00",
      "chqNo": "",
      "narration": "454793673/PAYUBOOKMYSHOW (Ref#1031123633)",
      "amount": "-105.83",
      "category": "Entertainment",
      "balance": "6964.07"
    },
    {
      "date": "10/31/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "6941.27"
    },
    {
      "date": "10/31/2015 00:00:00",
      "chqNo": "5778",
      "narration": "ATW-524254XXXXXX9417-S1ACMM10-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "4941.27"
    },
    {
      "date": "11/01/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-08075011-MUMBAI (Ref#530513017294)",
      "amount": "-2900.00",
      "category": "Cash Withdrawal",
      "balance": "2041.27"
    },
    {
      "date": "11/01/2015 00:00:00",
      "chqNo": "1884",
      "narration": "NWD-524254XXXXXX9417-SACWF642-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "1541.27"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "8861",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "1041.27"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-1208.73"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "1041.27"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.80",
      "category": "Bank Charges",
      "balance": "1018.47"
    },
    {
      "date": "11/02/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-ATM0036-MUMBAI (Ref#530619995514)",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "518.47"
    },
    {
      "date": "11/03/2015 00:00:00",
      "chqNo": "5088",
      "narration": "NWD-524254XXXXXX9417-02054830-MUMBAI",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "18.47"
    },
    {
      "date": "11/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-2231.53"
    },
    {
      "date": "11/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "18.47"
    },
    {
      "date": "11/18/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "-2231.53"
    },
    {
      "date": "11/18/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "18.47"
    },
    {
      "date": "12/01/2015 00:00:00",
      "chqNo": "72344",
      "narration": "CHQ DEP - MICR 8 CLEARING - MUMBAI CLEAR",
      "amount": "70000.00",
      "category": "Transfer in",
      "balance": "70018.47"
    },
    {
      "date": "12/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "67768.47"
    },
    {
      "date": "12/02/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "2250.00",
      "category": "Bounced I/W ECS",
      "balance": "70018.47"
    },
    {
      "date": "12/02/2015 00:00:00",
      "chqNo": "",
      "narration": "CHQ DEP RET- INSUFFICIENT FUNDS",
      "amount": "-70000.00",
      "category": "Bounced O/W Cheque",
      "balance": "18.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGAON WES",
      "amount": "49000.00",
      "category": "Cash Deposit",
      "balance": "49018.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4152729729/RELIANCE ENERGY LTD (Ref#1204171323)",
      "amount": "-2750.00",
      "category": "Utilities",
      "balance": "46268.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4153066719/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#1204201241)",
      "amount": "-297.00",
      "category": "Utilities",
      "balance": "45971.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "6223",
      "narration": "ATW-524254XXXXXX9417-S1ANMI71-MUMBAI",
      "amount": "-20000.00",
      "category": "Cash Withdrawal",
      "balance": "25971.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "4946",
      "narration": "ATW-524254XXXXXX9417-S1ACMU25-MUMBAI",
      "amount": "-3000.00",
      "category": "Cash Withdrawal",
      "balance": "22971.47"
    },
    {
      "date": "12/04/2015 00:00:00",
      "chqNo": "4947",
      "narration": "ATW-524254XXXXXX9417-S1ACMU25-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "21971.47"
    },
    {
      "date": "12/05/2015 00:00:00",
      "chqNo": "",
      "narration": "477820748/PAYUBOOKMYSHOW (Ref#1205102525)",
      "amount": "-144.89",
      "category": "Entertainment",
      "balance": "21826.58"
    },
    {
      "date": "12/05/2015 00:00:00",
      "chqNo": "5259",
      "narration": "EAW-524254XXXXXX9417-DWDC1062-MUMBAI",
      "amount": "-5000.00",
      "category": "Cash Withdrawal",
      "balance": "16826.58"
    },
    {
      "date": "12/05/2015 00:00:00",
      "chqNo": "662",
      "narration": "POS 524254XXXXXX9417 FOOD BAZAAR POS DEB IT",
      "amount": "-1005.00",
      "category": "Groceries",
      "balance": "15821.58"
    },
    {
      "date": "12/05/2015 00:00:00",
      "chqNo": "",
      "narration": "BAJAJFINEMI4050CD07738253-698879",
      "amount": "-2250.00",
      "category": "Loan",
      "balance": "13571.58"
    },
    {
      "date": "12/06/2015 00:00:00",
      "chqNo": "271776",
      "narration": "POS 524254XXXXXX9417 JYOTI COLLECTION PO S DEBIT",
      "amount": "-1050.00",
      "category": "Others",
      "balance": "12521.58"
    },
    {
      "date": "12/06/2015 00:00:00",
      "chqNo": "1347",
      "narration": "POS 524254XXXXXX9417 VINOD JEWELLERS, PO S DEBIT",
      "amount": "-3700.00",
      "category": "Jewelry",
      "balance": "8821.58"
    },
    {
      "date": "12/08/2015 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-03520001-MUMBAI (Ref#534215989511)",
      "amount": "-1800.00",
      "category": "Cash Withdrawal",
      "balance": "7021.58"
    },
    {
      "date": "12/11/2015 00:00:00",
      "chqNo": "4930",
      "narration": "ATW-524254XXXXXX9417-P3AWMT46-MUMBAI",
      "amount": "-2000.00",
      "category": "Cash Withdrawal",
      "balance": "5021.58"
    },
    {
      "date": "12/11/2015 00:00:00",
      "chqNo": "",
      "narration": "CHQ DEP RETURN CHARGES-0 021215",
      "amount": "-114.50",
      "category": "Bank Charges",
      "balance": "4907.08"
    },
    {
      "date": "12/11/2015 00:00:00",
      "chqNo": "240146",
      "narration": "POS 524254XXXXXX9417 NATIONAL TELECOM PO S DEBIT",
      "amount": "-1400.00",
      "category": "Others",
      "balance": "3507.08"
    },
    {
      "date": "12/12/2015 00:00:00",
      "chqNo": "",
      "narration": "482573378/PAYUBOOKMYSHOW (Ref#1212074633)",
      "amount": "-334.36",
      "category": "Entertainment",
      "balance": "3172.72"
    },
    {
      "date": "12/13/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4168923141/VODAFONE BILLDESK (Ref#1213073458)",
      "amount": "-100.00",
      "category": "Utilities",
      "balance": "3072.72"
    },
    {
      "date": "12/13/2015 00:00:00",
      "chqNo": "",
      "narration": "EHDF4168925365/VODAFONE BILLDESK (Ref#1213074052)",
      "amount": "-100.00",
      "category": "Utilities",
      "balance": "2972.72"
    },
    {
      "date": "12/13/2015 00:00:00",
      "chqNo": "531326",
      "narration": "POS 524254XXXXXX9417 MAXUS CINEMA BOR PO S DEBIT",
      "amount": "-465.00",
      "category": "Entertainment",
      "balance": "2507.72"
    },
    {
      "date": "12/14/2015 00:00:00",
      "chqNo": "5393",
      "narration": "EAW-524254XXXXXX9417-APCN0631-GR MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "1507.72"
    },
    {
      "date": "12/15/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "1484.82"
    },
    {
      "date": "12/17/2015 00:00:00",
      "chqNo": "",
      "narration": "REF-HUTCH-EHDF4168923141-",
      "amount": "100.00",
      "category": "Reversal",
      "balance": "1584.82"
    },
    {
      "date": "12/17/2015 00:00:00",
      "chqNo": "9935",
      "narration": "ATW-524254XXXXXX9417-P1ENPY02-MUMBAI",
      "amount": "-400.00",
      "category": "Cash Withdrawal",
      "balance": "1184.82"
    },
    {
      "date": "12/18/2015 00:00:00",
      "chqNo": "8067",
      "narration": "ATW-524254XXXXXX9417-P3AWMT46-MUMBAI",
      "amount": "-1100.00",
      "category": "Cash Withdrawal",
      "balance": "84.82"
    },
    {
      "date": "12/19/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "61.92"
    },
    {
      "date": "12/21/2015 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "39.02"
    },
    {
      "date": "01/04/2016 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGAON EAS",
      "amount": "7500.00",
      "category": "Cash Deposit",
      "balance": "7539.02"
    },
    {
      "date": "01/14/2016 00:00:00",
      "chqNo": "",
      "narration": "CASH DEP GOREGOAN EAS",
      "amount": "20000.00",
      "category": "Cash Deposit",
      "balance": "27539.02"
    },
    {
      "date": "01/14/2016 00:00:00",
      "chqNo": "409217",
      "narration": "POS 524254XXXXXX9417 VODAFONE INDIA L PO HDFC BANK LIMITED",
      "amount": "-1292.00",
      "category": "Utilities",
      "balance": "26247.02"
    },
    {
      "date": "01/14/2016 00:00:00",
      "chqNo": "",
      "narration": "EHDF4235049117/IDEA (Ref#160145594717)",
      "amount": "-30.00",
      "category": "Others",
      "balance": "26217.02"
    },
    {
      "date": "01/15/2016 00:00:00",
      "chqNo": "",
      "narration": "EHDF4235377162/IDEA (Ref#160155688407)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "26117.02"
    },
    {
      "date": "01/16/2016 00:00:00",
      "chqNo": "81982",
      "narration": "CHQ PAID-MICR CTS-MU-SATPURA NAGARI NIWA",
      "amount": "-7100.00",
      "category": "Transfer to SATPURA NAGARI NIWA",
      "balance": "19017.02"
    },
    {
      "date": "01/16/2016 00:00:00",
      "chqNo": "",
      "narration": "199579116/TECHBIG TREE ENTERTA (Ref#160166228395)",
      "amount": "-334.36",
      "category": "Others",
      "balance": "18682.66"
    },
    {
      "date": "01/16/2016 00:00:00",
      "chqNo": "744767",
      "narration": "POS 524254XXXXXX9417 HARDCASTLE RESTA PO S DEBIT",
      "amount": "-208.91",
      "category": "Others",
      "balance": "18473.75"
    },
    {
      "date": "01/16/2016 00:00:00",
      "chqNo": "2160",
      "narration": "ATW-524254XXXXXX9417-S1ACMU25-MUMBAI",
      "amount": "-2400.00",
      "category": "Cash Withdrawal",
      "balance": "16073.75"
    },
    {
      "date": "01/20/2016 00:00:00",
      "chqNo": "",
      "narration": "EHDF4246640233/VODAFONE BILLDESK (Ref#160208867188)",
      "amount": "-297.00",
      "category": "Utilities",
      "balance": "15776.75"
    },
    {
      "date": "01/21/2016 00:00:00",
      "chqNo": "86",
      "narration": "ATW-524254XXXXXX9417-S1ANMI64-MUMBAI",
      "amount": "-2500.00",
      "category": "Cash Withdrawal",
      "balance": "13276.75"
    },
    {
      "date": "01/22/2016 00:00:00",
      "chqNo": "",
      "narration": "511708479/PAYUIBIBOWEBPVTLTD (Ref#160229524553)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "13176.75"
    },
    {
      "date": "01/22/2016 00:00:00",
      "chqNo": "831",
      "narration": "POS 524254XXXXXX9417 MC DONALDS POS DEBI T",
      "amount": "-291.22",
      "category": "Food",
      "balance": "12885.53"
    },
    {
      "date": "01/22/2016 00:00:00",
      "chqNo": "3083",
      "narration": "ATW-524254XXXXXX9417-P3AWBL15-MUMBAI",
      "amount": "-800.00",
      "category": "Cash Withdrawal",
      "balance": "12085.53"
    },
    {
      "date": "01/23/2016 00:00:00",
      "chqNo": "2916",
      "narration": "NWD-524254XXXXXX9417-S1CW4684-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "11085.53"
    },
    {
      "date": "01/23/2016 00:00:00",
      "chqNo": "2904",
      "narration": "POS 524254XXXXXX9417 BABA SWEET MART POS DEBIT",
      "amount": "-375.00",
      "category": "Others",
      "balance": "10710.53"
    },
    {
      "date": "01/26/2016 00:00:00",
      "chqNo": "237418",
      "narration": "POS 524254XXXXXX9417 POONAM TEA CENTE PO S DEBIT",
      "amount": "-424.00",
      "category": "Others",
      "balance": "10286.53"
    },
    {
      "date": "01/28/2016 00:00:00",
      "chqNo": "7453",
      "narration": "POS 524254XXXXXX9417 AMAR JEET CHEMIS PO S DEBIT",
      "amount": "-170.00",
      "category": "Others",
      "balance": "10116.53"
    },
    {
      "date": "01/30/2016 00:00:00",
      "chqNo": "5384",
      "narration": "ATW-524254XXXXXX9417-S1ANMM43-MUMBAI",
      "amount": "-3000.00",
      "category": "Cash Withdrawal",
      "balance": "7116.53"
    },
    {
      "date": "01/31/2016 00:00:00",
      "chqNo": "",
      "narration": "202840029/TECHBIG TREE ENTERTA (Ref#160314628126)",
      "amount": "-111.45",
      "category": "Others",
      "balance": "7005.08"
    },
    {
      "date": "01/31/2016 00:00:00",
      "chqNo": "",
      "narration": "519398065/PAYUIBIBOWEBPVTLTD (Ref#160314872396)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "6905.08"
    },
    {
      "date": "01/31/2016 00:00:00",
      "chqNo": "1365",
      "narration": "EAW-524254XXXXXX9417-AECN2191-MUMBAI",
      "amount": "-800.00",
      "category": "Cash Withdrawal",
      "balance": "6105.08"
    },
    {
      "date": "02/02/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160332787565 (Ref#AXIR160332787565)",
      "amount": "100.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "6205.08"
    },
    {
      "date": "02/02/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160332856470 (Ref#AXIR160332856470)",
      "amount": "9900.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "16105.08"
    },
    {
      "date": "02/06/2016 00:00:00",
      "chqNo": "2917",
      "narration": "EAW-524254XXXXXX9417-AECN2191-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "15105.08"
    },
    {
      "date": "02/07/2016 00:00:00",
      "chqNo": "3089",
      "narration": "EAW-524254XXXXXX9417-AECN2191-MUMBAI",
      "amount": "-5000.00",
      "category": "Cash Withdrawal",
      "balance": "10105.08"
    },
    {
      "date": "02/07/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4278195546/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#160388981459)",
      "amount": "-18.00",
      "category": "Utilities",
      "balance": "10087.08"
    },
    {
      "date": "02/07/2016 00:00:00",
      "chqNo": "591830",
      "narration": "POS 524254XXXXXX9417 TINY BABY POS DEBIT",
      "amount": "-1840.00",
      "category": "Others",
      "balance": "8247.08"
    },
    {
      "date": "02/08/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT DR-ORBC0105142-MRS ANITA VIJAY POL- NETBANK, MUM-N039160127876371 (Ref#N039160127876371)",
      "amount": "-100.00",
      "category": "Transfer to MRS ANITA VIJAY POL",
      "balance": "8147.08"
    },
    {
      "date": "02/08/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160394322218 (Ref#AXIR160394322218)",
      "amount": "10000.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "18147.08"
    },
    {
      "date": "02/08/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT DR-ORBC0105142-MRS ANITA VIJAY POL- NETBANK, MUM-N039160127922308 (Ref#N039160127922308)",
      "amount": "-10400.00",
      "category": "Transfer to MRS ANITA VIJAY POL",
      "balance": "7747.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "524830834/PAYUINDIA (Ref#160400224031)",
      "amount": "-564.00",
      "category": "Others",
      "balance": "7183.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160404627716 (Ref#AXIR160404627716)",
      "amount": "1300.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "8483.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4282844242/IDEA (Ref#160400437070)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "8383.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4282859690/VODAFONE BILLDESK (Ref#160400442033)",
      "amount": "-100.00",
      "category": "Utilities",
      "balance": "8283.08"
    },
    {
      "date": "02/09/2016 00:00:00",
      "chqNo": "",
      "narration": "REF-HUTCH-FHDF4278195546-",
      "amount": "18.00",
      "category": "Reversal",
      "balance": "8301.08"
    },
    {
      "date": "02/10/2016 00:00:00",
      "chqNo": "5477",
      "narration": "EAW-524254XXXXXX9417-AECN2190-AMUMBAI",
      "amount": "-7000.00",
      "category": "Cash Withdrawal",
      "balance": "1301.08"
    },
    {
      "date": "02/12/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "1278.18"
    },
    {
      "date": "02/12/2016 00:00:00",
      "chqNo": "8331",
      "narration": "NWD-524254XXXXXX9417-SN004814-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "278.18"
    },
    {
      "date": "02/12/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4289973926/VODAFONE BILLDESK (Ref#160432596736)",
      "amount": "-78.00",
      "category": "Utilities",
      "balance": "200.18"
    },
    {
      "date": "02/13/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4290333689/VODAFONE BILLDESK (Ref#160442689724)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "98.18"
    },
    {
      "date": "02/15/2016 00:00:00",
      "chqNo": "",
      "narration": "529290373/PAYUIBIBOWEBPVTLTD (Ref#160463622716)",
      "amount": "-80.00",
      "category": "Others",
      "balance": "18.18"
    },
    {
      "date": "02/15/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CHGS INCL ST & CESS (Ref#80216)",
      "amount": "-8.59",
      "category": "Bank Charges",
      "balance": "9.59"
    },
    {
      "date": "02/15/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-9.59",
      "category": "Bank Charges",
      "balance": "0.00"
    },
    {
      "date": "02/15/2016 00:00:00",
      "chqNo": "",
      "narration": "REF-HUTCH-FHDF4289973926-",
      "amount": "78.00",
      "category": "Reversal",
      "balance": "78.00"
    },
    {
      "date": "02/18/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160496299751 (Ref#AXIR160496299751)",
      "amount": "4500.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "4578.00"
    },
    {
      "date": "02/18/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4300873416/BILLDKVODAFONEINDIAL (Ref#160495690609)",
      "amount": "-2000.00",
      "category": "Utilities",
      "balance": "2578.00"
    },
    {
      "date": "02/19/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4302724051/VODAFONE BILLDESK (Ref#160506164102)",
      "amount": "-18.00",
      "category": "Utilities",
      "balance": "2560.00"
    },
    {
      "date": "02/19/2016 00:00:00",
      "chqNo": "",
      "narration": "NWD-524254XXXXXX9417-08075037-MUMBAI (Ref#605013001911)",
      "amount": "-500.00",
      "category": "Cash Withdrawal",
      "balance": "2060.00"
    },
    {
      "date": "02/20/2016 00:00:00",
      "chqNo": "",
      "narration": "207463027/TECHBIG TREE ENTERTA (Ref#160516615141)",
      "amount": "-291.78",
      "category": "Others",
      "balance": "1768.22"
    },
    {
      "date": "02/20/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN) (Ref#160216)",
      "amount": "-13.31",
      "category": "Bank Charges",
      "balance": "1754.91"
    },
    {
      "date": "02/20/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "1732.01"
    },
    {
      "date": "02/20/2016 00:00:00",
      "chqNo": "2130",
      "narration": "NWD-524254XXXXXX9417-SPCND467-MUMBAI",
      "amount": "-700.00",
      "category": "Cash Withdrawal",
      "balance": "1032.01"
    },
    {
      "date": "02/21/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4306316368/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#160527057406)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "930.01"
    },
    {
      "date": "02/21/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4306318751/VODAFONE BILLDESK (Ref#160527056345)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "828.01"
    },
    {
      "date": "02/21/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4306601349/VODAFONE BILLDESK (Ref#160527142657)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "726.01"
    },
    {
      "date": "02/21/2016 00:00:00",
      "chqNo": "",
      "narration": "FHDF4307668959/VODAFONE BILLDESK (Ref#160527374897)",
      "amount": "-102.00",
      "category": "Utilities",
      "balance": "624.01"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "NEFT CR-UTIB0000373-CHETAN DATTATRAY GOR E-CHETAN HDFC-AXIR160536749366 (Ref#AXIR160536749366)",
      "amount": "7500.00",
      "category": "Transfer from CHETAN DATTATRAY GOR E",
      "balance": "8124.01"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "234",
      "narration": "NWD-524254XXXXXX9417-SN004814-MUMBAI",
      "amount": "-1000.00",
      "category": "Cash Withdrawal",
      "balance": "7124.01"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "100000442263472/IRCTC_NEW (Ref#160537722666)",
      "amount": "-362.90",
      "category": "Travel",
      "balance": "6761.11"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "100000442263472/IRCTC_NEW (Ref#160537722666)",
      "amount": "-11.45",
      "category": "Travel",
      "balance": "6749.66"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "REF-HUTCH-FHDF4302724051-",
      "amount": "18.00",
      "category": "Reversal",
      "balance": "6767.66"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "6744.76"
    },
    {
      "date": "02/22/2016 00:00:00",
      "chqNo": "",
      "narration": "534324027/PAYUIBIBOWEBPVTLTD (Ref#160537996214)",
      "amount": "-100.00",
      "category": "Others",
      "balance": "6644.76"
    },
    {
      "date": "02/23/2016 00:00:00",
      "chqNo": "",
      "narration": "FEE-ATM CASH(1TXN)",
      "amount": "-22.90",
      "category": "Bank Charges",
      "balance": "6621.86"
    }
    ],
     "recurringExpenses": [
      {
      "category": "Loan",
      "monthwiseBreakups": [
        {
          "month": "2015-06",
          "amount": "2250.00"
        },
        {
          "month": "2015-07",
          "amount": "2250.00"
        },
        {
          "month": "2015-08",
          "amount": "2250.00"
        },
        {
          "month": "2015-09",
          "amount": "2250.00"
        },
        {
          "month": "2015-10",
          "amount": "2600.00"
        },
        {
          "month": "2015-12",
          "amount": "2250.00"
        }
      ],
      "tranasactions": [
        {
          "date": "06/02/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        },
        {
          "date": "07/02/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        },
        {
          "date": "08/03/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        },
        {
          "date": "09/02/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        },
        {
          "date": "10/14/2015 00:00:00",
          "chqNo": "",
          "narration": "EHDF4063567190/BILLDESKBAJAJFINANCE (Ref#1014150436)",
          "amount": "2600.00",
          "category": null,
          "balance": null
        },
        {
          "date": "12/05/2015 00:00:00",
          "chqNo": "",
          "narration": "BAJAJFINEMI4050CD07738253-698879",
          "amount": "2250.00",
          "category": null,
          "balance": null
        }
      ]
    },
    {
      "category": "Utilities",
      "monthwiseBreakups": [
        {
          "month": "2015-06",
          "amount": "706.61"
        },
        {
          "month": "2015-08",
          "amount": "572.28"
        },
        {
          "month": "2015-09",
          "amount": "728.00"
        },
        {
          "month": "2015-10",
          "amount": "1393.00"
        },
        {
          "month": "2015-12",
          "amount": "3147.00"
        },
        {
          "month": "2016-01",
          "amount": "1589.00"
        },
        {
          "month": "2016-02",
          "amount": "2610.00"
        }
      ],
      "tranasactions": [
        {
          "date": "06/15/2015 00:00:00",
          "chqNo": "",
          "narration": "161927130/TECHTATASKY (Ref#615154246)",
          "amount": "200.00",
          "category": null,
          "balance": null
        },
        {
          "date": "06/30/2015 00:00:00",
          "chqNo": "",
          "narration": "DHDF3879170439/BILLDKVODAFONEINDIAL (Ref#630193324)",
          "amount": "506.61",
          "category": null,
          "balance": null
        },
        {
          "date": "08/04/2015 00:00:00",
          "chqNo": "",
          "narration": "DHDF3943044886/BILLDKVODAFONEINDIAL (Ref#804115625)",
          "amount": "572.28",
          "category": null,
          "balance": null
        },
        {
          "date": "09/01/2015 00:00:00",
          "chqNo": "311914",
          "narration": "POS 5242540100259417 VODAFONE INDIA L PO S DEBIT",
          "amount": "728.00",
          "category": null,
          "balance": null
        },
        {
          "date": "10/09/2015 00:00:00",
          "chqNo": "595397",
          "narration": "POS 524254XXXXXX9417 VODAFONE INDIA L PO S DEBIT",
          "amount": "1393.00",
          "category": null,
          "balance": null
        },
        {
          "date": "12/04/2015 00:00:00",
          "chqNo": "",
          "narration": "EHDF4152729729/RELIANCE ENERGY LTD (Ref#1204171323)",
          "amount": "2750.00",
          "category": null,
          "balance": null
        },
        {
          "date": "12/04/2015 00:00:00",
          "chqNo": "",
          "narration": "EHDF4153066719/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#1204201241)",
          "amount": "297.00",
          "category": null,
          "balance": null
        },
        {
          "date": "12/13/2015 00:00:00",
          "chqNo": "",
          "narration": "EHDF4168923141/VODAFONE BILLDESK (Ref#1213073458)",
          "amount": "100.00",
          "category": null,
          "balance": null
        },
        {
          "date": "01/14/2016 00:00:00",
          "chqNo": "409217",
          "narration": "POS 524254XXXXXX9417 VODAFONE INDIA L PO HDFC BANK LIMITED",
          "amount": "1292.00",
          "category": null,
          "balance": null
        },
        {
          "date": "01/20/2016 00:00:00",
          "chqNo": "",
          "narration": "EHDF4246640233/VODAFONE BILLDESK (Ref#160208867188)",
          "amount": "297.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/09/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4282859690/VODAFONE BILLDESK (Ref#160400442033)",
          "amount": "100.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/13/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4290333689/VODAFONE BILLDESK (Ref#160442689724)",
          "amount": "102.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/18/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4300873416/BILLDKVODAFONEINDIAL (Ref#160495690609)",
          "amount": "2000.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/21/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4306316368/VODAFONE BILLDESK HDFC BANK LIMITED (Ref#160527057406)",
          "amount": "102.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/21/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4306318751/VODAFONE BILLDESK (Ref#160527056345)",
          "amount": "102.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/21/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4306601349/VODAFONE BILLDESK (Ref#160527142657)",
          "amount": "102.00",
          "category": null,
          "balance": null
        },
        {
          "date": "02/21/2016 00:00:00",
          "chqNo": "",
          "narration": "FHDF4307668959/VODAFONE BILLDESK (Ref#160527374897)",
          "amount": "102.00",
          "category": null,
          "balance": null
        }
      ]
    }
   ],
   "referenceNumber": "a750bf80d4f64c25a1dbcbe76e0fe4ef"
   }
   ```
##### Configuration  
1. **Endpoint :**  
**Http GET/POST :** {{default_host}}:{{configuration_port}}/perfios

```
{
   "loanDuration": "6",
  "destination": "statement",
  "apiVersion": "2.0",
  "loanAmount": "0",
  "useProxy": "True",
  "serviceUrls": {
    "startTransactionUrl": "https://demo.perfios.com/KuberaVault/insights/api/statement/start",
    "uploadStatementUrl": "https://demo.perfios.com/KuberaVault/insights/api/statement/upload",
    "completeTransactionUrl": "https://demo.perfios.com/KuberaVault/insights/api/statement/done",
    "transactionStatusUrl": "https://demo.perfios.com/KuberaVault/insights/txnstatus",
    "retrieveReportsandStatementsUrl": "https://demo.perfios.com/KuberaVault/insights/retrieve",
    "deletedataUrl": "https://demo.perfios.com/KuberaVault/insights/delete",
    "requestReviewofAPerfiosTransactionUrl": "https://demo.perfios.com/KuberaVault/insights/review",
    "listofsupportedInstitutionsUrl": "https://demo.perfios.com/KuberaVault/insights/institutions"
  },
  "acceptancePolicy": "atLeastOneTransactionInRange",
  "privateKey": "-----BEGIN RSA PRIVATE KEY-----\r\n\r\n            MIIBOgIBAAJBAIk/YBItZo5OF3zb+lEjiMbrGGEKEThvvLb7L7R+pz6Ft3NVljHi\r\n\r\n            Th0/rp1vpntjcbBYuD250ABBgYZXL8ZL3A8CAwEAAQJAXqjwSPk5P7MKrhpGlknM\r\n\r\n            321sfhlkcSlX3lh2uaWVEiBC8EnrQbuUTFH/Z8DCPwg64jHrpj7ToC6fT9fyuL8S\r\n\r\n            6QIhAMOFW2IzBmhzDIR/MT6QSyGk/ZS4lQCfWq+VYS3iK7eVAiEAs7OMAOaNQJgo\r\n\r\n            3qKcwALSnfNH/EDjn3QCWontbIEZTBMCIFKKcujC57qijy9ETvK9kaozcAYf4m9v\r\n\r\n            1qX3Zx4qtA/9AiAoLld5xBOFhABvd7DRBlCN3N4Vu3SqLMhx8jFSd7NuXQIhAK5h\r\n\r\n            aMJVvxPr0TLgHGcIiDJw8bWOoA9ocvtXAOn5A6CE\r\n\r\n            -----END RSA PRIVATE KEY-----",
  "publickey":"-----BEGIN PUBLIC KEY-----\r\n\r\n            MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIk/YBItZo5OF3zb+lEjiMbrGGEKEThv\r\n\r\n            vLb7L7R+pz6Ft3NVljHiTh0/rp1vpntjcbBYuD250ABBgYZXL8ZL3A8CAwEAAQ==\r\n\r\n            -----END PUBLIC KEY-----",
  "loanType": "Personal",
  "vendorId": "creditExchange"
}
```