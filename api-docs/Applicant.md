##### Available End Points:

1. Add new Applicant

   ```
   - POST 
   - Body (ApplicantRequest)
   - Response: (IApplicant)
   ``` 
2. To get Applicant information by providing Applicant ID

   ```
   - Get [("{applicantId}")]
   - Response: (IApplicant)
   ```
3. To Update the applicant specific details by providing applicant ID

   ```
   - Put [("{applicantId}")]
   - Body (UpdateApplicantRequest)
   - Response: (No content)
   ```	
4. To delete the applicant by providing applicant ID

   ```
   - Delete [("{applicantId}")]
   - Body 
   - Response: (No content)
   ```	
5. Add address for given applicant ID

   ```
   - Post [("{applicantId}/addresses")]
   - Body Address
   - Response: (IApplicant)
   ```
   
6. Update given address for given applicant ID

   ```
   - Put [("{applicantId}/addresses/{addressId}")]
   - Body Address
   - Response: (No content)
   ```
7. Delete given address by providing address id for given applicant id

   ```
   - Delete [("{applicantId}/addresses/{addressId}")]
   - Body 
   - Response: (No content)
   ```
8. Add email id for given applicant id

   ```
   - Post [("{applicantId}/emailaddresses")]
   - Body EmailAddress
   - Response: (IApplicant)
   ```
   
9. Update given email id for given applicant ID

   ```
   - Put [("{applicantid}/emailaddresses/{emailAddressId}")]
   - Body EmailAddress
   - Response: (No content)
   ```
10. Delete given email id  by providing id for given applicant id

   ```
   - Delete [("{applicantid}/emailaddresses/{emailAddressId}")]
   - Body 
   - Response: (No content)
   ```
11. Add phone number for given applicant ID

   ```
   - Post [("{applicantId}/phonenumbers")]
   - Body PhoneNumber
   - Response: (IApplicant)
   ```
   
12. Update given phone number by providing phone id for given applicant ID

   ```
   - Put [("{applicantId}/phonenumbers/{phoneId}")]
   - Body PhoneNumber
   - Response: (No content)
   ```
13. Delete given phone number by providing phone id for given applicant id

   ```
   - Delete [("{applicantId}/phonenumbers/{phoneId}")]
   - Body 
   - Response: (No content)
   ```
   
14. Add employment detail for given applicant ID

   ```
   - Post [("{applicantId}/employments")]
   - Body EmploymentDetail
   - Response: (IApplicant)
   ```
   
15. Update given employment detail by providing employment id for given applicant ID

   ```
   - Put [("{applicantId}/employments/{employmentId}")]
   - Body EmploymentDetail
   - Response: (No content)
   ```
16. Delete given employment detail by providing employment id for given applicant id

   ```
   - Delete [("{applicantId}/employments/{employmentId}")]
   - Body 
   - Response: (No content)
   ```
17. Add bank detail for given applicant ID

   ```
   - Post [("{applicantId}/banks")]
   - Body BankInformation
   - Response: (IApplicant)
   ```
   
18. Update given bank detail by providing bank id for given applicant ID

   ```
   - Put [("{applicantId}/banks/{bankId}")]
   - Body BankInformation
   - Response: (No content)
   ```
19. Delete given employment detail by providing employment id for given applicant id

   ```
   - Delete [("{applicantId}/banks/{bankId}")]
   - Body 
   - Response: (No content)
   ```
20. Update the User ID for the given Applicant ID

   ```
   - Put [("/{applicantId}/associate/user/{userId}")]
   ```
21. To Update the applicant details by providing applicant ID

   ```
   - Put [("updateapplicant/{applicantId}")]
   - Body (Applicant)
			- Response: (IApplicant)
   ```	
22. Update education detail for given applicant id
    ```
   - Put [("{applicantId}/education")]
   - Body :EducationInformation
   - Response: (No content)
   ```	

### Schema

```
// ApplicantRequest
{
    "UserName":"",
    "Password":"",
	"UserId":"application",
    "Salutation":"",
    "FirstName":"",
    "MiddleName":"",
    "LastName":"",
   	"DateOfBirth":"",
	  "emailAddresses": [
    {
	  "id":"589c5f51fda561c9fab4cc85",
      "email": "test@gmail.com",
      "emailType": "Personal",
      "isDefault": true
    }
	],
	"phoneNumbers": [
    {
      "PhoneId":"",
      "phone": "9898989898",
      "countryCode": "91",
      "phoneType": "Mobile",
      "isDefault": false
    }
	],
	"addresses": [
    {
      "addressLine1": "test1",
      "addressLine2": "test3",
      "addressLine3": null,
      "addressLine4": null,
      "landMark": null,
      "city": "Ahmedabad",
      "state": "Gujarat",
      "pinCode": "380034",
      "country": "India",
      "addressType": "Residence",
      "isVerified": false,
      "isDefault": false
    }
	],
	"AadhaarNumber" : "",
	"PermanentAccountNumber" : "",
	"Gender":"",
	"MaritalStatus":"",
	"IncomeInformation":
	[{
		"VerifiedIncome" : "1000000",
		"PaymentFrequency" : "8",
		"IncomeVerifiedDate" : "2016-10-10T15:24:28.2480918+05:30"
	}]
	,
	"bankInformation": 
    [{
    "BankName" : "HDFC",
   	"AccountNumber" : "10004567",
   	"AccountHolderName":"SigmaInfo",
   	"IfscCode" : "Ifsccode100",
   	"AccountType":"Savings",
   	"IsVerified":"false",
	BankAddresses : {
          "addressLine1": "BankAddress1",
          "addressLine2": "BankAddress2",
          "addressLine3": null,
          "addressLine4": null,
          "landMark": null,
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "Bank",
          "isVerified": false
        }
   }],
   "employmentDetails": [
    {
      "addresses": [
        {
          "addressLine1": "Employeertest1",
          "addressLine2": "Employeertest2",
          "addressLine3": "Employeertest3",
          "addressLine4": "Employeertest4",
          "landMark": "Ratna High Street",
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "work",
          "isDefault": true
        }
      ],
      "designation": "Manager",
	  "CinNumber" : "U40101TN2004PTC054931",
      "LengthOfEmploymentInMonths": 60,
      "name": "SIS",
      "workEmail": "TestSigma@gmail.com",
      "EmploymentStatus" : "Salaried",
      "workPhoneNumbers": [
      {
        "PhoneId":"",
		"phone": "9898989898",
        "countryCode": "91",
        "phoneType": "work",
        "isDefault": false
        }
      ],
	"IncomeInformation":
	[{
		"VerifiedIncome" : "1000000",
		"PaymentFrequency" : "8",
		"IncomeVerifiedDate" : "2016-10-10T15:24:28.2480918+05:30"
	}]
    }
  ]
}

// Applicant
{
   "UserId":"application",
    "Salutation":"",
    "FirstName":"",
    "MiddleName":"",
    "LastName":"",
	"DateOfBirth":"",
	"emailAddresses": [
    {
      "id":"589c5f51fda561c9fab4cc86",
      "email": "test@gmail.com",
      "emailType": "Personal",
      "isDefault": true
    }
	],
	"phoneNumbers": [
    {
	  "PhoneId":"",
      "phone": "9898989898",
      "countryCode": "91",
      "phoneType": "Mobile",
      "isDefault": false
    }
	],
	"addresses": [
    {
      "addressLine1": "test1",
      "addressLine2": "test3",
      "addressLine3": null,
      "addressLine4": null,
      "landMark": null,
      "city": "Ahmedabad",
      "state": "Gujarat",
      "pinCode": "380034",
      "country": "India",
      "addressType": "Current",
      "isVerified": false,
      "isDefault": false
    }
	],
	"AadhaarNumber" : "",
	"PermanentAccountNumber" : "",
	"Gender":"",
	"MaritalStatus":"",
	"IncomeInformation":
	[{
		"VerifiedIncome" : "1000000",
		"PaymentFrequency" : "8",
		"IncomeVerifiedDate" : "2016-10-10T15:24:28.2480918+05:30"
	}]
	,
	"bankInformation": 
    [{
    "BankName" : "HDFC",
   	"AccountNumber" : "10004567",
   	"AccountHolderName":"SigmaInfo",
   	"IfscCode" : "Ifsccode100",
   	"AccountType":"Savings",
   	"IsVerified":"false",
	BankAddresses : {
          "addressLine1": "BankAddress1",
          "addressLine2": "BankAddress2",
          "addressLine3": null,
          "addressLine4": null,
          "landMark": null,
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "Bank",
          "isVerified": false
        }
   }],
   "employmentDetails": [
    {
      "addresses": [
        {
          "addressLine1": "Employeertest1",
          "addressLine2": "Employeertest2",
          "addressLine3": null,
          "addressLine4": null,
          "landMark": null,
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "Work",
          "isVerified": false,
          "isDefault": false
        }
      ],
      "designation": "Manager",
      "lengthOfEmployment": 60,
      "name": "SIS",
      "workEmailId": "TestSigma@gmail.com",
      "EmploymentStatus" : "Salaried",
      "workPhoneNumbers": [
        {
          "phone": "9898989898",
          "countryCode": "91",
          "phoneType": "Work",
          "isDefault": false
        }
      ]
    }
  ]
}

// EmailAddress
{
	"Id":"",
	"Email":"",
	"EmailType":"",
	"IsDefault":
}

// PhoneNumber
{
	"PhoneId":"",
	"Phone":"",
	"CountryCode":"",
	"PhoneType":"",
	"IsDefault":
}

// Address
{
	"AddressId":"",
	"AddressLine1":"",
	"AddressLine2":"",
	"AddressLine3":"",
	"AddressLine4":"",
	"City":"",
	"State":"",
	"PinCode":"",
	"Country":"",
	"AddressType":"",
	"IsDefault":"",
	"LandMark":"",
	"Location":""
}

// BankInformation
{
	"BankId":"",
	"BankName":"",
	"AccountNumber":"",
	"AccountHolderName":"",
	"IfscCode":"",
	"AccountType":"Bank",
	"BankAddresses":{
	"AddressId":"",
	"AddressLine1":"",
	"AddressLine2":"",
	"AddressLine3":"",
	"AddressLine4":"",
	"City":"",
	"State":"",
	"PinCode":"",
	"Country":"",
	"AddressType":"",
	"IsDefault":"",
	"LandMark":"",
	"Location":""
	}
}

// EmploymentDetail
{
	"EmploymentId":"",
	"Name":"",
	"Addresses":[{}],
	"Designation":"",
	"WorkPhoneNumbers":[{}],
	"WorkEmailId":"",
	"LengthOfEmployment":"",
	"IncomeInformation":{},
	"EmploymentStatus":"",
	"AsOfDate":"",
	"CinNumber":""
}

// IncomeInformation
{
	"Income":"",
	"PaymentFrequency":""
}

//HighestEducationInformation
{
	"LevelOfEducation":"",
	"EducationalInstitution":""
}
```

#**Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/security-identity
```
{
  "roles": {
    "VP Of Sales": {
      "permissions": [
        "{*any}",
        "back-office"
      ],
      "roles": {
        "Loan Coordinator Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Loan Coordinator": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "Sr. Relationship Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Relationship Manager": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        }
      }
    },
    "VP Of Operations": {
      "permissions": [
        "{*any}",
        "back-office"
      ],
      "roles": {
        "Sr. Loan Processor": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Loan Processor": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "Credit Specialist Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Credit Specialist": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "Merchant Underwriter Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Merchant Underwriter": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "Customer Service Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "Customer Service Representative": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        },
        "QA Manager": {
          "permissions": [
            "{*any}",
            "back-office"
          ],
          "roles": {
            "QA": {
              "permissions": [
                "{*any}",
                "back-office"
              ]
            }
          }
        }
      }
    },
    "Director Of Merchant Support": {
      "permissions": [
        "{*any}",
        "back-office"
      ],
      "roles": {
        "Merchant Support Specialist": {
          "permissions": [
            "{*any}",
            "back-office"
          ]
        }
      }
    },
    "Marketing": {
      "permissions": [
        "{*any}",
        "back-office"
      ],
      "roles": {
        "Social Media Coordinator": {
          "permissions": [
            "{*any}",
            "back-office"
          ]
        }
      }
    },
    "Borrower": {
      "permissions": [
        "{*any}",
        "borrower"
      ]
    },
    "Merchant": {
      "permissions": [
        "{*any}",
        "merchant"
      ]
    }
  },
  "sessionTimeout": "10",
  "tokenTimeout": "60"
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
  "isActive": "True",
  "email": "cetenantX@creditexchange.com",
  "name": "ce",
  "timezone": "Asia/Calcutta",
  "phone": "anything",
  "paymentTimeLimit": "19",
  "website": "www.creditexchange.com"
}
```

3. Configurations for {{default_host}}:{{configuration_port}}/applicant
```
{
  "defaultRoles": [
    "borrower"
  ]
}
```



   