##### Availalbe End Points:

1. **Get Cibil Report**  
   **Description:** Get Credit Report and Verification report by providing entitytype, entityid and Request Object.
   **HTTP Verb:** POST  
   **Route:**  "{entitytype}/{entityid}"  
   ```
   - Request: POST("{entitytype}/{entityid}")    
    {
      "applicantFirstName" :"Mabel",
      "applicantMiddleName":"",
      "applicantLastName":"Rajagopalan",
      "gender":"Female",
      "dateOfBirth":"02/06/1981",
      "emailId":"",
      "panNo":"ADZPR4147H",
      "drivingLicenseNumber":null,
      "voterId":"UBV2099604",
      "adharNumber":"898230945645",
      "passportNumber":null,
      "rationCardNo":null,
      "address1Line1":"501 Fifth Floor",
      "address1Line2":"Pam Ville DMonte Park",
      "address1Line3":null,
      "address1City":"Mumbai",
      "address1State":"27",
      "address1Pincode":"400050",
      "address1AddressCategory":"01",
      "address1ResidentOccupation":"01",
      "address2Line1":"HSBC Bank India",
      "address2Line2":"Business Park",
      "address2Line3":null,
      "address2City":"Mumbai",
      "address2State":"27",
      "address2Pincode":"400013",
      "address2AddressCategory":null,
      "address2ResidentOccupation":null,
      "MobileNo":"9012783067",
      "LandLineNo":"9345678109",
      "OfficeLandLineNo":null,
      "LoanAmount":"12343"
   }
   - Response:-//  Get Credit Report 
    {
       "creditInfomationReport": {
    "dateProcessed": "07102016",
    "consumerName1": "MABEL",
    "consumerName2": "R",
    "consumerName3": "RAJAGOPALAN",
    "consumerName4": null,
    "consumerName5": null,
    "dateOfBirth": null,
    "gender": "1",
    "idSegment": [
      {
        "idType": "01",
        "idNumber": "ADZPR4147H"
      },
      {
        "idType": "03",
        "idNumber": "URH0817686"
      }
    ],
    "telephoneSegment": [
      {
        "telephoneNumber": "9874561230",
        "telephoneType": "01"
      },
      {
        "telephoneNumber": "66231356",
        "telephoneType": "03"
      },
      {
        "telephoneNumber": "9845614731",
        "telephoneType": "01"
      },
      {
        "telephoneNumber": "45614731",
        "telephoneType": "02"
      }
    ],
    "emails": [
      "vibhavari.sathe@webaccessglobal.com",
      "vibhavari.sathe@webaccessglobal.com"
    ],
    "employmentSegment": {
      "accountType": null,
      "dateReportedCertified": null,
      "occupationCode": null,
      "income": null,
      "netGrossIndicator": null,
      "monthlyAnnualIndicator": null
    },
    "scoreSegment": {
      "scoreName": "CIBILTUSC2",
      "scoreCardName": "04",
      "scoreCardVersion": "10",
      "scoreDate": "07102016",
      "score": "00728",
      "reasonCode1": "17",
      "reasonCode2": null,
      "reasonCode3": null,
      "reasonCode4": null,
      "reasonCode5": null
    },
    "address": [
      {
        "addressLine1": "HSBC BANK INDIA",
        "addressLine2": "BUSINESS PARK",
        "addressLine3": "OPPHANGLA LOWER",
        "addressLine4": "MUMBAI",
        "addressLine5": null,
        "stateCode": "27",
        "pinCode": "400050",
        "addressCategory": "03",
        "residenceCode": null,
        "dateReported": "05082016"
      },
      {
        "addressLine1": "501 FIFTH FLOOR PAM VILLE DMONTE PARK",
        "addressLine2": null,
        "addressLine3": null,
        "addressLine4": null,
        "addressLine5": "MUMBAI",
        "stateCode": "33",
        "pinCode": "600002",
        "addressCategory": "02",
        "residenceCode": null,
        "dateReported": "10012016"
      },
      {
        "addressLine1": "ANNET TECHNOLOGIES",
        "addressLine2": "MAHALAXMI",
        "addressLine3": null,
        "addressLine4": null,
        "addressLine5": "MUMBAI",
        "stateCode": "27",
        "pinCode": "400013",
        "addressCategory": "03",
        "residenceCode": null,
        "dateReported": "10012016"
      },
      {
        "addressLine1": "CHENNAI",
        "addressLine2": "CHENNAI",
        "addressLine3": null,
        "addressLine4": null,
        "addressLine5": "CHENNAI",
        "stateCode": "33",
        "pinCode": "600002",
        "addressCategory": "03",
        "residenceCode": null,
        "dateReported": "18062015"
      }
    ],
    "account": [
      {
        "reportingMemberShortName": "NOT DISCLOSED",
        "accountType": "10",
        "owenershipIndicator": "1",
        "dateOpenedOrDisbursed": "11042014",
        "dateOfLastPayment": null,
        "dateReportedAndCertified": "30062014",
        "highCreditOrSanctionedAmount": null,
        "currentBalance": "0",
        "paymentHistory1": "000000000",
        "paymentHistoryStartDate": "01062014",
        "paymentHistoryEndDate": "01042014",
        "paymentHistory1FieldLength": "09",
        "paymentHistory2FieldLength": null,
        "creditLimit": "25000",
        "cashLimit": null,
        "rateOfInterest": null,
        "paymentFrequency": "03",
        "actualPaymentAmount": null,
        "paymentHistory2": null,
        "dateClosed": null,
        "repaymentTenure": null,
        "emiAmount": null,
        "amountOverdue": null
      },
      {
        "reportingMemberShortName": "NOT DISCLOSED",
        "accountType": "01",
        "owenershipIndicator": "1",
        "dateOpenedOrDisbursed": "30032014",
        "dateOfLastPayment": "10062014",
        "dateReportedAndCertified": "30062014",
        "highCreditOrSanctionedAmount": "500000",
        "currentBalance": "461634",
        "paymentHistory1": "000000000000",
        "paymentHistoryStartDate": "01062014",
        "paymentHistoryEndDate": "01032014",
        "paymentHistory1FieldLength": "12",
        "paymentHistory2FieldLength": null,
        "creditLimit": null,
        "cashLimit": null,
        "rateOfInterest": null,
        "paymentFrequency": null,
        "actualPaymentAmount": null,
        "paymentHistory2": null,
        "dateClosed": null,
        "repaymentTenure": null,
        "emiAmount": null,
        "amountOverdue": null
      },
      {
        "reportingMemberShortName": "NOT DISCLOSED",
        "accountType": "03",
        "owenershipIndicator": "1",
        "dateOpenedOrDisbursed": "27122007",
        "dateOfLastPayment": null,
        "dateReportedAndCertified": "30042015",
        "highCreditOrSanctionedAmount": "85669",
        "currentBalance": "0",
        "paymentHistory1": "000",
        "paymentHistoryStartDate": "01042015",
        "paymentHistoryEndDate": "01042015",
        "paymentHistory1FieldLength": "03",
        "paymentHistory2FieldLength": null,
        "creditLimit": null,
        "cashLimit": null,
        "rateOfInterest": null,
        "paymentFrequency": null,
        "actualPaymentAmount": null,
        "paymentHistory2": null,
        "dateClosed": "20102014",
        "repaymentTenure": null,
        "emiAmount": null,
        "amountOverdue": null
      }
    ],
    "enquiry": [
      {
        "dateOfEnquiryFields": "05102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "29092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "28092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "26092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "05",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "23092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "23092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "22092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25082016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "05082016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "05",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "01082016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27062016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "03022016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "01022016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "28012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "28012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "28012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "27012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "27012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "13",
        "enquiryAmount": "29100"
      },
      {
        "dateOfEnquiryFields": "22012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "22012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "22012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "22012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "21012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "21012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "21012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "14012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "14012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "12012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "12012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "12012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "10012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "10012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "07012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "06012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "13",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "05012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "31122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "24122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "22122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "21122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "19122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "18122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "15122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "11122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "10122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "08122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "24112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "24112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "21112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "20112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "19112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "19112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "19112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "18112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "18112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "16112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "15112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "14112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "13112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "10112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "09112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "08112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "07112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "06112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "05112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "04112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "03112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "01112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "31102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "30102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "30102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "29102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "28102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "27102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "26102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "24102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "23102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "22102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "19102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "16102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "15102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "13102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "11102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "09102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "08102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "08102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "05102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "05102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "01102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "01102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "29092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "29092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "28092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "26092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "24092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "23092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "22092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "21092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "21092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "18092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "16092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "16092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "14092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "13092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "11092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "09092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "04092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "04092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "1050000"
      },
      {
        "dateOfEnquiryFields": "03092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "612333"
      },
      {
        "dateOfEnquiryFields": "03092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "3000000"
      },
      {
        "dateOfEnquiryFields": "03092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "70000"
      },
      {
        "dateOfEnquiryFields": "02092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "31082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "3000000"
      },
      {
        "dateOfEnquiryFields": "31082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "29082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "28082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "25082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "24082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "24082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "19082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "3000000"
      },
      {
        "dateOfEnquiryFields": "18082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "18082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "4444"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "380000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "380000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "380000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "380000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "14082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "14082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "13082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "13082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "11082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "10082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "10082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "10082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "10082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "08082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "07082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "05082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "04082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "04082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "04082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "03082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "03082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "03082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "03082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "28072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "28072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "24072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "22072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "21072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "15072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "01",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "14072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "14072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "14072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "12072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "08072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "08072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "06072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "03072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "02072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "23062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "23062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "22062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "22062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "19062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "19062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "18062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "18062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "12062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07052015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "07052015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "18122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "15122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "12122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "12122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "08122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17112014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "31102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "21102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "20102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "20102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "20102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "06102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "08092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "03092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "02092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "02092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "02092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "31082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "26082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "21082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "5000000"
      },
      {
        "dateOfEnquiryFields": "28072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "5000000"
      },
      {
        "dateOfEnquiryFields": "26072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "5000000"
      },
      {
        "dateOfEnquiryFields": "26072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "5000000"
      },
      {
        "dateOfEnquiryFields": "26072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "1000000"
      },
      {
        "dateOfEnquiryFields": "26072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "1000000"
      },
      {
        "dateOfEnquiryFields": "14062014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "12",
        "enquiryAmount": "40000000"
      },
      {
        "dateOfEnquiryFields": "14062014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "12",
        "enquiryAmount": "40000000"
      },
      {
        "dateOfEnquiryFields": "09042014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09042014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "21032014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "01",
        "enquiryAmount": "100"
      },
      {
        "dateOfEnquiryFields": "21032014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "01",
        "enquiryAmount": "100"
      },
      {
        "dateOfEnquiryFields": "18062013",
        "enquiringMemberShortName": "VIJAYA BANK",
        "enquiryPurpose": "01",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "18062013",
        "enquiringMemberShortName": "VIJAYA BANK",
        "enquiryPurpose": "01",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "31122011",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "5400000"
      },
      {
        "dateOfEnquiryFields": "31122011",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "5400000"
      },
      {
        "dateOfEnquiryFields": "23022007",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "25000"
      },
      {
        "dateOfEnquiryFields": "23022007",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "25000"
      }
    ]
  },
  "cibilReport": {
    "ekyc": null,
    "voterId": {
      "id": "UBV2099604",
      "source": "Input",
      "errorCode": "S",
      "timeTrail": "GetDBRecordsTime:2622.865|OETime:12.1332|NameMatchTime:2.3264"
    },
    "aadhaarIdBA": {
      "id": "898230945645",
      "source": "Input",
      "errorCode": "S",
      "poi": "998-N",
      "poa": "998-N",
      "pfa": "998-N",
      "timeTrail": "POITimeTaken:102390.8845|POATimeTaken:556.5817|PFATimeTaken:563.0015",
      "poiNameMatch": null,
      "poiGenderMatch": null,
      "poidobMatch": null,
      "poaStateMatch": null,
      "poaPincodeMatch": null
    },
    "dataSource": {
      "source": null,
      "firstName": null,
      "middleName": null,
      "lastName": null,
      "nameMatch": null,
      "gender": "Female",
      "genderMatch": "true",
      "dateofBirth": "01051966",
      "dateofBirthMatch": "false",
      "pan": "ADZPR4147H",
      "panMatch": "true",
      "voterId": "URH0817686",
      "voterIdMatch": "false",
      "uid": null,
      "uidMatch": null,
      "drivingLicense": null,
      "drivingLicenseMatch": "N/A",
      "rationCard": null,
      "rationCardMatch": "N/A",
      "passport": null,
      "passportMatch": "N/A",
      "telephone1": null,
      "telephone1Match": null,
      "telephone2": null,
      "telephone2Match": null,
      "telephone3": null,
      "telephone3Match": null,
      "telephone4": null,
      "telephone4Match": null,
      "address": [
        {
          "addressLine1": "HSBC BANK INDIA BUSINESS PARK OPPHANGLA LOWER MUMBAI",
          "pinCode": "400050",
          "stateCode": "MAHARASHTRA",
          "match": "0.0",
          "match1": "0.0",
          "addressCategory": "03",
          "dateReported": "05082016"
        },
        {
          "addressLine1": "501 FIFTH FLOOR PAM VILLE DMONTE PARK    MUMBAI",
          "pinCode": "600002",
          "stateCode": "33",
          "match": "33.0",
          "match1": "33.0",
          "addressCategory": "02",
          "dateReported": "10012016"
        },
        {
          "addressLine1": "ANNET TECHNOLOGIES MAHALAXMI   MUMBAI",
          "pinCode": "400013",
          "stateCode": "27",
          "match": "0.0",
          "match1": "0.0",
          "addressCategory": "03",
          "dateReported": "10012016"
        },
        {
          "addressLine1": "CHENNAI CHENNAI   CHENNAI",
          "pinCode": "600002",
          "stateCode": "33",
          "match": "0.0",
          "match1": "0.0",
          "addressCategory": "03",
          "dateReported": "18062015"
        }
      ]
    },
    "pan": {
      "panName": "MAHANGARE SWAPNIL VITTHAL",
      "panNameMatch": "100",
      "panNo": "BCEPM0179D",
      "panNoMatch": "80"
    },
    "cibildata": {
      "cibilName": "",
      "cibilNameMatch": "",
      "cibilGender": "",
      "cibilGenderMatch": "",
      "cibilPassport": "",
      "cibilPassportMatch": "",
      "cibilDrivingLicense": "",
      "cibilDrivingLicenseMatch": "",
      "cibilAadharNumber": "",
      "cibilAadharNumberMatch": "",
      "cibilRationCard": null,
      "cibilRationCardMatch": null,
      "cibilVoterID": "",
      "cibilVoterIDMatch": "",
      "cibilAddress1": null,
      "cibilAddress1Match": null,
      "cibilAddress2": null,
      "cibilAddress2Match": null,
      "cibilPhone1": "",
      "cibilPhone1Match": "",
      "cibilPhone2": "",
      "cibilPhone2Match": "",
      "cibilPhone3": "",
      "cibilPhone3Match": "",
      "cibilPhone4": "",
      "cibilPhone4Match": "",
      "cibilDOB": "",
      "cibilDOBMatch": "0"
    }
  }
    } 
   ``` 

##### Schema
   // Cibil PAN Response Objec

   ```
    {
       "creditInfomationReport": {
    "dateProcessed": "07102016",
    "consumerName1": "MABEL",
    "consumerName2": "R",
    "consumerName3": "RAJAGOPALAN",
    "consumerName4": null,
    "consumerName5": null,
    "dateOfBirth": null,
    "gender": "1",
    "idSegment": [
        {
         "idType": "01",
         "idNumber": "ADZPR4147H"
        },
        {
         "idType": "03",
         "idNumber": "URH0817686"
        }
       ],
    "telephoneSegment": [
      {
        "telephoneNumber": "9874561230",
        "telephoneType": "01"
      },
      {
        "telephoneNumber": "66231356",
        "telephoneType": "03"
      },
      {
        "telephoneNumber": "9845614731",
        "telephoneType": "01"
      },
      {
        "telephoneNumber": "45614731",
        "telephoneType": "02"
      }
    ],
    "emails": [
      "vibhavari.sathe@webaccessglobal.com",
      "vibhavari.sathe@webaccessglobal.com"
    ],
    "employmentSegment": {
      "accountType": null,
      "dateReportedCertified": null,
      "occupationCode": null,
      "income": null,
      "netGrossIndicator": null,
      "monthlyAnnualIndicator": null
      },
    "scoreSegment": {
      "scoreName": "CIBILTUSC2",
      "scoreCardName": "04",
      "scoreCardVersion": "10",
      "scoreDate": "07102016",
      "score": "00728",
      "reasonCode1": "17",
      "reasonCode2": null,
      "reasonCode3": null,
      "reasonCode4": null,
      "reasonCode5": null
      },
    "address": [
        {
        "addressLine1": "HSBC BANK INDIA",
        "addressLine2": "BUSINESS PARK",
        "addressLine3": "OPPHANGLA LOWER",
        "addressLine4": "MUMBAI",
        "addressLine5": null,
        "stateCode": "27",
        "pinCode": "400050",
        "addressCategory": "03",
        "residenceCode": null,
        "dateReported": "05082016"
        },
        {
        "addressLine1": "501 FIFTH FLOOR PAM VILLE DMONTE PARK",
        "addressLine2": null,
        "addressLine3": null,
        "addressLine4": null,
        "addressLine5": "MUMBAI",
        "stateCode": "33",
        "pinCode": "600002",
        "addressCategory": "02",
        "residenceCode": null,
        "dateReported": "10012016"
        },
        {
        "addressLine1": "ANNET TECHNOLOGIES",
        "addressLine2": "MAHALAXMI",
        "addressLine3": null,
        "addressLine4": null,
        "addressLine5": "MUMBAI",
        "stateCode": "27",
        "pinCode": "400013",
        "addressCategory": "03",
        "residenceCode": null,
        "dateReported": "10012016"
        },
        {
        "addressLine1": "CHENNAI",
        "addressLine2": "CHENNAI",
        "addressLine3": null,
        "addressLine4": null,
        "addressLine5": "CHENNAI",
        "stateCode": "33",
        "pinCode": "600002",
        "addressCategory": "03",
        "residenceCode": null,
        "dateReported": "18062015"
        }
      ],
    "account": [
        {
        "reportingMemberShortName": "NOT DISCLOSED",
        "accountType": "10",
        "owenershipIndicator": "1",
        "dateOpenedOrDisbursed": "11042014",
        "dateOfLastPayment": null,
        "dateReportedAndCertified": "30062014",
        "highCreditOrSanctionedAmount": null,
        "currentBalance": "0",
        "paymentHistory1": "000000000",
        "paymentHistoryStartDate": "01062014",
        "paymentHistoryEndDate": "01042014",
        "paymentHistory1FieldLength": "09",
        "paymentHistory2FieldLength": null,
        "creditLimit": "25000",
        "cashLimit": null,
        "rateOfInterest": null,
        "paymentFrequency": "03",
        "actualPaymentAmount": null,
        "paymentHistory2": null,
        "dateClosed": null,
        "repaymentTenure": null,
        "emiAmount": null,
        "amountOverdue": null
        },
        {
        "reportingMemberShortName": "NOT DISCLOSED",
        "accountType": "01",
        "owenershipIndicator": "1",
        "dateOpenedOrDisbursed": "30032014",
        "dateOfLastPayment": "10062014",
        "dateReportedAndCertified": "30062014",
        "highCreditOrSanctionedAmount": "500000",
        "currentBalance": "461634",
        "paymentHistory1": "000000000000",
        "paymentHistoryStartDate": "01062014",
        "paymentHistoryEndDate": "01032014",
        "paymentHistory1FieldLength": "12",
        "paymentHistory2FieldLength": null,
        "creditLimit": null,
        "cashLimit": null,
        "rateOfInterest": null,
        "paymentFrequency": null,
        "actualPaymentAmount": null,
        "paymentHistory2": null,
        "dateClosed": null,
        "repaymentTenure": null,
        "emiAmount": null,
        "amountOverdue": null
        },
        {
        "reportingMemberShortName": "NOT DISCLOSED",
        "accountType": "03",
        "owenershipIndicator": "1",
        "dateOpenedOrDisbursed": "27122007",
        "dateOfLastPayment": null,
        "dateReportedAndCertified": "30042015",
        "highCreditOrSanctionedAmount": "85669",
        "currentBalance": "0",
        "paymentHistory1": "000",
        "paymentHistoryStartDate": "01042015",
        "paymentHistoryEndDate": "01042015",
        "paymentHistory1FieldLength": "03",
        "paymentHistory2FieldLength": null,
        "creditLimit": null,
        "cashLimit": null,
        "rateOfInterest": null,
        "paymentFrequency": null,
        "actualPaymentAmount": null,
        "paymentHistory2": null,
        "dateClosed": "20102014",
        "repaymentTenure": null,
        "emiAmount": null,
        "amountOverdue": null
        }
      ],
    "enquiry": [
       {
        "dateOfEnquiryFields": "05102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
       },
       {
        "dateOfEnquiryFields": "05102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
       },
       {
        "dateOfEnquiryFields": "05102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
       },
       {
        "dateOfEnquiryFields": "04102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
       },
      {
        "dateOfEnquiryFields": "04102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04102016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "29092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "28092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "26092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "05",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "23092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "23092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "22092016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25082016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "05082016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "05",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "01082016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27062016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "03022016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "01022016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "29012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "28012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "28012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "28012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "27012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "27012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "13",
        "enquiryAmount": "29100"
      },
      {
        "dateOfEnquiryFields": "22012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "22012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "22012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "22012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "21012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "21012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "21012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "14012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "14012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "12012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "12012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "12012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "11012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "10012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "10012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "07012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "06012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "13",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "05012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04012016",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "31122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "24122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "22122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "21122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "19122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "18122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "15122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "11122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "10122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "08122015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "24112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "24112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "21112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "20112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "19112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "19112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "19112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "18112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "18112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "16112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "15112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "14112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "13112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "10112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "09112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "08112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "07112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "06112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "05112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "04112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "03112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "01112015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "31102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "30102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "30102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "29102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "28102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "27102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "26102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "24102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "23102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "22102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "19102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "16102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "15102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "13102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "11102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "09102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "08102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "08102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "07102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "05102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "05102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "01102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "01102015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "29092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "29092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "28092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "26092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "24092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "23092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "22092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "21092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "21092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "18092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "16092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "16092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "14092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "13092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "11092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "09092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "04092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "04092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "1050000"
      },
      {
        "dateOfEnquiryFields": "03092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "612333"
      },
      {
        "dateOfEnquiryFields": "03092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "3000000"
      },
      {
        "dateOfEnquiryFields": "03092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "70000"
      },
      {
        "dateOfEnquiryFields": "02092015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "31082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "3000000"
      },
      {
        "dateOfEnquiryFields": "31082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "29082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "28082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "25082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "25082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "24082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "24082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "21082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "19082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "3000000"
      },
      {
        "dateOfEnquiryFields": "18082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "18082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "4444"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "380000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "380000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "380000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "380000"
      },
      {
        "dateOfEnquiryFields": "17082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "14082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "14082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "13082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "600000"
      },
      {
        "dateOfEnquiryFields": "13082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "12082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "11082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "10082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "10082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "10082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "10082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "08082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "07082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "06082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "05082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "04082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "04082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "04082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "03082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "03082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "03082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "03082015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "30072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "28072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "28072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "27072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "24072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "22072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "21072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "20072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "17072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "15072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "01",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "14072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "14072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "14072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "12072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "08072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "08072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "06072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "03072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "02072015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "80000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "23062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "23062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "23062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "22062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "22062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "19062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "19062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "18062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "18062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "15062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "12062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "11062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10062015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07052015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "07052015",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "100000"
      },
      {
        "dateOfEnquiryFields": "18122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "15122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "12122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "12122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "08122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05122014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17112014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "31102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "29102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "21102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "20102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "20102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "20102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "13102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "06102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04102014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "17092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "16092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "10092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "09092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "08092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "07092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "05092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "04092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "03092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "02092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "02092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "02092014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "31082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "30082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "27082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "26082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "25082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "21082014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "00",
        "enquiryAmount": "1000"
      },
      {
        "dateOfEnquiryFields": "28072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "5000000"
      },
      {
        "dateOfEnquiryFields": "28072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "5000000"
      },
      {
        "dateOfEnquiryFields": "26072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "5000000"
      },
      {
        "dateOfEnquiryFields": "26072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "5000000"
      },
      {
        "dateOfEnquiryFields": "26072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "1000000"
      },
      {
        "dateOfEnquiryFields": "26072014",
        "enquiringMemberShortName": "ICICI BANK",
        "enquiryPurpose": "02",
        "enquiryAmount": "1000000"
      },
      {
        "dateOfEnquiryFields": "14062014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "12",
        "enquiryAmount": "40000000"
      },
      {
        "dateOfEnquiryFields": "14062014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "12",
        "enquiryAmount": "40000000"
      },
      {
        "dateOfEnquiryFields": "09042014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "09042014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "50000"
      },
      {
        "dateOfEnquiryFields": "21032014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "01",
        "enquiryAmount": "100"
      },
      {
        "dateOfEnquiryFields": "21032014",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "01",
        "enquiryAmount": "100"
      },
      {
        "dateOfEnquiryFields": "18062013",
        "enquiringMemberShortName": "VIJAYA BANK",
        "enquiryPurpose": "01",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "18062013",
        "enquiringMemberShortName": "VIJAYA BANK",
        "enquiryPurpose": "01",
        "enquiryAmount": "10000"
      },
      {
        "dateOfEnquiryFields": "31122011",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "5400000"
      },
      {
        "dateOfEnquiryFields": "31122011",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "02",
        "enquiryAmount": "5400000"
      },
      {
        "dateOfEnquiryFields": "23022007",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "25000"
      },
      {
        "dateOfEnquiryFields": "23022007",
        "enquiringMemberShortName": "NOT DISCLOSED",
        "enquiryPurpose": "10",
        "enquiryAmount": "25000"
       }
      ]
    },
  "cibilReport": {
    "ekyc": null,
    "voterId": {
      "id": "UBV2099604",
      "source": "Input",
      "errorCode": "S",
      "timeTrail": "GetDBRecordsTime:2622.865|OETime:12.1332|NameMatchTime:2.3264"
        },
          "aadhaarIdBA": 
          {
            "id": "898230945645",
            "source": "Input",
            "errorCode": "S",
            "poi": "998-N",
            "poa": "998-N",
            "pfa": "998-N",
            "timeTrail": "POITimeTaken:102390.8845|POATimeTaken:556.5817|PFATimeTaken:563.0015",
            "poiNameMatch": null,
            "poiGenderMatch": null,
            "poidobMatch": null,
            "poaStateMatch": null,
            "poaPincodeMatch": null
          },
          "dataSource": 
        {
         "source": null,
      "firstName": null,
      "middleName": null,
      "lastName": null,
      "nameMatch": null,
      "gender": "Female",
      "genderMatch": "true",
      "dateofBirth": "01051966",
      "dateofBirthMatch": "false",
      "pan": "ADZPR4147H",
      "panMatch": "true",
      "voterId": "URH0817686",
      "voterIdMatch": "false",
      "uid": null,
      "uidMatch": null,
      "drivingLicense": null,
      "drivingLicenseMatch": "N/A",
      "rationCard": null,
      "rationCardMatch": "N/A",
      "passport": null,
      "passportMatch": "N/A",
      "telephone1": null,
      "telephone1Match": null,
      "telephone2": null,
      "telephone2Match": null,
      "telephone3": null,
      "telephone3Match": null,
      "telephone4": null,
      "telephone4Match": null,
      "address": [
         {
          "addressLine1": "HSBC BANK INDIA BUSINESS PARK OPPHANGLA LOWER MUMBAI",
          "pinCode": "400050",
          "stateCode": "MAHARASHTRA",
          "match": "0.0",
          "match1": "0.0",
          "addressCategory": "03",
          "dateReported": "05082016"
         },
        {
          "addressLine1": "501 FIFTH FLOOR PAM VILLE DMONTE PARK    MUMBAI",
          "pinCode": "600002",
          "stateCode": "33",
          "match": "33.0",
          "match1": "33.0",
          "addressCategory": "02",
          "dateReported": "10012016"
        },
        {
          "addressLine1": "ANNET TECHNOLOGIES MAHALAXMI   MUMBAI",
          "pinCode": "400013",
          "stateCode": "27",
          "match": "0.0",
          "match1": "0.0",
          "addressCategory": "03",
          "dateReported": "10012016"
        },
        {
          "addressLine1": "CHENNAI CHENNAI   CHENNAI",
          "pinCode": "600002",
          "stateCode": "33",
          "match": "0.0",
          "match1": "0.0",
          "addressCategory": "03",
          "dateReported": "18062015"
        }
      ]
    },
    "pan": {
      "panName": "MAHANGARE SWAPNIL VITTHAL",
      "panNameMatch": "100",
      "panNo": "BCEPM0179D",
      "panNoMatch": "80"
    },
    "cibildata": {
      "cibilName": "",
      "cibilNameMatch": "",
      "cibilGender": "",
      "cibilGenderMatch": "",
      "cibilPassport": "",
      "cibilPassportMatch": "",
      "cibilDrivingLicense": "",
      "cibilDrivingLicenseMatch": "",
      "cibilAadharNumber": "",
      "cibilAadharNumberMatch": "",
      "cibilRationCard": null,
      "cibilRationCardMatch": null,
      "cibilVoterID": "",
      "cibilVoterIDMatch": "",
      "cibilAddress1": null,
      "cibilAddress1Match": null,
      "cibilAddress2": null,
      "cibilAddress2Match": null,
      "cibilPhone1": "",
      "cibilPhone1Match": "",
      "cibilPhone2": "",
      "cibilPhone2Match": "",
      "cibilPhone3": "",
      "cibilPhone3Match": "",
      "cibilPhone4": "",
      "cibilPhone4Match": "",
      "cibilDOB": "",
      "cibilDOBMatch": "0"
    }
  }
     }

   ```  

##### Configuration  
1. **Endpoint :**  
**Http GET/POST :** {{default_host}}:{{configuration_port}}/cibil  

```
{
    "isCirReq": "Y",
  "userId": "CreditExchange_Admin",
  "password": "Password@123",
  "environmentType": "U",
  "solutionSetId": "1135",
  "executionMode": "NewWithContext",
  "url": "http://192.168.1.59:5199/cibil",
  "isEverifyReq": "Y",
  "solutionSetVersion": "26",
  "inpEnquiryPurpose": "01",
  "inpScoreType": "04",
  "authentication": "OnDemand"
}
```