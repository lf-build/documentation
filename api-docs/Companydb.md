##### Availalbe End Points:
	
1. **Search Company information**
   **Description** : Get company information by providing Name or Corporate Identity Number(CIN)
   **HTTP Verb**: GET
   **Route** : "/company/search/{s}"
   ```
   - Request: GET("/company/search/{Name}")    
   - Request Schema:  {
						"s":"U67120MH1991PTC063321"
					  }    
   - Response:-// CompanySearchResponseObject
				  {
						"Cin":"U18109DL2008PTC177167",
						"CompanyCategory":"Non-govt company",
						"NavratnaCategory":"NA",
						"Name":"B & A CREATIONS",
						"BankCategory":""
				   }
   ``` 
   
2. **Company Category**
   **Description** : Get company category by providing Corporate Identification Number(CIN), EntityType and EntityId
   **HTTP Verb**: GET
   **Route** : "/company/category/{cin}/{entityType}/{entityId}"
   ```
   - Request: GET("{/company/category/{cin}/{entityType}/{entityId}")
   - Request Schema: {
							"cin":"U67120MH1991PTC063321",
							"entityType":"",
							"entityId":""
					 }

   - Response :- // RuleResultObject  			
					{
						"RuleName":"LLP or Partnership",
						"Result":"D"
					}
   ```

3. **Directors List**: 
   **Description** : Get Directors and company data by providing Corporate Identification Number(CIN)
   **HTTP Verb**: GET
   **Route** : "/company/directors/{cin}"
```
     - Request: GET("{/company/directors/{cin}")
     - Request Schema: {
	   					"cin":"U67120MH1991PTC063321"
   			 		 }		
     - Response:  //  CompanyWithDirectorsObject
				    {
					    "Directors":{
									"Cin" : "U18109DL2008PTC177167",
									"Din" : "'01551588",
									"Name" : "SHEGGY",
									"AgeOfCompany" : 8,
									"PaidUpCapital" : "100000"
								},
					    "Company":{
									"Id":"123",
									"Cin":"U67120MH1991PTC063321",
									"Name":"B & A CREATIONS",
									"Type":"PRIVATE",
									"StatusOfListing":"Unlisted",
									"Category":"Non-govt company",
									"SubCategory":"Others",
									"AuthorisedCapital":"1000",
									"PaidUpCapital":"",
									"AgeOfCompany":"8",
									"LastYearRevenue":"0"
									"LastYearProfit":"0",
									"MoneyControlCompanyName":"",
									"MoneyControlRevenue":"0",
									"MoneyControlProfit":"0",
									"MoneyControlUrl":"",
									"ZaubaCorpUrl":"https://www.zaubacorp.com/company/B-A-CREATIONS/U18109DL2008PTC177160",
									"NavratnaCategory:"NA",
									"BankCategory":"",
									"Domain":"www.infkym.com",
									"ExperianCategory":"Cat A",
									"RblDefaulters":"TRUE"
							}
				    }
```
   
### Schema

```
// Company
{
	"Id":"123",
	"Cin":"U67120MH1991PTC063321",
	"Name":"B & A CREATIONS",
	"Type":"PRIVATE",
	"StatusOfListing":"Unlisted",
	"Category":"Non-govt company",
	"SubCategory":"Others",
	"AuthorisedCapital":"1000",
	"PaidUpCapital":"",
	"AgeOfCompany":"8",
	"LastYearRevenue":"0"
	"LastYearProfit":"0",
	"MoneyControlCompanyName":"",
	"MoneyControlRevenue":"0",
	"MoneyControlProfit":"0",
	"MoneyControlUrl":"",
	"ZaubaCorpUrl":"https://www.zaubacorp.com/company/B-A-CREATIONS/U18109DL2008PTC177160",
	"NavratnaCategory:"NA",
	"BankCategory":"",
	"Domain":"www.infkym.com",
	"ExperianCategory":"Cat A",
	"RblDefaulters":"TRUE"
}

// CompanyWithDirectors
{
"Directors":{
				"Cin" : "U18109DL2008PTC177167",
				"Din" : "'01551588",
				"Name" : "SHEGGY",
				"AgeOfCompany" : 8,
				"PaidUpCapital" : "100000"
			},
"Company":{
				"Id":"123",
				"Cin":"U67120MH1991PTC063321",
				"Name":"B & A CREATIONS",
				"Type":"PRIVATE",
				"StatusOfListing":"Unlisted",
				"Category":"Non-govt company",
				"SubCategory":"Others",
				"AuthorisedCapital":"1000",
				"PaidUpCapital":"",
				"AgeOfCompany":"8",
				"LastYearRevenue":"0"
				"LastYearProfit":"0",
				"MoneyControlCompanyName":"",
				"MoneyControlRevenue":"0",
				"MoneyControlProfit":"0",
				"MoneyControlUrl":"",
				"ZaubaCorpUrl":"https://www.zaubacorp.com/company/B-A-CREATIONS/U18109DL2008PTC177160",
				"NavratnaCategory:"NA",
				"BankCategory":"",
				"Domain":"www.infkym.com",
				"ExperianCategory":"Cat A",
				"RblDefaulters":"TRUE"
		  }
}

// CompanyDirector
{   
    "Cin" : "U18109DL2008PTC177167",
    "Din" : "'01551588",
    "Name" : "SHEGGY",
    "AgeOfCompany" : 8,
    "PaidUpCapital" : "100000"
}


// RuleResult
{
	"RuleName":"LLP or Partnership",
	"Result":"D"
}

// CompanySearchResponse
{
	"Cin":"U18109DL2008PTC177167",
	"CompanyCategory":"Non-govt company",
	"NavratnaCategory":"NA",
	"Name":"B & A CREATIONS",
	"BankCategory":""
}
```