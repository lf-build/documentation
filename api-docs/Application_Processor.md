##### Available End Points:

Route("/application")	
1. Add new borrower application

   ```
   - POST("submit") 
   - Body (SubmitApplicationRequest)
   - Response: (IApplicationResponse)
   ``` 
2. Add new lead

   ```
   - POST("lead") 
   - Body (SubmitApplicationRequest)
   - Response: (IApplicationResponse)
   ``` 
   
3. Add/Update the given bank information in given application number

   ```
   - Put [("{applicationNumber}/bankInformation")]
   - Body (BankInformation)
   - Response: (No content)			
   ```
4. Add/Update the given email information in given application number

   ```
   - Put [("{applicationNumber}/emailAddress")]
   - Body (EmailAddress)
   - Response: (No content)
   ```
   
5. Add/Update the given self declared expense in given application number

   ```
   - Put [("{applicationNumber}/selfDeclaredExpense")]
   - Body (SelfDeclareExpense)
   - Response: (No content)
   ```
6. Add/Update the given employment detail in given application number

   ```
   - Put [("{applicationNumber}/employmentDetail")]
   - Body (EmploymentDetail)
   - Response: (No content)			
   ```
7. Add/Update the given education information in given application number

   ```
   - Put [("{applicationNumber}/educationInformation")]
   - Body (EducationInformation)
   - Response: (No content)			
   ```
8. Add/Update ancillary data in data attribute for given application number

   ```
   - Post [("{applicationNumber}/ancillarydata")]
   - Body (AncillaryData)
   - Response: (IAncillaryData)			
   ```  
 
9. Add/Update loan funded data in data attribute for given application number

   ```
   - Post [("{applicationNumber}/loan-fundeddata")]
   - Body (LoanFundedData)
   - Response: (ILoanFundedData)			
   ```  
10. Add/Update perfios report data in data attribute for given application number

   ```
   - Post [("{applicationNumber}/perfios-reportData")]
   - Body (object)
   - Response: (object)			
   ```  
11. Add/Update perfios report data in data attribute for given application number

   ```
   - Post [("{applicationNumber}/perfios-reportData")]
   - Body (object)
   - Response: (object)			
   ```  
12. Add/Update perfios report data in data attribute for given application number

   ```
   - Post [("{applicationNumber}/perfios-upload-statement")]
   - Body (object)
   - Response: (object)			
   ```
   
13. Update application detail for given application number

   ```
   - Put [("{applicationNumber}/update-application")]
   - Body (SubmitApplicationRequest)
   - Response: (IApplication)			
   ```
   
14. Update applicant personal detail for given application number

   ```
   - Put [("{applicationNumber}/applicantDetails")]
   - Body (UpdateApplicantRequest)
   - Response: (No content)				
   ```
15. Update application status from "Lead" to "Submitted" if all validations are succeded

   ```
   - Put [("{applicationNumber}/changeStatus-submit")]
   - Body (UpdateApplicantRequest)
   - Response: (string)				
   ```
Route("/offer")

1. To initiate initial offer generation for given application number 

   ```
   - Post [("{applicationNumber}/initialoffer")]
   - Body 
   - Response: (IInitialOfferDefination)				
   ```
2. To get available initial offer for given application number 

   ```
   - Get [("{applicationNumber}/initialoffer")]
   - Body 
   - Response: (IInitialOfferDefination)				
   ```
3. To re compute initial offer for given application number

   ```
   - Post [("{applicationNumber}/reGenerateInitialoffer/{reCallSyndication}")]
   - Body - reCallSyndication - Set true if syndication needs to be called again else false
   - Response: (IInitialOfferDefination)				
   ```
4. To initiate final offer generation for given application number 

   ```
   - Post [("{applicationNumber}/finaloffer")]
   - Body 
   - Response: (IFinalOfferDefination)				
   ```
5. To get available final offer for given application number 

   ```
   - Get [("{applicationNumber}/finaloffer")]
   - Body 
   - Response: (IFinalOfferDefination)				
   ```
6. To re compute final offer if offer generation failed previously or offer is in manual review state for given application number

   ```
   - Post [("{applicationNumber}/reGenerateFinaloffer/{reCallSyndication}")]
   - Body - reCallSyndication - Set true if syndication needs to be called again else false
   - Response: (IFinalOfferDefination)				
   ``` 
7. To make single offer from list of given initial offers as selected offer for given application number
   ```
   - Put [("{applicationNumber}/acceptinitialoffer/{offerId}")]
   - Body 
   - Response: (NoContent)
   ``` 
8. To reject initial offer for given application number with or without reasons
   ```
   - Put [("{applicationNumber}/rejectinitialoffer/{*reasons}")]
   - Body 
   - Response: (NoContent)
   ```     
9. To make single offer from list of given final offers as selected offer for given application number.
   It also stipulates required documents and generate loan agreement and application form. 
   ```
   - Put [("{applicationNumber}/acceptfinaloffer/{offerId}")]
   - Body 
   - Response: (NoContent)
   ``` 
10. To reject final offer for given application number with or without reasons
   ```
   - Put [("{applicationNumber}/rejectfinaloffer/{*reasons}")]
   - Body 
   - Response: (NoContent)
   ```  
11. To add offer manually in list of generated offers for given application number 
   ```
   - Put [("{applicationNumber}/addmanualfinaloffer")]
   - Body 
   - Response: (NoContent)
   ```  
12. To update perfios data attribute for given application number
   ```
   - HttpPost [("{applicationNumber}/cashflow")]
   - Body : Json object to update perfios data
   - Response: (NoContent)
   ```  

Route("/status-management")	

1. To change the status of given application in status management service
   ```
   - HttpPost [("{applicationNumber}/{newStatus}/{*reasons}")]
   - Body : 
   - Response: (HttpOkResult)
   ```  
   
2. To reject the given application
   ```
   - HttpPost [("{applicationNumber}/rejectApplication/{*reasons}")]
   - Body : 
   - Response: (HttpOkResult)
   ```  
3. To transist given application from final offer acceptance to ce approved stage
   ```
   - HttpPost [("{applicationNumber}/finalofferaccept-ceapproved")]
   - Body : 
   - Response: (HttpOkResult)
   ```  
4. To transist given application from bank credi review to approved stage
   ```
   - HttpPost [("{applicationNumber}/bankcreditreview-approved")]
   - Body : 
   - Response: (HttpOkResult)
   ```  
5. To transist given application from manual review to final offer given stage
   ```
   - HttpPost [("{applicationNumber}/manualreview-finaloffermade")]
   - Body : 
   - Response: (HttpOkResult)
   ```  
6. To get all activities froms status management service for given application number
   ```
   - HttpGet [("{applicationNumber}/activities")]
   - Body : 
   - Response: (List<IActivities>)
   ```     
### Schema

```
// SubmitApplicationRequest
{
	{
  "purposeOfLoan": "other",
 "requestedAmount": 100000,
  "requestedTermType": "Monthly",
  "requestedTermValue": 20,
  "aadhaarNumber": "12345",
  "dateOfBirth": "1971-11-05",
  "salutation": "Miss",
  "firstName": "Geeta",
  "lastName": "NSharma",
  "middleName": "Rahul",
  "permanentAccountNumber": "APPPN695999",
  "gender": "Female",
  "maritalStatus": "Single",
  "userName": "",
  "password": "test123",
  "userId": "test",
  "personalMobile": "9205674575",
  "isMobileVerified": "true",
  "mobileVerificationTime":"1981-11-05",
  "mobileVerificationNotes":"Testing",
  "personalEmail" :"nayan.gp@sigmainfo.net",
  "residenceType": "OwnedSelf",
  "educationalInstitution": "L.D.Engineering",
  "levelOfEducation": "B.E.",
  "currentAddressLine1":"test1",
  "currentAddressLine2":"test2",
  "currentAddressLine3":"test3",
  "currentAddressLine4":"test4",
  "currentCity": "bangalore",
  "currentCountry": "India",
  "currentLandMark": "Sunder Nagar",
  "currentLocation": "Naranpura",
  "currentPinCode": "560033",
  "currentState": "Karnataka",
  "permanentAddressLine1": "test1",
  "permanentAddressLine2": "test2",
  "permanentAddressLine3": "test3",
  "permanentAddressLine4": "test4",
  "permanentCity": "bangalore",
  "permanentCountry": "India",
  "permanenttLandMark": "Sunder Nagar",
  "permanentLocation":  "Naranpura",
  "permanentPinCode": "560033",
  "permanentState": "Karnataka",
  "employmentAsOfDate": "",
  "cinNumber": "U18109DL2008PTC177167",
  "designation": "Manager",
  "employmentStatus": "Salaried",
  "lengthOfEmploymentInMonths": 60,
  "employerName": "Sigma Infosolutions",
  "workEmail": "sanjay.a@sigmainfo.net",
  "income": 50000,
  "paymentFrequency": "Monthly",
  "workAddressLine1" :"B-129 PARADISE APARTMENTS",
  "workAddressLine2" :"NEXT TO HOLY HOSPITAL BALANAGAR RANGA REDDY",
  "workAddressLine3" :"DISTRICT",
  "workAddressLine4": null,
  "workCity": "Bangalore",
  "workCountry": "India",
  "workLandMark": "Naranpura",
  "workLocation": "Ratna High Street",
  "workPinCode": "560033",
  "workState": "KA",
  "creditCardBalances":5000,
  "debtPayments": 5500,
  "monthlyExpenses": 0,
  "monthlyRent": 0,
  "sourceReferenceId":"RMMC",
  "sourceType": "Merchant",
  "systemChannel": "MerchantPortal",
  "trackingCode":"TMMC",
  "OtherPurposeDescription": "otherpurposeofloan"
	}
	
// ApplicationResponse
{
	"RequestedAmount":"",
	"RequestedTermType":"",
	"RequestedTermValue":"",
	"PurposeOfLoan":"",
	"Source":{
				"trackingCode": "",
				"systemChannel": "",
				"sourceType": "",
				"sourceReferenceId": ""
			 },
	"ApplicantId":"",
	"ApplicationNumber":"",
	"EmploymentDetail":
    {
      "addresses": [
        {
          "addressLine1": "Employeertest1",
          "addressLine2": "Employeertest2",
          "addressLine3": "Employeertest3",
          "addressLine4": "Employeertest4",
          "landMark": "Ratna High Street",
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "work",
          "isDefault": true
        }
      ],
      "designation": "Manager",
	  "CinNumber" : "U40101TN2004PTC054931",
      "LengthOfEmploymentInMonths": 60,
      "name": "SIS",
      "workEmail": "TestSigma@gmail.com",
      "EmploymentStatus" : "Salaried",
      "workPhoneNumbers": [
      {
        "PhoneId":"",
		"phone": "9898989898",
        "countryCode": "91",
        "phoneType": "work",
        "isDefault": false
        }
      ],
	"IncomeInformation":
	[{
		"VerifiedIncome" : "1000000",
		"PaymentFrequency" : "8",
		"IncomeVerifiedDate" : "2016-10-10T15:24:28.2480918+05:30"
	}]
    },
	"ResidenceType":"",
	"SelfDeclareExpense": {
							"DebtPayments": "",
							"MonthlyRent": "",
							"MonthlyExpenses": "",
							"creditCardBalances": ""
						  },
	"ApplicationDate":"",
	"ExpiryDate":""
}

// EmailAddress
{
	"Id":"",
	"Email":"",
	"EmailType":"",
	"IsDefault":
}

// PhoneNumber
{
	"PhoneId":"",
	"Phone":"",
	"CountryCode":"",
	"PhoneType":"",
	"IsDefault":
}

// Address
{
	"AddressId":"",
	"AddressLine1":"",
	"AddressLine2":"",
	"AddressLine3":"",
	"AddressLine4":"",
	"City":"",
	"State":"",
	"PinCode":"",
	"Country":"",
	"AddressType":"",
	"IsDefault":"",
	"LandMark":"",
	"Location":""
}

// BankInformation
{
	"BankId":"",
	"BankName":"",
	"AccountNumber":"",
	"AccountHolderName":"",
	"IfscCode":"",
	"AccountType":"Bank",
	"BankAddresses":{
	"AddressId":"",
	"AddressLine1":"",
	"AddressLine2":"",
	"AddressLine3":"",
	"AddressLine4":"",
	"City":"",
	"State":"",
	"PinCode":"",
	"Country":"",
	"AddressType":"",
	"IsDefault":"",
	"LandMark":"",
	"Location":""
	}
}

// EmploymentDetail
{
	"EmploymentId":"",
	"Name":"",
	"Addresses":[{}],
	"Designation":"",
	"WorkPhoneNumbers":[{}],
	"WorkEmailId":"",
	"LengthOfEmployment":"",
	"IncomeInformation":{},
	"EmploymentStatus":"",
	"AsOfDate":"",
	"CinNumber":""
}

// IncomeInformation
{
	"Income":"",
	"PaymentFrequency":""
}

//HighestEducationInformation
{
	"LevelOfEducation":"",
	"EducationalInstitution":""
}

//AncillaryData
{
	"Category":"",
	"Religion":"",
	"MothersMaidenName":"",
	"DocumentDate":""
}
//LoanFundedData
{
	"FundedDate":"",
	"FundedAmount":"",
	"FirstEmiDate":"",
	"EmiAmount":""
}

//UpdateApplicantRequest
{
	"AadhaarNumber":"",
	"DateOfBirth":"",
	"EmailAddress":"",
	"EmploymentStatus":"",
	"FirstName":"",
	"Gender":"",
	"LastName":"",
	"MaritalStatus":"",
	"MiddleName":"",
	"PermanentAccountNumber":"",
	"Salutation":""
}

//IInitialOfferDefination
{
  "entityType": "application",
  "entityId": "0000066",
  "status": "Completed",
  "reasons": null,
  "offers": [
    {
      "offerId": "c6ad06d83ad84d26b5e34ad1e13736e0",
      "fileType": "Thin",
      "interestRate": 17,
      "loanTenure": 36,
      "score": 35.95,
      "offerAmount": 350604.31,
      "finalOfferAmount": 100000,
      "maxPermittedEMI": 12500,
      "netMonthlyIncome": 22000,
      "currentEMINMI": 0.11,
      "monthlyExpense": 22500,
      "isSelected": true,
      "rejectCode": null,
      "isManualOffer": false
    }
  ],
  "generatedOn": {
    "time": "2017-02-13T13:41:10.443324+05:30",
    "hour": "2017-02-13-13-h",
    "day": "2017-02-13-d",
    "week": "2017-07-w",
    "month": "2017-02-m",
    "quarter": "2017-1-q",
    "year": "2017-y"
  },
  "offerSelectionDate": "2017-02-13T13:41:46.036553+05:30",
  "isAvailable": true,
  "id": "58a16a2a9325850007b35c6b"
}

//IFinalOfferDefination
{
  "entityType": "application",
  "entityId": "0000128",
  "status": "Completed",
  "reasons": null,
  "finalOffers": [
    {
      "offerId": "983c924185c84430988ab1776675a8cc",
      "loantenure": "12",
      "offerAmount": 76111.32,
      "emi": 6343.14,
      "finalOfferAmount": 76000,
      "netMonthlyIncome": 42964.87,
      "currentEMINMI": 0.11,
      "maxPermittedEMI": 7000,
      "processingFee": 3044,
      "processingFeePercentage": 4,
      "calculatedProcessingFee": 3044.45,
      "interestRate": 17.11,
      "manualReview": false,
      "isSelected": false,
      "rejectCode": null,
      "isManualOffer": false
    },
    {
      "offerId": "a3ef49c0e22643509b5ad738680d986b",
      "loantenure": "24",
      "offerAmount": 137298.57,
      "emi": 4167.4,
      "finalOfferAmount": 100000,
      "netMonthlyIncome": 42964.87,
      "currentEMINMI": 0.11,
      "maxPermittedEMI": 7000,
      "processingFee": 3750,
      "processingFeePercentage": 3.75,
      "calculatedProcessingFee": 3750,
      "interestRate": 18.68,
      "manualReview": false,
      "isSelected": true,
      "rejectCode": null,
      "isManualOffer": false
    },
    {
      "offerId": "56afa49b74db42b69cd6ff1b8811eb2d",
      "loantenure": "36",
      "offerAmount": 182450.5,
      "emi": 2778.58,
      "finalOfferAmount": 100000,
      "netMonthlyIncome": 42964.87,
      "currentEMINMI": 0.11,
      "maxPermittedEMI": 7000,
      "processingFee": 3500,
      "processingFeePercentage": 3.5,
      "calculatedProcessingFee": 3500,
      "interestRate": 20.84,
      "manualReview": false,
      "isSelected": false,
      "rejectCode": null,
      "isManualOffer": false
    }
  ],
  "generatedOn": {
    "time": "2017-02-15T15:58:50.664708+05:30",
    "hour": "2017-02-15-15-h",
    "day": "2017-02-15-d",
    "week": "2017-07-w",
    "month": "2017-02-m",
    "quarter": "2017-1-q",
    "year": "2017-y"
  },
  "offerSelectionDate": "2017-02-15T16:05:30.828746+05:30",
  "isAvailable": true,
  "id": "58a42d62a5877b0009855a12"
}

//IActivities

[
  {
    "code": "BorrowerFollowup",
    "title": "Borrower Followup"
  },
  {
    "code": "CallFromBorrower",
    "title": "Call From Borrower"
  }
]
#**Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/status-management
```
{
  "entityTypes": {
    "application": {
      "checklist": [
        {
          "code": "bank-verification",
          "title": "Bank verification completed",
          "ruleName": "ruleForBankVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "employment-everification",
          "title": "Employment verification completed",
          "ruleName": "ruleForEmploymentVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "kyc-verification",
          "title": "KYC verification completed",
          "ruleName": "ruleForKYCVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "mobile-verification",
          "title": "Mobile verification completed",
          "ruleName": "ruleForMobileVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "personal-email-verification",
          "title": "Personal email verification completed",
          "ruleName": "ruleForPersonalEmailVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "residence-everification",
          "title": "residence verification completed",
          "ruleName": "ruleForResidenceVerificationForChecklist",
          "ruleVersion": "1.0"
        }
      ],
      "statuses": [
        {
          "code": "200.01",
          "name": "Lead Created",
          "label": "Lead",
          "style": "Lead",
          "transitions": [
            "200.02",
            "200.13",
            "200.15",
            "200.16"
          ]
        },
        {
          "code": "200.02",
          "name": "Application Submitted",
          "label": "Application",
          "style": "Application",
          "checklist": [
            "mobile-verification"
          ],
          "transitions": [
            "200.03",
            "200.13",
            "200.15",
            "200.16"
          ],
          "activities": {
            "UploadDocument": "Upload Document",
            "Need followup": "Need followup"
          }
        },
        {
          "code": "200.03",
          "name": "Initial Offer Given",
          "label": "Offer Given",
          "style": "Offer Given",
          "transitions": [
            "200.04",
            "200.13",
            "200.14",
            "200.15"
          ],
          "activities": {
            "UploadDocument": "Upload Document",
            "Need followup": "Need followup"
          }
        },
        {
          "code": "200.04",
          "name": "Initial Offer Selected",
          "label": "Offer Selected",
          "style": "Offer Selected",
          "transitions": [
            "200.05",
            "200.06",
            "200.13",
            "200.14",
            "200.15",
            "200.16"
          ]
        },
        {
          "code": "200.05",
          "name": "Credit Review",
          "label": "Manual UW",
          "style": "Manual UW",
          "transitions": [
            "200.06",
            "200.13",
            "200.15",
            "200.16"
          ],
          "reasons": {
            "CompUnclass": "Company Unclassified",
            "OtherPurpose": "Other Loan Purpose",
            "HigherMaxCredit": "Monthly Income Max-Credit MisMatch",
            "ThinLowScore": "Thin File Low Score",
            "NTCLowScore": "NTC File Low Score",
            "ThickLowScore": "Thick File Low App and Bureau Score",
            "ThickAppBureauMisMatch": "Thick File  App and Bureau Score MisMatch",
            "AddressUnVerified": "Bureau Address Verification Failed",
            "HighIncome": "Higher Monthly Income"
          }
        },
        {
          "code": "200.06",
          "name": "Final offer made",
          "label": "Offer Ready",
          "style": "Offer Ready",
          "transitions": [
            "200.07",
            "200.13",
            "200.14",
            "200.15",
            "200.16"
          ]
        },
        {
          "code": "200.07",
          "name": "Final offer Accepted",
          "label": "Agreement/KYC",
          "style": "Agreement/KYC",
          "transitions": [
            "200.08",
            "200.13",
            "200.14",
            "200.15",
            "200.16"
          ]
        },
        {
          "code": "200.08",
          "name": "CE Approved",
          "label": "Approved",
          "style": "Approved",
          "checklist": [
            "employment-verification",
            "residence-verification",
            "bank-verification",
            "kyc-verification",
            "personal-email-verification"
          ],
          "transitions": [
            "200.09",
            "200.13",
            "200.16"
          ]
        },
        {
          "code": "200.09",
          "name": "RBL Fraud",
          "label": "RBL Fraud Review",
          "style": "RBL Fraud Review",
          "transitions": [
            "200.10",
            "200.13",
            "200.16"
          ]
        },
        {
          "code": "200.10",
          "name": "RBL Credit",
          "label": "RBL Credit Review",
          "style": "RBL Credit Review",
          "transitions": [
            "200.11",
            "200.13",
            "200.16"
          ]
        },
        {
          "code": "200.11",
          "name": "Approved",
          "label": "Approved",
          "style": "Approved ",
          "transitions": [
            "200.12"
          ]
        },
        {
          "code": "200.12",
          "name": "Funded",
          "label": "Funded",
          "style": "Funded",
          "transitions": []
        },
        {
          "code": "200.13",
          "name": "Cancelled",
          "label": "Cancelled",
          "style": "Cancelled",
          "transitions": [],
          "reasons": {
            "r2": "Borrower Initiated",
            "r3": "CE Initiated",
            "r4": "Bank Initiated"
          }
        },
        {
          "code": "200.14",
          "name": "Not Interested",
          "label": "Not Interested",
          "style": "Not Interested ",
          "transitions": [],
          "reasons": {
            "r5": "Don't need the loan",
            "r6": "Interest too high",
            "r7": "Approved amount too low",
            "r8": "Competitor offered better terms"
          }
        },
        {
          "code": "200.15",
          "name": "Expired",
          "label": "Expired",
          "style": "Expired",
          "transitions": [],
          "reasons": {
            "r9": "Auto expiry"
          }
        },
        {
          "code": "200.16",
          "name": "Rejected",
          "label": "Declined",
          "style": "Declined",
          "transitions": [],
          "IsRejectionStatus": "true",
          "reasons": {
            "BureauScoreLow": "Low Bureau Score",
            "HighInquiries": "Too Many Inquiries",
            "BureauLoanDerog": "Derogatory Loan",
            "CCBalToIncomeHigh": "High Credit Card Balance",
            "HighDPDTrade": "Higher Number of Deliquencies",
            "IncomeLowL3": "Low Income",
            "IncomeLowL2": "Low Income",
            "ReportedEMILow": "EMI Inconsistent",
            "RBLLiveLoan": "RBL Live Loan",
            "RBLDefaultLoan": "RBL Default Loan",
            "RBLInquiryRule": "RBL Inquiry in File",
            "RBLDPDRule1": "RBL DPD",
            "RBLOverdueRule": "RBL Overdue",
            "RBLDPDRule2": "RBL DPD",
            "RBLDPDRule3": "RBL DPD",
            "LoanPurposeCheck": "Loan Purpose not Supported",
            "CitySupported2": "Unsupported Geo Region",
            "EmpNotExist": "Employer Not in Database",
            "IncomeMismatch": "Income Mismatch with Bank Statement",
            "LoanAmtMismatch": "LoanAmount Mismatch with Credit File",
            "EmpTypeNotSupported": "Employment Type Not Supported",
            "InvalidPANNumber": "Invalid PAN Number",
            "HighDTILowIncomeL1": "Higher Debt Compared to Income",
            "CCBalanceRatioHigh": "High Credit Card Balance",
            "HighRiskZipRule1": "High Risk PIN Code",
            "HighRiskZipRule2": "High Risk PIN Code",
            "HighRiskZipRule3": "High Risk PIN Code",
            "HighRiskZipRule4": "High Risk PIN Code",
            "PANNotVerfied": "Unable to Verify PAN",
            "r10": "CE decline",
            "r11": "Bank decline"
          }
        }
      ]
    }
  }
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/offer-engine
```
{
  "reApplyStatuseCodes": [
    "200.13",
    "200.14",
    "200.15",
    "200.16"
  ],
  "statuses": {
    "ApplicationSubmitted": "200.02",
    "LeadCreated": "200.01",
    "InitialOfferGiven": "200.03",
    "InitialOfferSelected": "200.04",
    "NotInterested": "200.14",
    "Finaloffermade": "200.06",
    "FinalofferAccepted": "200.07",
    "Rejected": "200.16",
    "CEApproved": "200.08",
    "Approved": "200.11",
    "BankCreditReview": "200.10",
    "ManualReview": "200.05"
  },
  "reApplyTimeFrame": "6",
  "employmentChangeStatus": [
    "200.02",
    "200.01",
    "200.03",
    "200.04",
    "200.05"
  ],
  "expenseChangeStatus": [
    "200.02",
    "200.01"
  ],
  "updatePersonalDetails": [
    "200.02",
    "200.01"
  ],
  "primaryCreditBureau": "CIBIL",
  "secondaryCreditBureau": "CRIF",
  "bankChangeStatus": [
    "200.02",
    "200.01",
    "200.03",
    "200.04",
    "200.05",
    "200.06",
    "200.07"
  ],
  "addressChangeStatus": [
    "200.02",
    "200.01"
  ],
  "EnableEmailHunter": true
}
```

