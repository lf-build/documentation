##### Availalbe End Points:
	
1. **Send email with documents from document manager as attachment**
  
 **Description** : Prepares mail with given template and attachments from document manager and sends it using provider configured

   **HTTP Verb**: POST

   **Route** : "{entitytype}/{entityid}/{templateName}/{templateVersion}/{*attachmentids}"
   ```
   - Request: POST("{entitytype}/{entityid}/{templateName}/{templateVersion}/{*attachmentids}")    
   - Request Schema:  {							     
                                        	"Name" : "Jig Jani",     
	                                     "Email" : "jig.jani@xyz.com" 
				    }    
   - Response:-// SendEmailResult object

   - Response Schema:  { "EmailReferenceNumber" : "9ee33a8e-9b4a-11e6-a40f-00163ef91450" } 
   
2. **Send email with documents as attachment**

   **Description** : Prepares mail with given template and attachments uploaded by the caller of endpoint and sends it using provider configured

   **HTTP Verb**: POST

   **Route** : "{entitytype}/{entityid}/{templateName}/{templateVersion}/attachment"
   ```
   - Request: POST("{entitytype}/{entityid}/{templateName}/{templateVersion}/attachment")

   - Request Schema:  {
                                         "Name" : "Jig Jani",     
	                                  "Email" : "jig.jani@xyz.com"   
                                    }  

   - Response :- // SendEmailResult object
			
    - Response Schema:  { "EmailReferenceNumber" : "9ee33a8e-9b4a-11e6-a40f-00163ef91450" } 
   
3. **Send Email with free flowing text as body instead of a template**

   **Description** : Sends email using given email body and metadata using provider configured

   **HTTP Verb**: POST
   **Route** : "{entitytype}/{entityid}"
   ```
   - Request: POST("{entitytype}/{entityid}")

   - Request Schema:  {
                                         "ToName" : "Jigs Jani",     
	                                 "ToAddress" : "jig.jani@xyz.com" ,
	                                 "Subject" : "Testing custom endpoint",
	                                 "Body" : "Hi, This is just to test custom emails sent from ce email service."                                     
                                }

  
   - Response :- //SendEmailResult object

   - Response Schema:  { "EmailReferenceNumber" : "9ee33a8e-9b4a-11e6-a40f-00163ef91450" } 	
   
4. **Send Email with free flowing text as body instead of a template along with custom attachments**

   **Description** : Sends email using given email body and metadata along with attachments using provider configured

   **HTTP Verb**: POST
   **Route** : "{entitytype}/{entityid}/attachment"
   ```
   - Request: POST("{entitytype}/{entityid}/attachment")

   - Request Schema:  {                                         
                                        	"ToName" : "Jigs Jani",     
                                        	"ToAddress" : "jig.jani@xyz.com" ,
                                        	"Subject" : "Testing custom endpoint",
                                        	"Body" : "Hi, This is just to test custom emails sent from ce email service."                                        	
                                  }
  
   - Response :- //SendEmailResult object

   - Response Schema:  { "EmailReferenceNumber" : "9ee33a8e-9b4a-11e6-a40f-00163ef91450" } 

 5. ** To use from Identity service**

   **Description** : Send Email using template and provided metadata using configured provider

   **HTTP Verb**: POST
   **Route** : "{templateName}/{templateVersion}"
   ```
   - Request: POST("{templateName}/{templateVersion}")

   - Request Schema:  {                                         
                                        	"ToName" : "Jigs Jani",     
                                        	"ToAddress" : "jig.jani@xyz.com"                                         	
                                  }
  
   - Response :- //bool value indicating whether email sent successfully or not

   - Response Schema: { true } 


### Schema 
// No separate schema to mention

```
### **Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/email
```
{
  "emailProviderSettings": {
    "SendGrid": {
      "ApiKey": "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",
      "FromName": "LendFoundry Development",
      "FromAddress": "development@lendfoundry.com"
    }
  },
  "emailProvider": "SendGrid"
}```
