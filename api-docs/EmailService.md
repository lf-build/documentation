##### Availalbe End Points:

1. Send Email with specific template to a recipient.

   ```
   - [HttpPost("{entitytype}/{entityid}/{eventname}/{templateName}/{templateVersion}")]
   - Body: ([FromRoute] string entityType,[FromRoute] string entityId,[FromRoute] string templateName,
                                                   [FromRoute] string templateVersion,[FromBody] object data)
              - Response: Task<SendEmailResult>
   ```

2. Send custom Email to a recipient.

   ```
   - [HttpPost("{entitytype}/{entityid}")]
   - Body: ([FromRoute] string entityType,[FromRoute] string entityId,[FromBody] EmailData emailData)
              - Response: Task<SendEmailResult>
   ```

### Schema:

```
// SendEmailResult
{
    "EmailReferenceNumber":"guid value"
}
```

```
// EmailData
{
    "ToAddress":"siddharth.p@sigmainfo.net",
    "ToName":"siddharth",
    "Subject":"Email Subjet",
    "Body":"Demo Email Body"
}
```

#**Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/email
   ```
   {
       "EmailProvider": "SendGrid",
       "EmailProviderSettings": {
    	   "SendGrid": {
	     	   "ApiKey": "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",
      		   "FromName": "LendFoundry Development",
      		   "FromAddress": "development@lendfoundry.com"
    	   }
       }
   }
   ```
