##### Availalbe End Points:
	
1. **Facts Verification**
   **Description** : Verify fact by providing entityType, entityId, fact, method, source and request object
   **HTTP Verb**: POST
   **Route** : "{entityType}/{entityId}/{fact}/{method}/{source}"
   ```
   - Request: POST("{entityType}/{entityId}/{fact}/{method}/{source}")    
   - Request Schema:  {
							"PANNo" : "45345345",
							"FirstName" : "Test",
							"LastName" : "Test",
							"DOB" : "2016-09-02T00:00:00-04:00"
					  }    
   - Response:-// VerificationMethodObject
   - Response Schema:  {	
							"EntityType" : "application",
							"EntityId" : "2",
							"FactName" : "PANVerification",
							"MethodName" : "Syndication",
							"VerificationSources" : [ 
								{
									"_t" : "CreditExchange.VerificationEngine.VerificationSources, CreditExchange.VerificationEngine.Abstractions",
									"SourceName" : "CRIFPANVerifcation",
									"Source" : null,
									"DataExtracted" : "null",
									"DataExtractedDate" : {
															"DateTime" : ISODate("2016-11-08T14:31:56.851Z"),
															"Ticks" : NumberLong(636141943168514646),
															"Offset" : -300
														   }
								}
							],
							"Attempts" : [ 
								{
									"_t" : "CreditExchange.VerificationEngine.MethodAttemptResult, CreditExchange.VerificationEngine.Abstractions",
									"Result" : "UnableToSpecify",
									"DateAttempt" : {
														"DateTime" : Date(-62135596800000),
														"Ticks" : NumberLong(0),
														"Offset" : 0
													},
									"VerifiedBy" : null
								}
							]
						}
   ``` 
   
2. **Get Verification Status**
   **Description** : Get Verification fact Status by providing entityType, entityId, fact
   **HTTP Verb**: GET
   **Route** : "{entityType}/{entityId}/{fact}/status"
   ```
   - Request: GET("{/{entityType}/{entityId}/{fact}/status")
  
   - Response :- // VerificationFactsObject  			
   - Response Schema:  {	
							"EntityId" : "2",
							"EntityType" : "application",
							"FactName" : "PANVerification",
							"CurrentStatus" : "InProgess",
							"VerificationStatus" : "Failed",
							"LastVerifiedDate" : {
							"DateTime" : ISODate("2016-11-08T14:36:14.954Z"),
							"Ticks" : NumberLong(636141945749544700),
							"Offset" : -300
							},
							"LastVerifiedBy" : null
						}
   

3. **Get Verification Detail**: 
   **Description** : Get Verification Detail by providing entityType, entityId, fact
   **HTTP Verb**: GET
   **Route** : "{entityType}/{entityId}/{fact}/detail"
   ```
   - Request: GET("{entityType}/{entityId}/{fact}/detail")
   
   - Response:  // List of VerificationMethodObject
   - Response Schema:  {	
							"EntityType" : "application",
							"EntityId" : "2",
							"FactName" : "PANVerification",
							"MethodName" : "Syndication",
							"VerificationSources" : [ 
								{
									"SourceName" : "CRIFPANVerifcation",
									"Source" : null,
									"DataExtracted" : "null",
									"DataExtractedDate" : {
															"DateTime" : ISODate("2016-11-08T14:31:56.851Z"),
															"Ticks" : NumberLong(636141943168514646),
															"Offset" : -300
														   }
								}
							],
							"Attempts" : [ 
								{
									"_t" : "CreditExchange.VerificationEngine.MethodAttemptResult, CreditExchange.VerificationEngine.Abstractions",
									"Result" : "UnableToSpecify",
									"DateAttempt" : {
														"DateTime" : Date(-62135596800000),
														"Ticks" : NumberLong(0),
														"Offset" : 0
													},
									"VerifiedBy" : null
								}
							]
						}
   ```
   
4. **Get Verification**: 
   **Description** : Get Verified Facts by providing entityType, entityId
   **HTTP Verb**: GET
   **Route** : "{entityType}/{entityId}"
   ```
   - Request: GET("{entityType}/{entityId}")
   
   - Response:  // FactResponseObject
   - Response Schema:  {	
							"EntityId" : "2",
							"EntityType" : "application",
							"FactName" : "PANVerification",
							"CurrentStatus" : "InProgess",
							"VerificationStatus" : "Failed",
							"LastVerifiedDate" : {
								"DateTime" : ISODate("2016-11-08T14:36:14.954Z"),
								"Ticks" : NumberLong(636141945749544700),
								"Offset" : -300
							},
							"LastVerifiedBy" : null
							"VerificationSources" : [ 
								{
									"SourceName" : "CRIFPANVerifcation",
									"Source" : null,
									"DataExtracted" : "null",
									"DataExtractedDate" : {
															"DateTime" : ISODate("2016-11-08T14:31:56.851Z"),
															"Ticks" : NumberLong(636141943168514646),
															"Offset" : -300
														   }
								}
							],
							"Attempts" : [ 
								{
									"Result" : "UnableToSpecify",
									"DateAttempt" : {
														"DateTime" : Date(-62135596800000),
														"Ticks" : NumberLong(0),
														"Offset" : 0
													},
									"VerifiedBy" : null
								}
							]													
						}
   ```
5. **Get Verification Facts**: 
   **Description** : Get List Of Facts To Be Verified For entityType From Configurations
   **HTTP Verb**: GET
   **Route** : "{entityType}"
   ```
   - Request: GET("{entityType}")
  
   - Response:  // Fact Object
   - Response Schema:  {	
							"Name" : "PANVerification",
							"MaxRetryAttempts" : "2",							
							"Method" : [ 
								{
									"Name" : "PANVerification",
									"Source" : null,
									"VerificationRule" :{
															"Name":"PANVerification",
															"Version":"3"
														}
								}
							],													
						}
   ```
   
6. **Initiate Document Request**: 
   **Description** : Request User To Upload Document For Verification by providing entityType, entityId, fact, method, source
   **HTTP Verb**: POST
   **Route** : "{entityType}/{entityId}/{fact}/{method}/{source}/request-document"
   ```
   - Request: POST("{entityType}/{entityId}/{fact}/{method}/{source}/request-document")
		
   - Response:  // NoContentResult
   ```   
7. **Receive Verification Document**: 
   **Description** : Upload verification document entityType, entityId
   **HTTP Verb**: POST
   **Route** : "{entityType}/{entityId}/{fact}/{method}/{source}/{category}/upload-document"
   ```
   - Request: POST("{entityType}/{entityId}/{fact}/{method}/{source}/{category}/upload-document")
  
   - Response:  // ApplicationDocument Object
   - Response Schema:  {
						  "applicantId": null,
						  "applicationId": "3",
						  "category": "pancard",
						  "documentId": "6703794af0aa4b6baf9f0a566d0a19d7",
						  "fileName": "Test.pdf",
						  "stipulationType": "PANVerification",
						  "uploadedDate": "2016-11-15T08:35:10.185331-05:00",
						  "verificationStatus": 0,
						  "verifiedDate": "0001-01-01T00:00:00+00:00",
						  "verifiedBy": null,
						  "verificationResult": 0,
						  "rejectReason": null,
						  "id": "582b0f0eaf8caf0006cf6631"
						}
   ```
8. **Pending Document**: 
   **Description** : Get Pending Document List For Given EntityId by providing entityType, entityId
   **HTTP Verb**: GET
   **Route** : "{entityType}/{entityId}/pending-document"
   
   - Request: GET("{{entityType}/{entityId}/pending-document")
 	
   - Response :- // VerificationFactsObject  			
   - Response Schema:  {	
							"EntityId" : "2",
							"EntityType" : "application",
							"FactName" : "PANVerification",
							"CurrentStatus" : "InProgess",
							"VerificationStatus" : "Failed",
							"LastVerifiedDate" : {
								"DateTime" : ISODate("2016-11-08T14:36:14.954Z"),
								"Ticks" : NumberLong(636141945749544700),
								"Offset" : -300
							},
							"LastVerifiedBy" : null
						}
   ```   
 
9. **Get Required Documet**: 
   **Description** : Get Required Document To Be Uploaded For Verification For Fact by providing entityType, fact
   **HTTP Verb**: GET
   **Route** : "{entityType}/{entityId}/pending-document"
   ```
   - Request: GET("{{entityType}/{entityId}/pending-document")
 	
   - Response :- // RequiredDocumentResponseObject  			
   - Response Schema:  {	
							"MethodName":"PANVerification",
							"Documents"{"abc","xyz"}
						}
   ```  
   
### Schema

```
// VerificationMethod
{	
	"EntityType" : "application",
	"EntityId" : "2",
	"FactName" : "PANVerification",
	"MethodName" : "Syndication",
	"VerificationSources" : [ 
								{
									"_t" : "CreditExchange.VerificationEngine.VerificationSources, CreditExchange.VerificationEngine.Abstractions",
									"SourceName" : "CRIFPANVerifcation",
									"Source" : null,
									"DataExtracted" : "null",
									"DataExtractedDate" : {
															"DateTime" : ISODate("2016-11-08T14:31:56.851Z"),
															"Ticks" : NumberLong(636141943168514646),
															"Offset" : -300
														   }
								}
							],
							"Attempts" : [ 
								{
									"_t" : "CreditExchange.VerificationEngine.MethodAttemptResult, CreditExchange.VerificationEngine.Abstractions",
									"Result" : "UnableToSpecify",
									"DateAttempt" : {
														"DateTime" : Date(-62135596800000),
														"Ticks" : NumberLong(0),
														"Offset" : 0
													},
									"VerifiedBy" : null
								}
							]
}

// VerificationFacts
{	
	"EntityId" : "2",
	"EntityType" : "application",
	"FactName" : "PANVerification",
	"CurrentStatus" : "InProgess",
	"VerificationStatus" : "Failed",
	"LastVerifiedDate" : {
								"DateTime" : ISODate("2016-11-08T14:36:14.954Z"),
								"Ticks" : NumberLong(636141945749544700),
								"Offset" : -300
						 },
	"LastVerifiedBy" : null
}

// FactResponse
{	
	"EntityId" : "2",
	"EntityType" : "application",
	"FactName" : "PANVerification",
	"CurrentStatus" : "InProgess",
	"VerificationStatus" : "Failed",
	"LastVerifiedDate" : {
								"DateTime" : ISODate("2016-11-08T14:36:14.954Z"),
								"Ticks" : NumberLong(636141945749544700),
								"Offset" : -300
						 },
	"LastVerifiedBy" : null
	"VerificationSources" : [ 
								{
									"SourceName" : "CRIFPANVerifcation",
									"Source" : null,
									"DataExtracted" : "null",
									"DataExtractedDate" : {
															"DateTime" : ISODate("2016-11-08T14:31:56.851Z"),
															"Ticks" : NumberLong(636141943168514646),
															"Offset" : -300
														   }
								}
							],
	"Attempts" : [ 
					{
							"Result" : "UnableToSpecify",
							"DateAttempt" : {
												"DateTime" : Date(-62135596800000),
												"Ticks" : NumberLong(0),
												"Offset" : 0
											},
							"VerifiedBy" : null
						}
					]													
}


// Fact
{	
	"Name" : "PANVerification",
	"MaxRetryAttempts" : "2",							
	"Method" : [ 
					{
						"Name" : "PANVerification",
						"Source" : null,
						"VerificationRule" :{
												"Name":"PANVerification",
												"Version":"3"
											}
					}
				],													
}
```

### **Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/verification-engine
```
{
  "facts": {
    "application": [
      {
        "Name": "MobileVerification",
        "MaxRetryAttempts": 1,
        "Methods": [
          {
            "Name": "MobileSMS",
            "Sources": [
              {
                "Name": "MobileSMS",
                "Type": "Automatic"
              }
            ]
          }
        ]
      },
      {
        "Name": "EmailVerification",
        "MaxRetryAttempts": 1,
        "Methods": [
          {
            "Name": "Email",
            "Sources": [
              {
                "Name": "Email",
                "Type": "Automatic"
              }
            ]
          }
        ]
      },
      {
        "Name": "PANVerification",
        "MaxRetryAttempts": 1,
        "Methods": [
          {
            "Name": "PanSyndication",
            "Sources": [
              {
                "Type": "Syndication",
                "Name": "CRIFPANVerifcation",
                "Settings": {},
                "DataExtractRule": {
                  "Name": "PANVerificationDataExtractRule",
                  "Version": "1.0"
                }
              }
            ],
            "VerificationRule": {
              "Name": "PANVerificationRule",
              "Version": "2.0"
            }
          },
		}
		]
	}
}
```

3. Configurations for {{default_host}}:{{lookupservice_port}}/applicationDocument-category
```
{
   "pancard":"PanCard",
   "bankstatement" : "BankStatement",
     "payslip" : "Payslip",
     "itr": "ITR",
     "form16" : "Form16",
     "physicalverification" : "PhysicalVerification",
     "aadhaarcard": "AadhaarCard",
     "passport": "Passport",
     "driverslicense": "DriversLicense"
}
```

4. Configurations for {{default_host}}:{{lookupservice_port}}/applicationDocument-stipulationType
```
{
    "MobileVerification":  "MobileVerification",
    "EmailVerification":  "EmailVerification",
    "PANVerification":  "PANVerification",
    "BankVerification":  "BankVerification",
    "IncomeVerification":  "IncomeVerification",
    "SocialVerification":  "SocialVerification",
    "LocationVerification":  "LocationVerification",
    "FraudVerification":  "FraudVerification",
    "EmploymentVerification" : "EmploymentVerification",
    "ID(KYC)Verification":  "ID(KYC)Verification"
}
```
