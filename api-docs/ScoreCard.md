##### Available End Points:
	
1. Get Score Card Configurations based on enntity Type and productId

   ```
   - Get("{entityType}/{productId}/configurations")   
   - Response: (CaseInsensitiveDictionary<CaseInsensitiveDictionary<List<ScoreCardDetail>>>)
   ``` 
2. Run the Rule base on the configuration details

   ```
   - Post("{entityType}/{entityId}/{productId}/{ruleType}/{group}/{rule?}")   
   - Response: (List<ScoreCardRuleResult>)
   ```
3. Get ScoreCard Details based in EntityType

   ```
   - Get("{entityType}/{entityId}")
   - Response: (List<IScoreCardRuleResult>)
   ```

### Schema

```

// ScoreCardRuleResult
[
  {
    "entityType": "",
    "entityId": "0000154",
    "name": "",   
    "result": true,
    "resultDetail": null,
    "intermediateData": [],
    "exceptionDetail": null   
  }
]
```

### Configurations

```
1. Configurations for ScoreCard

{
  "scorecard": {
    "application": {
      "Product1": {
        "EligibilityCriteria": {
          "Prequalification": [
            {
              "Name": "ScoreCardEligibilityRule",
              "Sequence": 1,
              "RequiredSources": [
                "application"
              ],
              "Rule": {
                "Name": "initialoffer_execute_eligibility_rules",
                "Version": "1.0"
              }
            },
            {
              "Name": "CrifPanVerificationRule",
              "Sequence": 1,
              "RequiredSources": [
                "crifPanReport"
              ],
              "Rule": {
                "Name": "initialoffer_verify_pan",
                "Version": "1.0"
              }
            },
            {
              "Name": "GeoRiskEligibility",
              "Sequence": 1,
              "RequiredSources": [
                "cibilReport",
                "geoRiskPinCode",
                "application"
              ],
              "Rule": {
                "Name": "initialoffer_georiskclassification_eligibility",
                "Version": "1.0"
              }
            },
            {
              "Name": "CreditEligibilityCheck",
              "Sequence": 1,
              "RequiredSources": [
                "application",
                "cibilReport"
              ],
              "Rule": {
                "Name": "initialoffer_credit_eligibility",
                "Version": "1.0"
              }
            }
          ]
        },
        "ScoringCritiria": {
          "ScoringRules": [
            {
              "Name": "FinalOfferScoreCalculation",
              "Sequence": 1,
              "RequiredSources": [
                "application",
                "cibilReport",
                "perfiosReport",
                "companyCategoryReport"
              ],
              "OptionalSources": [
                "clientScoreCard"
              ],
              "PreRequisiteRules": [
                "CalculateInitialOffer",
                "GeoRiskEligibility"
              ],
              "Rule": {
                "Name": "final_offer_score_calculation_rule",
                "Version": "1.0"
              }
            }
          ]
        },
        "ManualReviewCritiria": {
          "ManualReviewRules": [
            {
              "Name": "ScoringRule1",
              "Sequence": 1,
              "PreRequisiteRule": [
                "Rule1",
                "Rule"
              ]
            },
            {
              "Name": "ScoringRule2",
              "Sequence": 1,
              "PreRequisiteRule": [
                "Rule1",
                "Rule"
              ]
            }
          ]
        },
        "OfferCritiria": {
          "InitialOfferCalculation": [
            {
              "Name": "CalculateInitialOffer",
              "PreRequisiteRules": [
                "GeoRiskEligibility"
              ],
              "RequiredSources": [
                "application",
                "cibilReport"
              ],
              "OptionalSources": [
                "companyCategoryReport"
              ],
              "Rule": {
                "Name": "initialoffer_compute",
                "Version": "1.0"
              }
            }
          ],
          "FinalOfferCalculation": [
            {
              "Name": "FinalOfferCalculation",
              "PreRequisiteRules": [
                "FinalOfferScoreCalculation"
              ],
              "RequiredSources": [
                "perfiosReport",
                "application",
                "cibilReport",
                "companyCategoryReport"
              ],
              "Rule": {
                "Name": "final_offer_calculation_rule",
                "Version": "1.0"
              }
            }
          ]
        }
      }
    }
  },
  "application": [
    {
      "eventName": "ApplicationCreated",
      "dataGroupName": "application",
      "entityId": "{Data.Application.ApplicationNumber}",
      "rule": {
        "Name": "data_attribute_applicationcreated",
        "Version": "1.0"
      },
      "EventsToBePublish": [
        "Test1",
        "Test2"
      ]
    },
    {
      "eventName": "ApplicationModified",
      "dataGroupName": "application",
      "entityId": "{Data.Application.ApplicationNumber}",
      "rule": {
        "Name": "data_attribute_applicationcreated",
        "Version": "1.0"
      }
    },
    {
      "eventName": "CompanyDbCategoryRequested",
      "dataGroupName": "companyCategoryReport",
      "entityId": "{Data.EntityId}",
      "rule": {
        "Name": "data_attribute_companycategorydatarequested",
        "Version": "1.0"
      }
    },
    {
      "eventName": "CrifCreditReportPulled",
      "dataGroupName": "crifCreditReport",
      "entityId": "{Data.EntityId}",
      "rule": {
        "Name": "data_attribute_creditcreditreportpulled",
        "Version": "1.0"
      }
    },
    {
      "eventName": "PanVerificationRequested",
      "dataGroupName": "crifPanReport",
      "entityId": "{Data.EntityId}",
      "rule": {
        "Name": "data_attribute_crifpanverificationrequested",
        "Version": "1.0"
      },
      "EventsToBePublish": [
        "CrifPanAttributesUpdated"
      ]
    },
    {
      "eventName": "GeoRiskPinCodeVerified",
      "dataGroupName": "geoRiskPinCode",
      "entityId": "{Data.EntityId}",
      "rule": {
        "Name": "data_attribute_georiskpincodeverified",
        "Version": "1.0"
      }
    },
    {
      "eventName": "LenddoSocialScoreRequested",
      "dataGroupName": "clientScoreCard",
      "entityId": "{Data.EntityId}",
      "rule": {
        "Name": "data_attribute_lendosocialscorerequested",
        "Version": "1.0"
      },
      "EventsToBePublish": [
        "LenddoAttributesUpdated"
      ]
    },
    {
      "eventName": "PerfiosXmlReportPulled",
      "dataGroupName": "perfiosReport",
      "entityId": "{Data.EntityId}",
      "rule": {
        "Name": "data_attribute_perfiosreportpulled",
        "Version": "1.0"
      }
    },
    {
      "eventName": "ZumigoValidatedOnlineTransaction",
      "dataGroupName": "zumigoReport",
      "entityId": "{Data.EntityId}",
      "rule": {
        "Name": "data_attribute_zumigovalidationonlinetransaction",
        "Version": "1.0"
      },
      "EventsToBePublish": [
        "ValidatedOnlineTransaction"
      ]
    },
    {
      "eventName": "cibilreportpulled",
      "dataGroupName": "cibilReport",
      "entityId": "{Data.EntityId}",
      "rule": {
        "Name": "data_attribute_cibilreportpulled",
        "Version": "1.0"
      }
    }
  ]
}
```