##### Availalbe End Points:

1. **Add Device**  
   **Description:** Registers an MDN Request by providing entitytype, entityid and mobileDeviceNumber  
   **HTTP Verb:** POST  
   **Route:**  "device/{entitytype}/{entityid}/{mobileDeviceNumber}"  
   ```
   - Request: POST("device/{entitytype}/{entityid}/{mobileDeviceNumber}")    
  
   - Response:-// Add Device Response 
    {
      "expiration": null,
      "data": null,
      "name": "zumigo",
      "entityId": "1",
      "entityType": "application",
      "syndicationData": null,
      "syndicationName": "zumigo",
      "referenceNumber": "1408556dba97480aa342aebea77a98e2",
      "request": "919900922008",
      "response": "MDN_OPTED_IN"
    } 
   ``` 
2. **Delete Device**  
   **Description:** Removes the registration of an MDN  by providing entitytype, entityid and mobileDeviceNumber  
   **HTTP Verb:** DELETE  
   **Route:**  "device/{entitytype}/{entityid}/{mobileDeviceNumber}"  
   ```
   - Request: DELETE("device/{entitytype}/{entityid}/{mobileDeviceNumber}")    
  
   - Response:-// Delete Device Response 
      Http Status - 204 (No Content)
   ``` 
3. **Get Device Status**  
   **Description:** Returns the current OptIn status of an MDN  by providing entitytype, entityid and mobileDeviceNumber  
   **HTTP Verb:** GET  
   **Route:**  "device/{entitytype}/{entityid}/{mobileDeviceNumber}/status"  
   ```
   - Request: GET("device/{entitytype}/{entityid}/{mobileDeviceNumber}/status")    
  
   - Response:-// Get Device Status Response 
      {
          "status":"MDN_OPTED_IN",
          "referenceNumber": "f09fe4bac8564262b6b8bf319b664c7c"
      }
   ``` 
4. **Get MobileLocation And Address**  
   **Description:** Returns latitude/longitude coordinates and street address of an MDN  by providing entitytype, entityid and mobileDeviceNumber  
   **HTTP Verb:** GET  
   **Route:**  "device/{entitytype}/{entityid}/{mobileDeviceNumber}/location"  
   ```
   - Request: GET("device/{entitytype}/{entityid}/{mobileDeviceNumber}/location")    
  
   - Response:-// Get Device Location and Address Response 
      {
          "mobileDeviceNumber":"919845074029",
          "latitude": "38.83456",
          "longitude":"-94.663995",
          "address":"2001 Gateway Place, San Jose,CA, 95110",
          "accuracy":"1000",
          "iimestamp":"2015-04-23T12:41:16.000-0700",
          "referenceNumber":"f09fe4bac8564262b6b8bf319b664c7c"
      }
   ```  
5. **Validate Online Transaction**  
   **Description:** Call compares one or more of the following: IP, latitude/longitude, an
     address, wifi location(s) to the location of the MDN  by providing entitytype, entityid and mobileDeviceNumber  
   **HTTP Verb:** GET  
   **Route:**  "device/{entitytype}/{entityid}/{mobileDeviceNumber}/location/is-validcation"  
   ```
   - Request: GET("device/{entitytype}/{entityid}/{mobileDeviceNumber}/location/is-valid")    
  
   - Response:-// validate online transaction response Response 
      {
          "distanceFromIp":"4753.222177650772",
          "distanceFromLatitudeLongitude": "1073.0103452006858",
          "distanceFromWifi":"-94.663995",
          "distanceFromAddress":null,
          "averageDistance":null,
          "referenceNumber":"f09fe4bac8564262b6b8bf319b664c7c"
      }
   ```  
##### Schema
   //  Add Device Response 

   ```
    {
       "expiration": null,
      "data": null,
      "name": "zumigo",
      "entityId": "1",
      "entityType": "application",
      "syndicationData": null,
      "syndicationName": "zumigo",
      "referenceNumber": "1408556dba97480aa342aebea77a98e2",
      "request": "919900922008",
      "response": "MDN_OPTED_IN"
     }

   ```  
   //  Get Device Status Response

   ```
    {
      "status":"MDN_OPTED_IN",
      "referenceNumber": "f09fe4bac8564262b6b8bf319b664c7c"
    }

   ```   
    //  Get Device Location and Address Response
   ```
     {
          "mobileDeviceNumber":"919845074029",
          "latitude": "38.83456",
          "longitude":"-94.663995",
          "address":"2001 Gateway Place, San Jose,CA, 95110",
          "accuracy":"1000",
          "iimestamp":"2015-04-23T12:41:16.000-0700",
          "referenceNumber":"f09fe4bac8564262b6b8bf319b664c7c"
      }
   ```  
   // validate online transaction response Response 
   ```
      {
          "distanceFromIp":"4753.222177650772",
          "distanceFromLatitudeLongitude": "1073.0103452006858",
          "distanceFromWifi":"-94.663995",
          "distanceFromAddress":null,
          "averageDistance":null,
          "referenceNumber":"f09fe4bac8564262b6b8bf319b664c7c"
      }
   ```  
##### Configuration  
1. **Endpoint :**  
**Http GET/POST :** {{default_host}}:{{configuration_port}}/crif-pan-verification  

```
{
   "output": "json",
  "optInMethod": "One",
  "serviceUrls": {
    "AddDeviceUrl": "https://api.zumigo.com/zumigo/addDevice.jsp",
    "DeleteDeviceUrl": "https://api.zumigo.com/zumigo/deleteDevice.jsp",
    "GetDeviceStatusUrl": "https://api.zumigo.com/zumigo/deviceStatus.jsp",
    "ValidateOnlineTransactionUrl": "https://api.zumigo.com/zumigo/validateOnlineTransaction.jsp",
    "LocationAndAddressUrl": "https://api.zumigo.com/zumigo/locationAndAddress.jsp"
  },
  "optInType": "Whitelist",
  "userName": "creditxchangeapi",
  "password": "cred17Exchang3Ap!"
}
```