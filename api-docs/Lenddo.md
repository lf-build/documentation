### Availalbe End Points:

1. **Lenddo Verification.**
   **Description:** Sends Lenddo Verification Request by providing entitytype, entityid and clientId  
   **HTTP Verb:** GET  
   **Route:**  "{entitytype}/{entityid}/{clientid}/verification"  
   ```
   - [HttpGet("{entitytype}/{entityid}/{clientid}/verification")]

   // Response  
   {
    "partnerScriptId": "57846ad1f7a5795ec7345d34",
    "updated": 1475738897,
    "duplicateProfiles": null,
    "verifiedByFacebook": true,
    "facebookPhotoUrl": "https://graph.facebook.com/1243437202354436/picture?type=large",
    "created": 1475738897,
    "flags": [ "DB00", "DB02", "DB05", "EM02", "EM03", "UN02", "EPH00" ],
    "verifications": {
        "name": true,
        "university": true,
        "employer": false,
        "phone": null,
        "birthday": null,
        "topEmployer": null
    },
    "applicationId": "LEDEMO45030690396790650",
    "id": "57846ad1f7a5795ec7345d34.LEDEMO45030690396790650",
    "partnerId": "57846ad1f7a5795ec7345d33",
    "probes": {
        "name": [
            "hariharan",
            "",
            "anand"
        ],
    "university": {
      "university": "Bharathiar University"
    },
    "employer": {
      "employer": "Sigma Infosolutions Ltd"
    },
    "phone": null,
    "birthday": null,
    "topEmployer": null
   }
   }   
   
   ```

2. **Lenddo Social Score.**
   **Description:** Sends Lenddo get social score Request by providing entitytype, entityid and clientId  
   **HTTP Verb:** GET  
   **Route:**  "{entitytype}/{entityid}/{clientid}/score"  

   ```
   - [HttpGet("{entitytype}/{entityid}/{clientid}/score")]  
   //Response  
   {
    "partnerScriptId": "57846ad1f7a5795ec7345d34",
    "created": 1475738896,
    "score": 823,
    "flags": null,
    "clientId": "LEDEMO45030690396790650",
    "partnerId": "57846ad1f7a5795ec7345d33"
    }   
   
   ```

### Schema:

```
// GetApplicationVerificationResponse
{
    "partnerScriptId": "57846ad1f7a5795ec7345d34",
    "updated": 1475738897,
    "duplicateProfiles": null,
    "verifiedByFacebook": true,
    "facebookPhotoUrl": "https://graph.facebook.com/1243437202354436/picture?type=large",
    "created": 1475738897,
    "flags": [ "DB00", "DB02", "DB05", "EM02", "EM03", "UN02", "EPH00" ],
    "verifications": {
        "name": true,
        "university": true,
        "employer": false,
        "phone": null,
        "birthday": null,
        "topEmployer": null
    },
    "applicationId": "LEDEMO45030690396790650",
    "id": "57846ad1f7a5795ec7345d34.LEDEMO45030690396790650",
    "partnerId": "57846ad1f7a5795ec7345d33",
    "probes": {
        "name": [
            "hariharan",
            "",
            "anand"
        ],
    "university": {
      "university": "Bharathiar University"
    },
    "employer": {
      "employer": "Sigma Infosolutions Ltd"
    },
    "phone": null,
    "birthday": null,
    "topEmployer": null
  }
}
```

```
// GetApplicationScoreResponse
{
    "partnerScriptId": "57846ad1f7a5795ec7345d34",
    "created": 1475738896,
    "score": 823,
    "flags": null,
    "clientId": "LEDEMO45030690396790650",
    "partnerId": "57846ad1f7a5795ec7345d33"
}
```

### Configurations:

1. Configurations for {{default_host}}:{{configuration_port}}/lenddo
   ```
    {
        "apiId":"57847605f7a5795ec7345d38",
        "apiSecret" :"mi0ihoYnX4DI7E1qP7+lUWylmrzpM1E2ClR9xCDr0YWpb3jY9Mw9SZU1nxizsPPhG/eXqkQGj24PYMoNPrwMgg==",
        "apiBaseUrl" : "https://scoreservice.lenddo.com/",
        "clientVerificationUrl":"ClientVerification",
        "clientScoreUrl":"ClientScore",
        "partnerScriptId":"57846ad1f7a5795ec7345d34"
    }   
   ```