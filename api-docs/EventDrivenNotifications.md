##### Availalbe End Points:
None   - Works based on queuing mechanism

### Schema 
// No separate schema to mention

```
### **Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/event-driven-notifications
```
{
    "EmailEvents": 
      [
        {
          "EventName": "ApplicationExpired",          
          "EntityId": "{Data.ExpiredApplication.ApplicationNumber}",
          "EmailTemplate": {
            "TemplateName": "LeadExpiryNotification",
            "TemplateVersion": "1.0",
            "DataExtractionRule": {
              "Name": "GetLeadExpiryNotificationData",
              "Version" :  "1.0"
            }
          }          
        },
        {
          "EventName": "InActivityThresholdCrossed",          
          "EntityId": "{Data.InActiveApplication.ApplicationNumber}",
          "EmailTemplate": {
            "TemplateName": "ApplicationInActiveNotification",
            "TemplateVersion": "1.0",
            "DataExtractionRule": {
              "Name": "GetApplicationInActiveNotificationData",
              "Version": "1.0"
            }
          }
        },
        {
          "EventName": "FinalOfferSelected",
          "EntityId": "{Data.EntityId}",
          "EmailTemplate": {
            "TemplateName": "FinalOfferAcceptedNotification",
            "TemplateVersion": "1.0",
            "DataExtractionRule": {
              "Name": "GetFinalOfferAcceptedNotificationData",
              "Version": "1.0"
            }
          }
        },
        {
          "EventName": "ApplicationRejected",          
          "EntityId": "{Data.EntityId}",         
          "EmailTemplate": {
            "TemplateName": "ApplicationRejectionNotification",
            "TemplateVersion": "1.0",
            "DataExtractionRule": {
              "Name": "GetApplicationRejectionNotificationData",
              "Version": "1.0"
            }
          }
        },
        {
        "EventName": "NotifyPendingDocuments",
        "EntityId": "{Data.PendingDocApplication.ApplicationNumber}",
        "EmailTemplate": {
          "TemplateName": "PendingDocumentsNotification",
          "TemplateVersion": "1.0",
          "DataExtractionRule": {
            "Name": "GetPendingDocumentsNotificationData",
            "Version": "1.0"
          }
        }
      },
        {
        "EventName": "FinalOfferProcessed",
        "EntityId": "{Data.EntityId}",
        "EmailTemplate": {
          "TemplateName": "FinalOfferAcceptanceNotification",
          "TemplateVersion": "1.0",
          "DataExtractionRule": {
            "Name": "GetFinalOfferAcceptanceNotificationData",
            "Version": "1.0"
          }
        }
      }
      ]    
  }```
