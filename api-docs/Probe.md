##### Availalbe End Points:

1. **GET Financials Details **  
   **Description:** Returns the balance sheet and P&L for the given company CIN by providing entitytype, entityid and CIN  
   **HTTP Verb:** GET  
   **Route:**  "{entityType}/{entityId}/{cin}/finance"  
   ```
   - Request: POST("{entityType}/{entityId}/{cin}/finance")    
   

   - Response:-// Probe financials details Response Object
   {
        "financialDetails": [
    {
      "year": "2015-03-31",
      "nature": "STANDALONE",
      "statedOn": "2015-03-31",
      "capitalWip": 863805,
      "cashAndBankBalances": 188688813,
      "investments": 1704543,
      "inventories": 1432185910,
      "netFixedAssets": 533516255,
      "otheAssets": 145076149,
      "totalAssets": 3673251854,
      "tradeReceivables": 1371216379,
      "shareCapital": 120000000,
      "reservesAndSurplus": 556982125,
      "others": 0,
      "longTermBorrowings": 514544285,
      "otherCurrentLiabilitiesAndProvisions": 54804460,
      "otherLongTermLiabilitiesAndProvisions": 37549784,
      "tradePayables": 868815882,
      "shortTermBorrowings": 1520555318,
      "totalEquity": 676982125,
      "totalEquityAndLiabilities": 3673251854,
      "depreciation": 37945108,
      "incomeTax": 37990436,
      "interest": 190700551,
      "operatingCost": 6541531614,
      "operatingProfit": 433559954,
      "otherIncome": 26572243,
      "profitBeforeInterestAndTax": 422187089,
      "profitBeforeTax": 231486538,
      "revenue": 6975091568,
      "exceptionalItemsBeforeTax": 0,
      "minorityInterestAndProfitFromAssociatesAndJointVentures": "",
      "profitAfterTaxButBeforeExceptionalItemsAfterTax": 193496102,
      "profitBeforeTaxAndExceptionalItemsBeforeTax": 231486538,
      "profitAfterTax": 193496102,
      "pnlVersion": "1"
    },
    {
      "year": "2014-03-31",
      "nature": "STANDALONE",
      "statedOn": "2015-03-31",
      "capitalWip": 23735453,
      "cashAndBankBalances": 124797370,
      "investments": 1704543,
      "inventories": 1145809937,
      "netFixedAssets": 402492629,
      "otheAssets": 147051663,
      "totalAssets": 2490702208,
      "tradeReceivables": 645110613,
      "shareCapital": 120000000,
      "reservesAndSurplus": 363486023,
      "others": 0,
      "longTermBorrowings": 546079564,
      "otherCurrentLiabilitiesAndProvisions": 56295018,
      "otherLongTermLiabilitiesAndProvisions": 12059347,
      "tradePayables": 282751048,
      "shortTermBorrowings": 1110031208,
      "totalEquity": 483486023,
      "totalEquityAndLiabilities": 2490702208,
      "depreciation": 18722275,
      "incomeTax": 35446782,
      "interest": 116795015,
      "operatingCost": 2645030743,
      "operatingProfit": 253301323,
      "otherIncome": 23425237,
      "profitBeforeInterestAndTax": 258004285,
      "profitBeforeTax": 141209270,
      "revenue": 2898332066,
      "exceptionalItemsBeforeTax": 0,
      "minorityInterestAndProfitFromAssociatesAndJointVentures": "",
      "profitAfterTaxButBeforeExceptionalItemsAfterTax": 105762488,
      "profitBeforeTaxAndExceptionalItemsBeforeTax": 141209270,
      "profitAfterTax": 105762488,
      "pnlVersion": "1"
    },
    {
      "year": "2013-03-31",
      "nature": "STANDALONE",
      "statedOn": "2013-03-31",
      "capitalWip": 111047150,
      "cashAndBankBalances": 63138072,
      "investments": 51500,
      "inventories": 591696011,
      "netFixedAssets": 145745637,
      "otheAssets": 120901536,
      "totalAssets": 1406488792,
      "tradeReceivables": 373908886,
      "shareCapital": 77169700,
      "reservesAndSurplus": 198250306,
      "others": 14300000,
      "longTermBorrowings": 55082736,
      "otherCurrentLiabilitiesAndProvisions": 101771610,
      "otherLongTermLiabilitiesAndProvisions": 97452538,
      "tradePayables": 63926763,
      "shortTermBorrowings": 798535139,
      "totalEquity": 289720006,
      "totalEquityAndLiabilities": 1406488792,
      "depreciation": 13481000,
      "incomeTax": 3585000,
      "interest": 107053000,
      "operatingCost": 2257164000,
      "operatingProfit": 134566000,
      "otherIncome": 33154000,
      "profitBeforeInterestAndTax": 154239000,
      "profitBeforeTax": 47186000,
      "revenue": 2391730000,
      "exceptionalItemsBeforeTax": 0,
      "minorityInterestAndProfitFromAssociatesAndJointVentures": "",
      "profitAfterTaxButBeforeExceptionalItemsAfterTax": 43601000,
      "profitBeforeTaxAndExceptionalItemsBeforeTax": 47186000,
      "profitAfterTax": 43601000,
      "pnlVersion": "1"
    }
    ],
      "referenceNumber": "1b5d1320b40040508eebee67c44d8fec"
    }
   ``` 
   

  
##### Schema
   // Probe get financials detail Response Object

   ```
    {
      "financialDetails": [
    {
      "year": "2015-03-31",
      "nature": "STANDALONE",
      "statedOn": "2015-03-31",
      "capitalWip": 863805,
      "cashAndBankBalances": 188688813,
      "investments": 1704543,
      "inventories": 1432185910,
      "netFixedAssets": 533516255,
      "otheAssets": 145076149,
      "totalAssets": 3673251854,
      "tradeReceivables": 1371216379,
      "shareCapital": 120000000,
      "reservesAndSurplus": 556982125,
      "others": 0,
      "longTermBorrowings": 514544285,
      "otherCurrentLiabilitiesAndProvisions": 54804460,
      "otherLongTermLiabilitiesAndProvisions": 37549784,
      "tradePayables": 868815882,
      "shortTermBorrowings": 1520555318,
      "totalEquity": 676982125,
      "totalEquityAndLiabilities": 3673251854,
      "depreciation": 37945108,
      "incomeTax": 37990436,
      "interest": 190700551,
      "operatingCost": 6541531614,
      "operatingProfit": 433559954,
      "otherIncome": 26572243,
      "profitBeforeInterestAndTax": 422187089,
      "profitBeforeTax": 231486538,
      "revenue": 6975091568,
      "exceptionalItemsBeforeTax": 0,
      "minorityInterestAndProfitFromAssociatesAndJointVentures": "",
      "profitAfterTaxButBeforeExceptionalItemsAfterTax": 193496102,
      "profitBeforeTaxAndExceptionalItemsBeforeTax": 231486538,
      "profitAfterTax": 193496102,
      "pnlVersion": "1"
    },
    {
      "year": "2014-03-31",
      "nature": "STANDALONE",
      "statedOn": "2015-03-31",
      "capitalWip": 23735453,
      "cashAndBankBalances": 124797370,
      "investments": 1704543,
      "inventories": 1145809937,
      "netFixedAssets": 402492629,
      "otheAssets": 147051663,
      "totalAssets": 2490702208,
      "tradeReceivables": 645110613,
      "shareCapital": 120000000,
      "reservesAndSurplus": 363486023,
      "others": 0,
      "longTermBorrowings": 546079564,
      "otherCurrentLiabilitiesAndProvisions": 56295018,
      "otherLongTermLiabilitiesAndProvisions": 12059347,
      "tradePayables": 282751048,
      "shortTermBorrowings": 1110031208,
      "totalEquity": 483486023,
      "totalEquityAndLiabilities": 2490702208,
      "depreciation": 18722275,
      "incomeTax": 35446782,
      "interest": 116795015,
      "operatingCost": 2645030743,
      "operatingProfit": 253301323,
      "otherIncome": 23425237,
      "profitBeforeInterestAndTax": 258004285,
      "profitBeforeTax": 141209270,
      "revenue": 2898332066,
      "exceptionalItemsBeforeTax": 0,
      "minorityInterestAndProfitFromAssociatesAndJointVentures": "",
      "profitAfterTaxButBeforeExceptionalItemsAfterTax": 105762488,
      "profitBeforeTaxAndExceptionalItemsBeforeTax": 141209270,
      "profitAfterTax": 105762488,
      "pnlVersion": "1"
    },
    {
      "year": "2013-03-31",
      "nature": "STANDALONE",
      "statedOn": "2013-03-31",
      "capitalWip": 111047150,
      "cashAndBankBalances": 63138072,
      "investments": 51500,
      "inventories": 591696011,
      "netFixedAssets": 145745637,
      "otheAssets": 120901536,
      "totalAssets": 1406488792,
      "tradeReceivables": 373908886,
      "shareCapital": 77169700,
      "reservesAndSurplus": 198250306,
      "others": 14300000,
      "longTermBorrowings": 55082736,
      "otherCurrentLiabilitiesAndProvisions": 101771610,
      "otherLongTermLiabilitiesAndProvisions": 97452538,
      "tradePayables": 63926763,
      "shortTermBorrowings": 798535139,
      "totalEquity": 289720006,
      "totalEquityAndLiabilities": 1406488792,
      "depreciation": 13481000,
      "incomeTax": 3585000,
      "interest": 107053000,
      "operatingCost": 2257164000,
      "operatingProfit": 134566000,
      "otherIncome": 33154000,
      "profitBeforeInterestAndTax": 154239000,
      "profitBeforeTax": 47186000,
      "revenue": 2391730000,
      "exceptionalItemsBeforeTax": 0,
      "minorityInterestAndProfitFromAssociatesAndJointVentures": "",
      "profitAfterTaxButBeforeExceptionalItemsAfterTax": 43601000,
      "profitBeforeTaxAndExceptionalItemsBeforeTax": 47186000,
      "profitAfterTax": 43601000,
      "pnlVersion": "1"
     }
    ],
      "referenceNumber": "1b5d1320b40040508eebee67c44d8fec"
     }

   ```  

##### Configuration  
1. **Endpoint :**  
**Http GET/POST :** {{default_host}}:{{configuration_port}}/probe  

```

   {
    "offset": "0",
    "apiVersion": "1.1",
    "limit": "25",
    "apiKey": "ULexyqL8Ke6yBEj2rcTwO2VIlOP6oiZT3k5AtaYc",
    "url": "https://api.probe42.in/probe_lite/"
   }

```