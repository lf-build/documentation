** Image Name : 192.168.1.61:8118/ce_application-document Ports:5204 **

##### Availalbe End Points:

1. Add new document with ApplicationId-Category-StipulationType

   ```
   - POST /{applicationId}/{category}/{stipulationType}
   - Body (FileUplod)
   ```
2. Add new document with ApplicationId-ApplicantId-Category-StipulationType

   ```
   - POST (/{applicationId}/{applicantId}/{category}/{stipulationType})
   - Body (FileUplod)
   ```
3. Get ApplicationDocument List By ApplicantId

   ```
   - GET (/applicant/{applicantId})
   ```
   
4. Get ApplicationDocument List By ApplicationId

   ```
   - GET (/application/{applicationId})
   ```
5. Get ApplicationDocument List By ApplicationId and ApplicantId

   ```
    - GET (/{applicationId}/{applicantId})
   ```
6. Approve ApplicationDocument by ApplicationDocumentId

   ```
   - PUT (/{applicationDocumentId}/Approve)
   ```
7. Reject ApplicationDocument by ApplicationDocumentId

   ```
   - PUT (/{applicationDocumentId}/Reject)
   - Body (RejectedReason)
   ```

##### Look Up Entries :

1. Category 
```

{{default_host}}:{{lookupservice_port}}/applicationDocument-category
{
   "1" : "DriverLicense",
   "2" :"ElectricityBill", 
   "3" :"LandlineBill", 
   "4":"MobileBill", 
   "5":"InternetBill", 
   "6":"CableBill",
   "7":"UIDDocument", 
   "8":"PayStub", 
   "9":"Photo", 
   "10":"Other" 
   
}
```   
2. StipulationType
```   
{{default_host}}:{{lookupservice_port}}/applicationDocument-stipulationType
{
    "1":  "None",
    "2":  "Income",
    "3":  "Bank",
    "4":  "ID",
    "5":  "Email",
    "6":  "Employment"
   
}
```

#**Configurations**

1. Configurations for {{configurationservice}}/document-manager
```
{
	"storageEngine": "AmazonS3",
	"storageSettings": 
	{
		 "RegionName":"us-west-1",
		 "BucketName":"lf-document-development",
		 "DataStoreName":"MyDataStore.json",
		 "AccessToken":"AKIAJKBUAJNQPSV2CZYQ",
		 "SecretToken":"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"
	}
}
```
2. Configuration for the {{configurationservice}}/tenant
```
{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
```