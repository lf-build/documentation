** Image Name : registry.lendfoundry.com/ce-consent Ports:5208**

#####Availalbe End Points:

1. Sign Document 
   ```
   - POST (/{entityType}/{entityId}/{consentName}/sign)
   - Body {"IPAddress":"10.1.1.99" , "SignedBy": "1"}  
   ```
2. GetSignedConsentDocument
   ```
   - GET ({entityType}/{entityId})   
   ```
3. View Document
    ```
   - POST ({entityType}/{entityId}/{consentName}/view)
    - Body {"IPAddress":"10.1.1.99" , "SignedBy": "1"}  
   ```
 
#**Configurations**

1. Configuration for the {{configurationservice}}/tenant

```
{
  "isActive": "True",
  "email": "ce-dev-tenant@gmail.com",
  "name": "ce-dev",
  "timezone": "Asia/Calcutta",
  "phone": "anything",
  "paymentTimeLimit": "19",
  "website": "www.creditexchange.com"
}
```
2. Configurations for {{configurationservice}}/documentmanager
```
{
  "storageEngine": "AmazonS3",
  "storageSettings": {
    "RegionName": "us-west-1",
    "BucketName": "lf-document-development",
    "DataStoreName": "MyDataStore.json",
    "AccessToken": "AKIAJKBUAJNQPSV2CZYQ",
    "SecretToken": "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"
  }
}
```
3. Configurations for {{configurationservice}}/application-documents
```
{
  "entities": [
    {
      "EntityName": "Application",
      "ApplicationDocument": [
        {
          "groupName": "consentandagreement",
          "category": [
            "applicationconsent",
            "loanapplication",
            "loanagreement"
          ]
        },
        {
          "groupName": "thirdparty",
          "category": [
            "crifcreditreport",
            "perfioscashflowreport"
          ]
        },
        {
          "groupName": "borrowerdocument",
          "category": [
            "pancard",
            "aadhaarcard",
            "form26",
            "bankstatement"
          ]
        },
        {
          "groupName": "signeddocument",
          "category": [
            "signeddocument"
          ]
        }
      ]
    }
  ]
}
```
4. Configurations for {{configurationservice}}/application-consent
```
{
  "entities": [
      {
        "EntityName": "Application",
        "Consents": [
          {
            "name": "CreditPullConsent",
            "templateName": "CreditPullConsent",
            "templateVersion": "1.0",
            "document": {
              "name": "CreditPullConsent.pdf",
              "category": "applicationsubmitconsent",
              "tags": [
                "consent"
              ]
            }
          },
          {
            "name": "BankAccInfoConsent",
            "templateName": "BankAccInfoConsent",
            "templateVersion": "1.0",
            "document": {
              "name": "BankAccInfoConsent.pdf",
              "category": "applicationsubmitconsent",
              "tags": [
                "consent"
              ]
            }
          },
          {
            "name": "EmploymentVerificationConsent",
            "templateName": "EmploymentVerificationConsent",
            "templateVersion": "1.0",
            "document": {
              "name": "EmploymentVerificationConsent.pdf",
              "category": "applicationsubmitconsent",
              "tags": [
                "consent"
              ]
            }
          },
          {
            "name": "eKYCVerificationConsent",
            "templateName": "eKYCVerificationConsent",
            "templateVersion": "1.0",
            "document": {
              "name": "eKYCVerificationConsent.pdf",
              "category": "applicationsubmitconsent",
              "tags": [
                "consent"
              ]
            }
          },
          {
            "name": "LocationVerificationConsent",
            "templateName": "LocationVerificationConsent",
            "templateVersion": "1.0",
            "document": {
              "name": "LocationVerificationConsent.pdf",
              "category": "applicationsubmitconsent",
              "tags": [
                "consent"
              ]
            }
          },
          {
            "name": "LoanAgreement",
            "templateName": "LoanAgreement",
            "templateVersion": "1.0",
            "document": {
              "name": "LoanAgreement.pdf",
              "category": "loanagreement",
              "tags": [
                "consent"
              ]
            }
          },
          {
            "name": "LoanApplicationForm",
            "templateName": "LoanApplicationForm",
            "templateVersion": "1.0",
            "document": {
              "name": "LoanApplicationForm.pdf",
              "category": "loanapplication",
              "tags": [
                "consent"
              ]
            }
          }
        ]
      }
    ]
}
```