##### Availalbe End Points:
	
1. **Initiate Email Verification**
   **Description** : Initiate Email verification by sending across link with verification code using a concrete provider implementation
   **HTTP Verb**: POST
   **Route** : "{entityType}/{entityId}"
   ```
   - Request: POST("{entityType}/{entityId}")    
   - Request Schema:  {
							"Source" : "borrower",
							"Name" : "J J",
							"Email" : "jigs.jani@xyz.com",
							"Data" : {}
				    }    
   - Response:-// InitiateEmailVerificationResponse object
   - Response Schema:  { "ReferenceNumber" : "9ee33a8e-9b4a-11e6-a40f-00163ef91450" } 
   
2. **Verify Email**
   **Description** : Verified Email by verifiying verification code passed to the end point
   **HTTP Verb**: POST
   **Route** : "verification/{verificationCode}"
   ```
   - Request: POST("verification/{verificationCode}")
  
   - Response :- // EmailVerificationResponse			
   - Response Schema:  {
                                         "EntityId" : "0000345",
                                         "EntityType" : "application",
                                         "Email" : "jigs.jani@xyz.com"   
                                     }
   
3. **Get Verification Status**
   **Description** : To get verification status details for ongoing/completed email verification
   **HTTP Verb**: GET
   **Route** : "verificationstatus/{emailid}"
   ```
   - Request: GET("verificationstatus/jigs.jani@xyz.com")
  
   - Response :- // IEmailVerifications			
   - Response Schema:  {
                                         "EntityId" : "0000345",
                                         "EntityType" : "application",
                                         "Email" : "jigs.jani@xyz.com" ,
                                         "Code" : "9ee33b7e-9b4a-11e6-a40f-00163ef91450",
                                         "CodeCreationTime": "2017-02-20​T20:07:31.215Z",
                                         "CodeVerifiedOn": "2017-02-21T20:07:31.215Z",
                                         "VerificationStatus": "Verified"
                                     }

   
### Schema 
// No separate schema to mention

```
### **Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/email-verification
```
{
    "verificationexpirywindow": "1",
  "emailVerificationSettings": {
    "borrower": {
      "templatename": "EmailVerification",
      "version": "1.0",
      "format": "Html"
    },
    "merchant": {
      "templatename": "EmailVerification",
      "version": "1.0",
      "format": "Html"
    }
  }
}
```
