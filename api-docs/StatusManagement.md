##### Availalbe End Points:
	
1. Used to get the status of Entity

   ```
   - Get("{entityType}/{entityId}") 
			- Response: (Statusobject)
   ``` 
2. Used to set the status of Entity

   ```
   - Post("/{entityType}/{entityId}/{newStatus}/{updatedby}/{*reason}")
			- Response: (NoContentResult)
   ```
3. Used to get Checklist for given status transition
...
- Get("/{entityType}/{entityId}/{status?}/checklist")
                      -Response: IEnumerable<IChecklist>
...
4. Used to get the Transitions of Entity

   ```
   - Get("/{entityType}/{status}/transitions")
			- Response: List<Transition>
   ```
5. Used to get the Reasons of Entity

   ```
   - Get("/{entityType}/{status}/reasons")
			- Response: List<Reason>
   ``` 
6. Used to get the Activities of Entity

   ```
   - Get("/{entityType}/{status}/activities")
			- Response: List<Activity>
   ```   
7. Used to get Status configuration details for given status
...
- Get("/{entityType}/{status}/detail")
                       - Response: (Statusobject)
...
8. Used to get status transition history for given entity
...
- Get("getstatushisotry/{entitytype}/{entityid}")
                        - Response: IEnumerable<IEntityStatus>
...

### Schema

```
// EntityStatus
{
    "EntityType":"application",
    "EntityId":"",
    "Status":"",
    "Reason":"",
    "UpdatedOn":"",
    "UpdatedBy":""
}

// Status
{
     "Activities": "",
	 "Reasons" : "",
	 "Code" : "",
	 "Label" : "",
	 "Name" : "",
	 "Style" : "",
	 "Transitions" : "",
	 "UpdatedBy" : "",
	 "UpdatedOn" : ""
}

// Transition
{
	"Code" : "",
	"Name" : ""
}

// Reason
{
	"Code" : "" 
	"Descriptions" : "" 
}

// Activity
{
   "Code" : "",
   "Title" : ""
}
```

#**Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
	{
		"paymentTimeLimit": "15",
		"isActive": "True",
		"name": "my-tenant",
		"website": "www.lendfoundry.com",
		"email": "my-tenantX@lendfoundry.com",
		"phone": "anything",
		"timezone": "America/Detroit"
	}
}
```
2. Configurations for {{default_host}}:5001/status-management
```
{
  "entityTypes": {
    "application": {
      "checklist": [
        {
          "code": "bank-verification",
          "title": "Bank verification completed",
          "ruleName": "ruleForBankVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "employment-everification",
          "title": "Employment verification completed",
          "ruleName": "ruleForEmploymentVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "kyc-verification",
          "title": "KYC verification completed",
          "ruleName": "ruleForKYCVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "mobile-verification",
          "title": "Mobile verification completed",
          "ruleName": "ruleForMobileVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "personal-email-verification",
          "title": "Personal email verification completed",
          "ruleName": "ruleForPersonalEmailVerificationForChecklist",
          "ruleVersion": "1.0"
        },
        {
          "code": "residence-everification",
          "title": "residence verification completed",
          "ruleName": "ruleForResidenceVerificationForChecklist",
          "ruleVersion": "1.0"
        }
      ],
      "statuses": [
        {
          "code": "200.01",
          "name": "Lead Created",
          "label": "Lead",
          "style": "Lead",
          "transitions": [
            "200.02",
            "200.15",
            "200.16",
			"200.14"
          ],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.02",
          "name": "Application Submitted",
          "label": "Application",
          "style": "Application",
          "checklist": [
            "mobile-verification"
          ],
          "transitions": [
            "200.03",
            "200.15",
            "200.16",
			"200.14"
          ],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.03",
          "name": "Initial Offer Given",
          "label": "Offer Given",
          "style": "Offer Given",
          "transitions": [
            "200.04",
            "200.14",
            "200.15"
          ],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.04",
          "name": "Initial Offer Selected",
          "label": "Offer Selected",
          "style": "Offer Selected",
          "transitions": [
            "200.05",
            "200.06",
            "200.14",
            "200.15",
            "200.16"
          ],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.05",
          "name": "Credit Review",
          "label": "Manual UW",
          "style": "Manual UW",
          "transitions": [
            "200.06",
            "200.15",
            "200.16",
			"200.14"
          ],
          "reasons": {
            "CompUnclass": "Company Unclassified",
            "OtherPurpose": "Other Loan Purpose",
            "HigherMaxCredit": "Monthly Income Max-Credit MisMatch",
            "ThinLowScore": "Thin File Low Score",
            "NTCLowScore": "NTC File Low Score",
            "ThickLowScore": "Thick File Low App and Bureau Score",
            "ThickAppBureauMisMatch": "Thick File  App and Bureau Score MisMatch",
            "AddressUnVerified": "Bureau Address Verification Failed",
            "HighIncome": "Higher Monthly Income",
            "PerfiosNameNotExist": "Perfios Name Not Exist",
            "PerfiosNameNotMatch": "Perfios Name not Matched with application"
          },
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.06",
          "name": "Final offer made",
          "label": "Offer Ready",
          "style": "Offer Ready",
          "transitions": [
            "200.07",
            "200.14",
            "200.15",
            "200.16"
          ],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.07",
          "name": "Final offer Accepted",
          "label": "Agreement/KYC",
          "style": "Agreement/KYC",
          "transitions": [
            "200.08",
            "200.14",
            "200.15",
            "200.16"
          ],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.08",
          "name": "CE Approved",
          "label": "Approved",
          "style": "Approved",
          "checklist": [
            "employment-verification",
            "residence-verification",
            "bank-verification",
            "kyc-verification"
          ],
          "transitions": [
            "200.09",
            "200.16"
          ],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.09",
          "name": "Bank Fraud Review",
          "label": "RBL Fraud Review",
          "style": "RBL Fraud Review",
          "transitions": [
            "200.10",
            "200.16"
          ],
          "activities": {
            "Need followup": "Need followup"
          }
        },
        {
          "code": "200.10",
          "name": "Bank Credit Review",
          "label": "RBL Credit Review",
          "style": "RBL Credit Review",
          "transitions": [
            "200.11",
            "200.16"
          ],
          "activities": {
            "Need followup": "Need followup"
          }
        },
        {
          "code": "200.11",
          "name": "Approved",
          "label": "Approved",
          "style": "Approved ",
          "transitions": [
            "200.12"
          ],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.12",
          "name": "Funded",
          "label": "Funded",
          "style": "Funded",
          "transitions": [],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.14",
          "name": "Not Interested",
          "label": "Not Interested",
          "style": "Not Interested ",
          "transitions": [],
          "reasons": {
			"lesseramount":"This amount is less than my loan application amount",
			"otherlender":"I'm taking a loan from some other lender",
			"tooslow":"Application is taking too long a time",
			"lessoffer":"This amount is less than my loan application amount",
			"highinterest":"The interest rate is high",
			"longduration":"The duration is too long",
			"unexpectedemi":"I'm not happy with the EMI amount",
			"unexpectedprocessingfee":"I'm not happy with the processing fee",
			"unhappyoverall":"I'm not happy with the application experience",			
			"other":"Other"
          },
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.15",
          "name": "Expired",
          "label": "Expired",
          "style": "Expired",
          "transitions": [],
          "reasons": {
            "r9": "Auto expiry"
          },
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          }
        },
        {
          "code": "200.16",
          "name": "Rejected",
          "label": "Declined",
          "style": "Declined",
          "transitions": [],
          "activities": {
            "Contacted but No Answer": "Contacted but No Answer",
            "Number Not Reachable": "Number Not Reachable",
            "Notes": "Notes",
            "Call from Borrower": "Call from Borrower",
            "Borrower Followup": "Borrower Followup"
          },
          "IsRejectionStatus": "true",
          "reasons": {
           "BureauScoreLow": "Low Bureau Score",
			      "AgeMismatch":"Age MisMatch",
            "HighInquiries": "Too Many Inquiries",
            "BureauLoanDerog": "Derogatory Loan",
            "CCBalToIncomeHigh": "High Credit Card Balance",
            "HighDPDTrade": "Higher Number of Deliquencies",
            "IncomeLowL3": "Low Income",
            "IncomeLowL2": "Low Income",
            "ReportedEMILow": "EMI Inconsistent",
            "RBLLiveLoan": "RBL Live Loan",
            "RBLDefaultLoan": "RBL Default Loan",
            "RBLInquiryRule": "RBL Inquiry in File",
            "RBLDPDRule1": "RBL DPD",
            "RBLOverdueRule": "RBL Overdue",
            "RBLDPDRule2": "RBL DPD",
            "RBLDPDRule3": "RBL DPD",
            "LoanPurposeCheck": "Loan Purpose not Supported",
            "CitySupported2": "Unsupported Geo Region",
			"CitySupported1" : "Unsupported Geo Region",
            "EmpNotExist": "Employer Not in Database",
            "IncomeMismatch": "Income Mismatch",
            "LoanAmtMismatch": "LoanAmount Mismatch with Credit File",
            "EmpTypeNotSupported": "Employment Type Not Supported",
            "InvalidPANNumber": "Invalid PAN Number",
            "HighDTILowIncomeL1": "Higher Debt Compared to Income",
            "CCBalanceRatioHigh": "High Credit Card Balance",
            "HighRiskZipRule1": "High Risk PIN Code",
            "HighRiskZipRule2": "High Risk PIN Code",
            "HighRiskZipRule3": "High Risk PIN Code",
            "HighRiskZipRule4": "High Risk PIN Code",
            "PANNotVerfied": "Unable to Verify PAN",
            "LOWELIGIBILITY": "LOWELIGIBILITY",
            "r10": "CE decline",
            "r11": "Bank decline"
          }
        }
      ]
    }
  }
}
```