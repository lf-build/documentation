##### Availalbe End Points:

1. **Domain Search**  
   **Description:**search all the email addresses corresponding to one website Domain by providing entitytype, entityid and domain 
   **HTTP Verb:** GET  
   **Route:**  "search-domain/{entitytype}/{entityid}/{domain}"  
   ```
   - Request: GET("search-domain/{entitytype}/{entityid}/{domain}")    
 
   - Response:-// domain search Response Object
   {
       "domain": "stripe.com"
       "results": 165,
       "webmail": false,
       "pattern": "{first}"
       "offset": 0,
        "emails": [
            {
                "value": "hoon@stripe.com",
                "type": "personal",
                "confidence": 97,
                "sources":[
                    {
                       "domain": "stripe.com",
                       "uri": "https://stripe.com/blog/weekly-and-monthly-transfers",
                       "extracted_on": "2015-03-05" 
                    }
                ]
            }
        ],
       
       "referenceNumber": "f09fe4bac8564262b6b8bf319b664c7c"
    }
   ``` 
2. **Email Finder**  
   **Description:** endpoint generates the most likely email address from a domain name, a first name and a last name by providing entitytype, entityid and Request object 
   **HTTP Verb:** POST  
   **Route:**  "search-email"  
   ```
   - Request: POST("search-email")    
   {
       "domain":"asana.com",
       "company":"Asana",
       "firstName":"Dustin",
       "lastName":"Moskovitz"
   }
   - Response:-// search-email Response Object
   {
       "email": "dustin@asana.com",
       "score": 72,
       "sources":[
                    {
                       "domain": "stripe.com",
                       "uri": "https://stripe.com/blog/weekly-and-monthly-transfers",
                       "extracted_on": "2015-03-05" 
                    }
                ]
           
    }
   ``` 
3. **Email Verification**  
   **Description:** endpoint allows you to verify the deliverability of an email address by providing entitytype and email.
   **HTTP Verb:** GET  
   **Route:**  "verify-email/{entitytype}/{email}"  
   ```
   - Request: GET("verify-email/{entitytype}/{email}")    

   - Response:-// Email Verification Response Object
   {
       "email": "steli@close.io",
       "result": "deliverable",
       "score": 91,
       "regexp": true,
       "gibberish": false,
       "disposable": false,
       "webmail": false,
       "mxRecords": true,
       "smtpServer": true,
       "smtpCheck": true,
       "sources": [
         {
           "domain": "blog.close.io",
           "uri": "http://blog.close.io/how-to-become-great-at-sales",
           "extracted_on": "2015-01-26"
         }, 
         {
           "domain": "blog.close.io",
           "uri": "http://blog.close.io/how-to-do-referral-sales",
           "extracted_on": "2015-01-26"
         }
                ]
           
    }
   ``` 
3. **Email Count**  
   **Description:** endpoint allows you to know how many email addresses we have for one domain by providing entitytype,entityid and domain.
   **HTTP Verb:** GET  
   **Route:**  "EmailCount/{entitytype}/{entityid}/{domain}"  
   ```
   - Request: GET("EmailCount/{entitytype}/{entityid}/{domain}")    

   - Response:-// Email Count Response Object
    {
      "count": 81
           
    }
   ```
  
##### Schema
   // domain search Response Object

   ```
    {
       "domain": "stripe.com"
       "results": 165,
       "webmail": false,
       "pattern": "{first}"
       "offset": 0,
        "emails": [
            {
                "value": "hoon@stripe.com",
                "type": "personal",
                "confidence": 97,
                "sources":[
                    {
                       "domain": "stripe.com",
                       "uri": "https://stripe.com/blog/weekly-and-monthly-transfers",
                       "extracted_on": "2015-03-05" 
                    }
                ]
            }
        ],
       
       "referenceNumber": "f09fe4bac8564262b6b8bf319b664c7c"
     }

   ```  
// search-email Response Object
   ```
    {
       "email": "dustin@asana.com",
       "score": 72,
       "sources":[
                    {
                       "domain": "stripe.com",
                       "uri": "https://stripe.com/blog/weekly-and-monthly-transfers",
                       "extracted_on": "2015-03-05" 
                    }
                ]
           
    }
   ``` 
   // Email Verification Response Object
   ```
    {
       "email": "steli@close.io",
       "result": "deliverable",
       "score": 91,
       "regexp": true,
       "gibberish": false,
       "disposable": false,
       "webmail": false,
       "mxRecords": true,
       "smtpServer": true,
       "smtpCheck": true,
       "sources": [
         {
           "domain": "blog.close.io",
           "uri": "http://blog.close.io/how-to-become-great-at-sales",
           "extracted_on": "2015-01-26"
         }, 
         {
           "domain": "blog.close.io",
           "uri": "http://blog.close.io/how-to-do-referral-sales",
           "extracted_on": "2015-01-26"
         }
                ]
           
    }
   ```
   Email Count Response Object
   ```
   {
       "count": 81
   }
   ```
##### Configuration  
1. **Endpoint :**  
**Http GET/POST :** {{default_host}}:{{configuration_port}}/email-hunter

```
{
     "apiKey": "a787c4d7ceb14a723e162ed6fabec80ee2f8db9b",
  "apiVersion": "v1",
  "apiUrl": "https://api.emailhunter.co/"
}
```