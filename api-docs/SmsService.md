##### Availalbe End Points:
	
1. **Send Sms using template to a recipient**
   
    **Description** : Sends Sms to provided number after getting template populated with input data in json format
   **HTTP Verb**: POST
   **Route** : "{entitytype}/{entityid}/{templateName}/{templateVersion}"
   ```
   - Request: POST("{entitytype}/{entityid}/{templateName}/{templateVersion}")    
   - Request Schema:  {
                                        	"Name" : "J J",
                                        	"RecipientPhone": "+919008444844",
                                        	"PlaceHolderValue": "if you read this text then it is working."                                    
				    }    

   - Response:-// SmsResult object
   - Response Schema:  { "SmsReferenceNumber" : "9ee33a8e-9b4a-11e6-a40f-00163ef91450" } 
   
2. **Send free text Sms without using a template to a recipient**

   **Description** : Sends Sms to provided number with free text as sent to this end point
   **HTTP Verb**: POST
   **Route** : "{entitytype}/{entityid}"
   ```
   - Request: POST("{entitytype}/{entityid}")
   - Request Schema:  {
                                        	"Name" : "J J",
	                                     "RecipientPhone": "+919008444844",
	                                     "Message": "This is to test free text transactional message."
				    }  
   - Response :- // SmsResult object			
   - Response Schema:  { "SmsReferenceNumber" : "9ee33a8e-9b4a-11e6-a40f-00163ef91450" }   
   
### Schema 
// No separate schema to mention

```
### **Configurations**

1. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/mobile-verification
```
{
  "smsProviderSettings": {
    "Two2Factor": {
      "BaseUrl": "http://2factor.in/API/V1",
      "ApiKey": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
      "OtpEndPoint": "SMS/{phone_number}/AUTOGEN/{template_name}",
      "OtpCheckBalanceEndPoint": "BAL/SMS",
      "VerifyOtpEndPoint": "SMS/VERIFY/{session_id}/{otp_input}",
      "TrSmsEndPoint": "ADDON_SERVICES/SEND/TSMS",
      "TrSmsCheckBalanceEndPoint": "ADDON_SERVICES/BAL/TRANSACTIONAL_SMS",
      "TrSmsDeliveryReportEndPoint": "ADDON_SERVICES/RPT/TSMS/{session_id}",
      "SenderName": "QBERAA"
    }
  },
  "smsProvider": "Two2Factor"
}
```
