
##### Available End Points:

1. Get all Loan Applications for a tenant.

   ```
   - HttpGet("/all")
   - Body ()
			- Response: IEnumerable<IFilterView>
   ``` 

2. Export Loan Applications for a tenant to Csv with configured template

   ```
   - HttpGet("/all")
   - Body (templatename)
			- Response: FileResult
   ``` 

3. Get all Loan Applications based on Source Type & Source Id.

   ```
   - HttpGet("/{sourceType}/{sourceId}/all")
   - Body (sourceType,sourceId)
			- Response: IEnumerable<IFilterView>
   ```
4. Get count of submitted Loan Application for current month.

   ```
   - [HttpGet("/{sourceType}/{sourceId}/metrics/submit/month")]
   - Body (sourceType,sourceId)
			- Response: Task<int>
   ```
5. Get count of submitted Loan Application for current year.

   ```
   - [HttpGet("/{sourceType}/{sourceId}/metrics/submit/year")]
   - Body (sourceType,sourceId)
			- Response: Task<int>
   ```
6. Get Loan Application by Loan Application Status

   ```
   - [HttpGet("/{sourceType}/{sourceId}/status/{*statuses}")]
   - Body (sourceType,sourceId,statuses)
			- Response: Task<IEnumerable<IFilterView>>
   ```
7. Get Loan Application by Loan Application Status

   ```
   - [HttpGet("status/{*statuses}")]
   - Body (statuses)
			- Response: Task<IEnumerable<IFilterView>>
   ```
8. Export Loan Application by Loan Application Status

   ```
   - [HttpGet("export/{templatename}/status/{*statuses}")]
   - Body (templatename,statuses)
			- Response: FileResult
   ```

9. Get Loan Application count by Loan Application Status

   ```
   - [HttpGet("/{sourceType}/{sourceId}/count/status/{*statuses}")]
   - Body (sourceType,sourceId,statuses)
			- Response: Task<int>
   ```
10. Get Loan Application by Tag

   ```
   - [HttpGet("tag/{*tags}")]
   - Body (tags)
			- Response: Task<IEnumerable<IFilterView>>
   ```
11. Export Loan Application count by Tag

   ```
   - [HttpGet("export/{templatename}/tag/{*tag}")]
   - Body (templatename ,tags)
			- Response: FileResult
   ```
12. Get Tag infor for applications by tag
   ```
   - [HttpGet("tag/taginfo/{tag}")]
   - Body (tag)
			- Response: Task<IEnumerable<IFilterViewTags>>
   ```
13. Get count of applications by tag

   ```
   - [HttpGet("/count/tag/{*tags}")]
   - Body (tags)
			- Response: int
   ```
14. Get Loan Application using applicant name

   ```
   - [HttpGet("/{sourceType}/{sourceId}/search/{name}")]
   - Body (sourceType,sourceId,name)
			- Response: Task<IEnumerable<IFilterView>>
   ```
15. Get total amount approved for given sourcetype
   ```
   - [HttpGet("/{sourceType}/{sourceId}/metrics/total-approved-amount")]
   - Body (sourceType, sourceId)
			- Response: double
   ```
16. Get loan application by application number
   ```
   - [HttpGet("/{sourceType}/{sourceId}/{applicationNumber}")]
   - Body (sourceType, sourceId, applicationNumber)
			- Response: Task<IFilterView>
   ```

17. Get loan application using application number along with status transition history
   ```
   - [HttpGet("getwithstatushistory/{sourceType}/{sourceId}/{applicationNumber}")]
   - Body (sourceType, sourceId, applicationNumber)
			- Response: Task<IFilterView>
   ```
18. Get duplicate loan application details for unique attributes like email, pan number and emailid
   ```
   - [HttpGet("duplicateexistsdata/{parametertype}/{parametervalue}")]
   - Body (parametertype, parametervalue)
			- Response: Task<IFilterView>
   ```
19. Get all applications for which expiry date has elapsed
   ```
   - [HttpGet("/expired/all")]
   - Body ()
			- Response: Task<IEnumerable<IFilterView>>
   ```
20. Apply tags to given loan application
   ```
   - [HttpPost("applytags/{applicationnumber}/{*tags}")]
   - Body (applicationnumber, tags)
			- Response: void
   ```
21. Remove tags to given loan application
   ```
   - [HttpPost("removetags/{applicationnumber}/{*tags}")]
   - Body (applicationnumber, tags)
			- Response: void
   ```

22. Search applications based on multiple criteria with OR operation
   ```
   - [HttpPost("searchapplications")]
   - Body (searchParams)   
 {
        FromApplicationDate , ToApplicationDate, FromExpiryDate , ToExpiryDate ,DateOfBirth, MobileNumber ,  Name, PanNumber, PersonalEmail, EmployerName, StatusCode, InTags, NotInTags }
			- Response: Task<IEnumerable<IFilterView>>
   ```
23. Search applications based attribute name - value pair
   ```
   - [HttpGet("searchforattribute/{attributename}/{attributevalue}")]
   - Body (attributename, attributevalue)   
			- Response: Tuple<applicationnumber, IFilterView, IFilterViewTags>
   ```
24. Get Tagging information for application using application number
  ...
  - [HttpGet("getapplicationtaginfo/{applicationnumber}")]
  - Body(applicationnumber)
                      - Response: IEnumerable<IFilterViewTags>
...

### Schema

```
// IFilterView
{
	"ApplicationId":"12345",
	"ApplicationNumber":"2345678",
	"Applicant":"Mr Shah",
	"AmountRequested":10000.00,
	"RequestTermType":"Monthly",
	"RequestTermValue":"22.50",
	"ApplicantFirstName":"Ravi",
	"ApplicantLastName":"Shah",
	"ApplicantMiddleName":"Manish",
	"ApplicantPhone":"7698599433",
	"ApplicantPhoneVerified":true,
	"ApplicantWorkEmail":"siddharth.p@sigmainfo.net",
	"ApplicantPersonalEmail":"pavagadhisiddharth@gmail.com",
	"ApplicantPersonalEmailVerified":true,
	"ApplicantPanNumber":"PAN12345",
	"ApplicantAadharNumber":"ADHAR1234",
	"ApplicantEmployer":"Sigma Infosolutions Ltd.",
	"ApplicantAddressLine1":"Main Road Area",
	"ApplicantAddressLine2":"Main Street",
	"ApplicantAddressLine3":"Main Area",
	"ApplicantAddressLine4":"Fourth Line",
	"ApplicantAddressCity":"Ahmedabad",
	"ApplicantAddressCountry":"India",
	"ApplicantAddressIsDefault":true,
	"ApplicantAddressIsVerified":true,
	"Submitted":6/19/2008 7:00:00 AM +00:00,
	"ExpirationDate":6/19/2008 7:00:00 AM +00:00,
	"StatusCode":"01",
	"StatusName":"Approved",
	"SourceId":"01",
	"SourceType":"Marchant",
	"InitialOfferAmount":100000.00,
	"InitialOfferInterestRate":12.5,
	"InitialOfferInstallmentAmount":1000.00,
	"FinalOfferAmount":60000,
	"FinalOfferInterestRate":13,
	"FinalOfferInstallmentAmount":2000.00,
	"Tags":[{
	"tag1",
	"tag2",
	"tag3"
	}]
}
```

#**Configurations**

1. Configurations for {{default_host}}:{{tenant_port}}/
```
{
    "Name" : "my-tenant",
    "Email" : "mytenant@gmail.com",
    "Phone" : "(999) 999-9999",
    "Website" : "www.tenant.com",
    "IsActive" : true
}
```

2. Configurations for {{default_host}}:{{configuration_port}}/tenant
```
{
    "paymentTimeLimit": "15",
    "isActive": "True",
    "name": "my-tenant",
    "website": "www.lendfoundry.com",
    "email": "my-tenantX@lendfoundry.com",
    "phone": "anything",
    "timezone": "America/Detroit"
}
```

3. Configurations for {{default_host}}:{{configuration_port}}/number-generator
```
{
  "configurations": {
      "loan": {
        "prefix": "LOAN",
        "suffix": "",
        "type": "Incremental",
        "separator": "_",
        "startsFrom": 100,
        "leftPadding": 10
      },
      "application": {
        "prefix": "LCUSA-",
        "suffix": "",
        "type": "Incremental",
        "separator": "",
        "startsFrom": 1,
        "leftPadding": 7
      },
        "applicant": {
        "prefix": "LCUSA-",
        "suffix": "",
        "type": "Incremental",
        "separator": "",
        "startsFrom": 1,
        "leftPadding": 7
      },
      "merchant": {
        "prefix": "MER",
        "suffix": "",
        "type": "Incremental",
        "separator": "",
        "startsFrom": 0,
        "leftPadding": 10
      }
    }
}
```
4. Configurations for {{default_host}}:{{configuration_port}}/security-identity
```
{
	"sessionTimeout": 10,
	"tokenTimeout": 60,
      "roles": {
        "VP Of Sales": {
          "permissions": [
            "{*any}"
          ],
          "roles": {
            "Loan Coordinator Manager": {
              "permissions": [
                "{*any}"
              ],
              "roles": {
                "Loan Coordinator": {
                  "permissions": [
                    "{*any}"
                  ]
                }
              }
            },
            "Sr. Relationship Manager": {
              "permissions": [
                "{*any}"
              ],
              "roles": {
                "Relationship Manager": {
                  "permissions": [
                    "{*any}"
                  ]
                }
              }
            }
          }
        },
        "VP Of Operations": {
          "permissions": [
            "{*any}"
          ],
          "roles": {
            "Sr. Loan Processor": {
              "permissions": [
                "{*any}"
              ],
              "roles": {
                "Loan Processor": {
                  "permissions": [
                    "{*any}"
                  ]
                }
              }
            },
            "Credit Specialist Manager": {
              "permissions": [
                "{*any}"
              ],
              "roles": {
                "Credit Specialist": {
                  "permissions": [
                    "{*any}"
                  ]
                }
              }
            },
            "Merchant Underwriter Manager": {
              "permissions": [
                "{*any}"
              ],
              "roles": {
                "Merchant Underwriter": {
                  "permissions": [
                    "{*any}"
                  ]
                }
              }
            },
            "Customer Service Manager": {
              "permissions": [
                "{*any}"
              ],
              "roles": {
                "Customer Service Representative": {
                  "permissions": [
                    "{*any}"
                  ]
                }
              }
            },
            "QA Manager": {
              "permissions": [
                "{*any}"
              ],
              "roles": {
                "QA": {
                  "permissions": [
                    "{*any}"
                  ]
                }
              }
            }
          }
        },
        "Director Of Merchant Support": {
          "permissions": [
            "{*any}"
          ],
          "roles": {
            "Merchant Support Specialist": {
              "permissions": [
                "{*any}"
              ]
            }
          }
        },
        "Marketing": {
          "permissions": [
            "{*any}"
          ],
          "roles": {
            "Social Media Coordinator": {
              "permissions": [
                "{*any}"
              ]
            }
          }
        }
      }
}
```
5. Configurations for {{default_host}}:{{configuration_port}}/application-filters
```
{
      "ReApplyStatuseCodes": [ "200.14", "200.15", "200.16", "200.17" ],
      "ReApplyTimeFrameDays": "90",
      "events": [
        {
          "Name": "ApplicationCreated",
          "ApplicationNumber": "{Data.Application.ApplicationNumber}",
		  "ShouldUpdateAppFilterRecord" : "true",
          "UpdateLastProgressDate": "true",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
        },
        {
          "Name": "ApplicationModified",
          "ApplicationNumber": "{Data.Application.ApplicationNumber}",
		  "ShouldUpdateAppFilterRecord" : "true",
          "UpdateLastProgressDate": "true",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
        },
        {
          "Name": "ApplicationStatusChanged",
          "ApplicationNumber": "{Data.EntityId}",
		  "ShouldUpdateAppFilterRecord" : "true",
          "UpdateLastProgressDate": "true",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
        },
        {
          "Name": "FactVerificationCompleted",
          "ApplicationNumber": "{Data.Fact.EntityId}",
		  "ShouldUpdateAppFilterRecord" : "true",
          "UpdateLastProgressDate": "true",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
        },
        {
          "Name": "InitialOfferSelected",
          "ApplicationNumber": "{Data.EntityId}",
		  "ShouldUpdateAppFilterRecord" : "true",
          "UpdateLastProgressDate": "true",
          "ShouldUpdateInitialOfferSelectedDate": "true",
          "ShouldUpdateFinalOfferSelectedDate": "false"
        },
        {
          "Name": "FinalOfferSelected",
          "ApplicationNumber": "{Data.EntityId}",
		  "ShouldUpdateAppFilterRecord" : "true",
          "UpdateLastProgressDate": "true",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "true"
        },
		{
		  "Name": "ManualFactVerificationInitiated",
          "ApplicationNumber": "{Data.EntityId}",
		  "ShouldUpdateAppFilterRecord" : "false",
          "UpdateLastProgressDate": "false",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
		},
		{
		  "Name": "VerificationDocumentReceived",
          "ApplicationNumber": "{Data.EntityId}",
		  "ShouldUpdateAppFilterRecord" : "false",
          "UpdateLastProgressDate": "false",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
		},
		{
		  "Name": "ApplicationActivityLogged",
          "ApplicationNumber": "{Data.ApplicationNumber}",
		  "ShouldUpdateAppFilterRecord" : "false",
          "UpdateLastProgressDate": "false",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
		},
		{
		  "Name": "FundingDataUpdated",
          "ApplicationNumber": "{Data.EntityId}",
		  "ShouldUpdateAppFilterRecord" : "true",
          "UpdateLastProgressDate": "false",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
		},
		{
		  "Name": "ApplicationRejected",
		  "ApplicationNumber" : "{Data.EntityId}",
		  "ShouldUpdateAppFilterRecord" : "false",
          "UpdateLastProgressDate": "false",
          "ShouldUpdateInitialOfferSelectedDate": "false",
          "ShouldUpdateFinalOfferSelectedDate": "false"
		}
      ],
	  "ExportTemplates": [
        {
          "TemplateName": "defaultTemplate",
          "FileNamePrefix": "FilterExport_",
          "Delimeter": "|",
          "CsvProjection": {
            "ApplicationNumber": "Application Number",            
			"Applicant" : "Applicant Name",
			"ApplicantWorkEmail" : "Applicant work Email"
          }
        }
      ]
}```
6. {{default_host}}:{{configuration_port}}/application
```
{
    "initialStatus" : "Created/Lead"
}
```
7. {{default_host}}:{{configuration_port}}/status-management
```
{
    "entityTypes": {
     "application": {
        "statuses": [
          {
            "code": "200.01",
            "name": "Created/Lead",
            "label": "Created/Lead",
            "style": "Created/Lead",
            "transitions": [
              "200.02",
              "200.03",
              "200.10",
              "200.12",
              "200.09",
              "200.11"
            ],
            "reasons": {
              "r1": "Borrower Initiated",
              "r2": "System Initiated",
              "r3": "Don't need the loan",
              "r4": "Interest too high",
              "r5": "Approved amount too low",
              "r6": "Competitor offered better terms",
              "r7": "Initial Decline - Insufficient Income",
              "r8": "Verification decline",
              "r9": "UW decline"
            }
          },
          {
            "code": "200.02",
            "name": "Submitted",
            "label": "Submitted",
            "style": "Submitted",
            "transitions": [
              "200.03",
              "200.10",
              "200.12",
              "200.09",
              "200.11"
            ],
            "reasons": {
              "r1": "Borrower Initiated",
              "r2": "System Initiated",
              "r3": "Don't need the loan",
              "r4": "Interest too high",
              "r5": "Approved amount too low",
              "r6": "Competitor offered better terms",
              "r7": "Initial Decline - Insufficient Income",
              "r8": "Verification decline",
              "r9": "UW decline"
            }
          },
          {
            "code": "200.03",
            "name": "In Process - Verification",
            "label": "In Process - Verification",
            "style": "In Process - Verification",
            "transitions": [
              "200.04",
              "200.10",
              "200.12",
              "200.09",
              "200.11"
            ],
            "reasons": {
              "r1": "Borrower Initiated",
              "r2": "System Initiated",
              "r3": "Don't need the loan",
              "r4": "Interest too high",
              "r5": "Approved amount too low",
              "r6": "Competitor offered better terms",
              "r7": "Initial Decline - Insufficient Income",
              "r8": "Verification decline",
              "r9": "UW decline"
            }
          },
          {
            "code": "200.04",
            "name": "In Process - Underwriting",
            "label": "In Process - Underwriting",
            "style": "In Process - Underwriting",
            "transitions": [
              "200.05",
              "200.10",
              "200.12",
              "200.09",
              "200.11"
            ],
            "reasons": {
              "r1": "Borrower Initiated",
              "r2": "System Initiated",
              "r3": "Don't need the loan",
              "r4": "Interest too high",
              "r5": "Approved amount too low",
              "r6": "Competitor offered better terms",
              "r7": "Initial Decline - Insufficient Income",
              "r8": "Verification decline",
              "r9": "UW decline"
            }
          },
          {
            "code": "200.05",
            "name": "In Process - Approved",
            "label": "In Process - Approved",
            "style": "In Process - Approved",
            "transitions": [
              "200.06",
              "200.09",
              "200.10",
              "200.11"
            ],
            "reasons": {
              "r1": "Borrower Initiated",
              "r2": "System Initiated",
              "r3": "Don't need the loan",
              "r4": "Interest too high",
              "r5": "Approved amount too low",
              "r6": "Competitor offered better terms"
            }
          },
          {
            "code": "200.06",
            "name": "Sent for funding",
            "label": "Sent for funding",
            "style": "Sent for funding",
            "transitions": [
              "200.07"
            ]
          },
          {
            "code": "200.07",
            "name": "Funded",
            "label": "Funded",
            "style": "Funded",
            "transitions": [
              "200.08"
            ]
          },
          {
            "code": "200.08",
            "name": "On-boarded to LMS",
            "label": "On-boarded to LMS",
            "style": "On-boarded to LMS",
            "transitions": []
          },
          {
            "code": "200.09",
            "name": "Cancelled",
            "label": "Cancelled",
            "style": "Cancelled",
            "transitions": [],
            "reasons": {
              "r1": "Borrower Initiated",
              "r2": "System Initiated"
            }
          },
          {
            "code": "200.10",
            "name": "Not Interested",
            "label": "Not Interested",
            "style": "Not Interested",
            "transitions": [],
            "reasons": {
              "r3": "Don't need the loan",
              "r4": "Interest too high",
              "r5": "Approved amount too low",
              "r6": "Competitor offered better terms"
            }
          },
          {
            "code": "200.11",
            "name": "Expired",
            "label": "Expired",
            "style": "Expired",
            "transitions": []
          },
          {
            "code": "200.12",
            "name": "Declined",
            "label": "Declined",
            "style": "Declined",
            "transitions": [],
            "reasons": {
              "r7": "Initial Decline - Insufficient Income",
              "r8": "Verification decline",
              "r9": "UW decline"
            }
          }
        ]
      }
    }
}
```
8. Configurations for {{default_host}}:5070
```
{
  "primaryApplicant": {
  "userName": "{{LoginName}}",
  "password": "TestPassword",
  "salutation": "Miss",
  "firstName": "Test",
  "lastName": "Test",
  "middleName": "T",
  "emailAddresses": [
    {
      "email": "test@gmail.com",
      "emailType": "Personal",
      "isDefault": true
    }
  ],
  "aadhaarNumber": "12345",
  "permanentAccountNumber": "45345345",
  "dateOfBirth": "2016-09-02T15:24:28.2480918+05:30",
  "phoneNumbers": [
    {
      "phone": "9898989898",
      "countryCode": "91",
      "phoneType": "Residence",
      "isDefault": false
    }
  ],
  "addresses": [
    {
      "addressLine1": "test1",
      "addressLine2": "test3",
      "addressLine3": null,
      "addressLine4": null,
      "landMark": null,
      "city": "Ahmedabad",
      "state": "Gujarat",
      "pinCode": "380034",
      "country": "India",
      "addressType": "Residence",
      "isDefault": false
    }
  ],
  "gender": "Female",
  "maritalStatus": "Single",
  "employmentDetails": [
    {
      "addresses": [
        {
          "addressLine1": "Employeertest1",
          "addressLine2": "Employeertest2",
          "addressLine3": null,
          "addressLine4": null,
          "landMark": null,
          "city": "Ahmedabad",
          "state": "Gujarat",
          "pinCode": "380034",
          "country": "India",
          "addressType": "Work",
          "isDefault": false
        }
      ],
      "designation": "Manager",
      "lengthOfEmployment": 60,
      "name": "SIS",
      "workEmailId": "TestSigma@gmail.com",
         "employmentStatus": "Salaried",
      "workPhoneNumbers": [
        {
          "phone": "9898989898",
          "countryCode": "91",
          "phoneType": "Work",
          "isDefault": false
        }
      ]
    }
  ]
  },

  "requestedAmount": "7000",
  "requestedTermType": "Monthly",
  "requestedTermValue": "12.5",
  "purposeOfLoan": "MovingAndRelocation",
  "source": {
    "trackingCode": "Code123",
    "systemChannel": "Merchant",
    "sourceType": "Merchant",
    "sourceReferenceId": "Ref123"
  },
  "applicationNumber": null,
  "initialOffer": {
    "offerId": "Offer123",
    "givenLoanAmount": 5678,
    "disbursedLoanAmount": 11,
    "annualPercentageRate": 34,
    "interestRate": 10,
    "repaymentFrequency": "Monthly",
    "repaymentInstallmentAmount": "0",
    "numberOfIntallments": "0",
    "offerFees": null,
    "loanTerm": "1"
  },
  "givenOffers": null,
  "selectedOffer": null,
  "employmentDetail": {
    "name": "Sigma",
    "WorkAddresses": [],
    "designation": "Manager",
    "workPhoneNumbers": [],
    "workEmailId": 
    {
      "email": "worktest@gmail.com",
      "emailType": "Personal",
      "isDefault": true
    },
    "employmentStatus": "Salaried",
    "lengthOfEmployment":"5"
  },
  "residenceType": "OwnedSelf",
  "selfDeclareExpense": {
    "monthlyRent": "5000",
    "emi": "5000",
    "monthlyExpenses": "10000",
    "creditCardBalances": "12000"
  }
}
```
9. {{default_host}}:5070/LCUSA-0000020/emailinforamtion
```
{
      "email": "test@gmail.com",
      "emailType": "Primary",
      "isVerified": false,
      "isDefault": true,
      "verifiedOn": "0001-01-01T00:00:00+00:00"
}
```
10. {{default_host}}:{{configuration_port}}/tagging-events
...
{
  "events": [
    {
      "Name": "ManualFactVerificationInitiated",
      "ApplicationNumber": "{Data.EntityId}",
      "ApplyTags": [
        "Pending Documents"
      ]
    },
    {
      "Name": "ApplicationCreated",
      "ApplicationNumber": "{Data.Application.ApplicationNumber}",
      "ApplyTags": [
        "Application Created"
      ]
    },
    {
      "Name": "FactVerificationCompleted",
      "ApplicationNumber": "{Data.Fact.EntityId}",
      "Rules": [
        {
          "Name": "taggingRuleForFactVerified",
          "Version": "1.0"
        }
      ]
    },
    {
      "Name": "InitialOfferSelected",
      "ApplicationNumber": "{Data.EntityId}",
      "Rules": [
        {
          "Name": "taggingRuleForInitialOfferAccepted",
          "Version": "1.0"
        }
      ]
    },
    {
      "Name": "FinalOfferSelected",
      "ApplicationNumber": "{Data.EntityId}",
      "Rules": [
        {
          "Name": "taggingRuleForFinalOfferAccepted",
          "Version": "1.0"
        }
      ]
    },
    {
      "Name": "VerificationDocumentReceived",
      "ApplicationNumber": "{Data.EntityId}",
      "Rules": [
        {
          "Name": "taggingRuleForPendingDocuments",
          "Version": "1.0"
        }
      ]
    },
    {
      "Name": "ApplicationStatusChanged",
      "ApplicationNumber": "{Data.EntityId}",
      "Rules": [
        {
          "Name": "taggingRuleForExportsOnStatus",
          "Version": "1.0"
        }
      ]
    },
    {
      "Name": "ApplicationRejected",
      "ApplicationNumber": "{Data.EntityId}",
      "Rules": [
        {
          "Name": "taggingRuleForApplicationRejection",
          "Version": "1.0"
        }
      ]
    },
	{
		"Name" : "ApplicationActivityLogged",
		"ApplicationNumber" : "{Data.ApplicationNumber}",
		"Rules" : [
			{
				"Name" : "taggingRuleForFollowupActivity",
				"Version" : "1.0"
			}
		]
	}
  ]
}
...
