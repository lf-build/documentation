var Emitter = require('events').EventEmitter
var util = require('util')
var path = require('path')
var View = require('./view')

var App = function () {
  this.on('view-selected', (viewname) => {
    var view = new View(viewname)
    this.emit('rendered', view.toHtml())
  })
}

util.inherits(App, Emitter)
module.exports = new App()
